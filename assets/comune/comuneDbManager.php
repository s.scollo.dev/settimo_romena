<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 04/02/2018
 * Time: 20:42
 */

namespace comune;

use db\driverDbHandler;

class comuneDbManager
{
    public static function getComuneById($id_comune){
        $query = "SELECT * FROM  comuni WHERE id_comune = ? ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute(array($id_comune));
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            if (is_array($result) && count($result) > 0) {
                return $result;
            }
            else {
                return false;
            }
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function getNominativiByComune($id_comune){
        $query = "
          SELECT * FROM
          nominativi
          INNER JOIN territori
            ON nominativi.id_territorio=territori.id_territorio
          INNER JOIN comuni
            ON nominativi.id_comune=comuni.id_comune
          WHERE nominativi.id_comune = :id_comune
          ORDER BY territori.nome_territorio";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute(
                array(
                    ":id_comune" => $id_comune
                )
            );
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result[0];
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }
}