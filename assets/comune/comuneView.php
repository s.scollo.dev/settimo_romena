<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 16/02/2018
 * Time: 15:24
 */

namespace comune;
use twig\driverTwigContext;
use twig\driverTwigView;

class comuneView
{
    public function showList()
    {
        $context = new driverTwigContext();
        driverTwigView::show('comune/lista_comune.twig',$context);
    }

    public function showDettagliNominativi($context)
    {
        driverTwigView::show('comune/dettagli_comune_nominativi.twig',$context);
    }

    public function showDettagliMappa($context)
    {
        driverTwigView::show('comune/dettagli_comune_mappa.twig',$context);
    }
}