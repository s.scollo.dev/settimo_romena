<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 16/02/2018
 * Time: 15:24
 */

namespace comune;

use db\driverDbHandler;
use dtjssp\DataTableJsServerSideProcessingManagerMySQL;

use impostazioni\impostazioniController;
use movimento\movimentoController;
use nominativo\nominativoController;

class comuneController
{
    public function pageLista(){
        $context = new comuneContext();
        $view = new comuneView();
        $view->showList($context);
    }

    public function loadLista()
    {
        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array('db' => 'id_comune', 'dt' => 'id_comune'),
            array('db' => 'nome_comune', 'dt' => 'nome_comune'),
            array('db' => 'nominativi', 'dt' => 'nominativi')
        );
        $whereResult = "";
        $whereResult2 = "";
        $res = DataTableJsServerSideProcessingManagerMySQL::complex($_POST, $sql_details, 'v_comuni_nominativi', 'id_comune', $columns, $whereResult, $whereResult2);

        echo json_encode($res);
    }

    public function pageDettagli($par){

        $context = new comuneContext();
        $id_comune = $par['id_comune'];
        $pagina = $par['pagina'];

        $info_comune = comuneDbManager::getComuneById($id_comune);
        $nome_comune =  $info_comune['nome_comune'];

        $context->nome_comune = $nome_comune;
        $context->id_comune = $id_comune;

        $view = new comuneView();
        switch ($pagina) {
            case 'nominativi':
                //recupero nominativi territorio
                $view->showDettagliNominativi($context);
                break;
            case 'mappa':
                //recupero mappa comune
                $view->showDettagliMappa($context);
                break;
        }
    }

    //INFO PER MARKER SU MAPPA
    public function loadListaMarker($par){
        $info = array();
        $id_comune = $par['id_comune'];
        $response = nominativoController::getNominativiByComune($id_comune);
        $info_territori = $response['array_territori'];
        $info_nominativi = $response['array_nominativi'];
        $latitudine_centro_mappa = 0;
        $longitudine_centro_mappa = 0;
        foreach($info_nominativi as $k => $v){
            //$info_comune = comuneDbManager::getComuneById($info_nominativi[$k]['id_comune']);
            //$info_nominativi[$k]['nome_comune'] = $info_comune['nome_comune'];

            //CALCOLO CENTRO MAPPA
            if($latitudine_centro_mappa == 0){
                $latitudine_centro_mappa = $info_nominativi[$k]['latitudine'];
            }else{
                $latitudine_centro_mappa = ($latitudine_centro_mappa+$info_nominativi[$k]['latitudine'])/2;
            }
            if($longitudine_centro_mappa == 0){
                $longitudine_centro_mappa = $info_nominativi[$k]['longitudine'];
            }else{
                $longitudine_centro_mappa = ($longitudine_centro_mappa+$info_nominativi[$k]['longitudine'])/2;
            }

        }
        $centro_mappa = array(
            "latitudine" => $latitudine_centro_mappa,
            "longitudine" => $longitudine_centro_mappa
        );

        $info = array(
            "info_territori" =>   $info_territori,
            "info_nominativi" =>   $info_nominativi,
            "centro_mappa" =>$centro_mappa
        );
        echo json_encode($info);
    }

}