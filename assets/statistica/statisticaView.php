<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 12/03/2018
 * Time: 12:09
 */

namespace statistica;
use twig\driverTwigContext;
use twig\driverTwigView;

class statisticaView
{
    public function showDatiPercorrenza($context){
        driverTwigView::show('statistica/dati_percorrenza.twig',$context);
    }
    public function showGraficoPercorrenza($context){
        driverTwigView::show('statistica/grafico_percorrenza.twig',$context);
    }
}