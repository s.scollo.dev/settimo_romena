<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 12/03/2018
 * Time: 12:08
 */

namespace statistica;


use helper\helpDate;
use impostazioni\impostazioniController;
use territorio\territorioDbManager;

class statisticaController
{
    public function pageDatiPercorrenza(){
        //CARICO DATE IN E OUT DI DEFAULT
        $data_start_default = helpDate::data_dash2slash(impostazioniController::getImpostazioniByKey("data_start_grafico"));
        $context = new statisticaContext();
        $context->data_start_default = $data_start_default;
        $context->data_end_default = date("d/m/Y");
        $view = new statisticaView();
        $view->showDatiPercorrenza($context);
    }

    public function pageGraficoPercorrenza(){
        //CARICO DATE IN E OUT DI DEFAULT
        $data_start_default = helpDate::data_dash2slash(impostazioniController::getImpostazioniByKey("data_start_grafico"));
        $context = new statisticaContext();
        $context->data_start_default = $data_start_default;
        $context->data_end_default = date("d/m/Y");
        $view = new statisticaView();
        $view->showGraficoPercorrenza($context);
    }

    public function loadDatiPercorrenza(){
        $data_start = helpDate::data_slash2dash($_POST['data_start']);
        $data_end = helpDate::data_slash2dash($_POST['data_end']);

        $diff_secondi=strtotime($data_end)-strtotime($data_start);
        $giorni=round($diff_secondi/86400);


        $tot_territori = 0;

        $array_id_terr = array();
        $info_perorrenza = territorioDbManager::getInfoPercorrenza($data_start,$data_end);
        foreach($info_perorrenza as $one){
            $id_territorio = $one['id_territorio'];
            if(!isset($array_id_terr[$id_territorio])){
                $tot_territori++;
                $array_id_terr[$id_territorio] = $id_territorio;
            }
        }

        $tot_rientri = count($info_perorrenza);
        if($tot_rientri > 0 && $tot_territori > 0){
            $media_lavorazione = round($giorni*$tot_territori/$tot_rientri,1);
        }else{
            $media_lavorazione = false;
        }


        $info = array(
            "data_start" => $_POST['data_start'],
            "data_end" => $_POST['data_end'],
            "tot_territori" => $tot_territori,
            "tot_rientri" => $tot_rientri,
            "giorni" => $giorni,
            "media" => $media_lavorazione,
        );
        echo json_encode($info);
    }

    public function loadGraficoPercorrenza(){

        $data_start = helpDate::data_slash2dash($_POST['data_start']);
        $data_end = helpDate::data_slash2dash($_POST['data_end']);

        $struct = array();
        $info = territorioDbManager::getInfoPercorrenza($data_start,$data_end);

        foreach($info as $one){
            $id_territorio = $one['id_territorio'];
            $nome_territorio = $one['nome_territorio'];

            $info_movimento = array
            (
                "data_out" => helpDate::data_dash2slash($one['data_out']),
                "data_in" => helpDate::data_dash2slash($one['data_in']),
                "proclamatore" => $one['nome_utente']." ".$one['cognome_utente'],
                "nome_lavorazione" => $one['nome_lavorazione']
            );

            if(!isset($struct[$nome_territorio])){
                $struct[$nome_territorio]['id_territorio'] = $id_territorio;
                $struct[$nome_territorio]['nome_territorio'] = $nome_territorio;
                $struct[$nome_territorio]['movimenti'] = array();
            }

            $struct[$nome_territorio]['movimenti'][] = $info_movimento;
        }
        ksort($struct);
        echo json_encode($struct);
    }
}