<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 12/03/2018
 * Time: 12:08
 */

namespace statistica;


use helper\helpDate;
use impostazioni\impostazioniController;
use territorio\territorioDbManager;

class statisticaController
{
    public function pageDatiPercorrenza(){
        //CARICO DATE IN E OUT DI DEFAULT
        $data_start_default = helpDate::data_dash2slash(impostazioniController::getImpostazioniByKey("data_start_grafico"));
        $context = new statisticaContext();
        $context->data_start_default = $data_start_default;
        $context->data_end_default = date("d/m/Y");
        $view = new statisticaView();
        $view->showDatiPercorrenza($context);
    }

    public function pageGraficoPercorrenza(){
        //CARICO DATE IN E OUT DI DEFAULT
        $data_start_default = helpDate::data_dash2slash(impostazioniController::getImpostazioniByKey("data_start_grafico"));
        $context = new statisticaContext();
        $context->data_start_default = $data_start_default;
        $context->data_end_default = date("d/m/Y");
        $view = new statisticaView();
        $view->showGraficoPercorrenza($context);
    }

    public function loadDatiPercorrenza(){
        $data_start = helpDate::data_slash2dash($_POST['data_start']);
        $data_end = helpDate::data_slash2dash($_POST['data_end']);

        $diff_secondi=strtotime($data_end)-strtotime($data_start);
        $giorni=round($diff_secondi/86400);


        $tot_territori = 0;

        $array_id_terr = array();
        $info_perorrenza = territorioDbManager::getInfoPercorrenza($data_start,$data_end);
        foreach($info_perorrenza as $one){
            $id_territorio = $one['id_territorio'];
            if(!isset($array_id_terr[$id_territorio])){
                $tot_territori++;
                $array_id_terr[$id_territorio] = $id_territorio;
            }
        }

        $tot_rientri = count($info_perorrenza);
        if($tot_rientri > 0 && $tot_territori > 0){
            $media_lavorazione = round($giorni*$tot_territori/$tot_rientri,1);
        }else{
            $media_lavorazione = false;
        }


        $info = array(
            "data_start" => $_POST['data_start'],
            "data_end" => $_POST['data_end'],
            "tot_territori" => $tot_territori,
            "tot_rientri" => $tot_rientri,
            "giorni" => $giorni,
            "media" => $media_lavorazione,
        );

        //conservo dati in sessione per stampa futura
        $_SESSION['dati_percorrenza'] = $info;
        echo json_encode($info);
    }

    public function loadGraficoPercorrenza(){

        $data_start = helpDate::data_slash2dash($_POST['data_start']);
        $data_end = helpDate::data_slash2dash($_POST['data_end']);

        $struct = array();
        $info = territorioDbManager::getInfoPercorrenza($data_start,$data_end);

        foreach($info as $one){
            $id_territorio = $one['id_territorio'];
            $nome_territorio = $one['nome_territorio'];

            $info_movimento = array
            (
                "data_out" => helpDate::data_dash2slash($one['data_out']),
                "data_in" => helpDate::data_dash2slash($one['data_in']),
                "proclamatore" => $one['nome_utente']." ".$one['cognome_utente'],
                "nome_lavorazione" => $one['nome_lavorazione']
            );

            if(!isset($struct[$nome_territorio])){
                $struct[$nome_territorio]['id_territorio'] = $id_territorio;
                $struct[$nome_territorio]['nome_territorio'] = $nome_territorio;
                $struct[$nome_territorio]['movimenti'] = array();
            }

            $struct[$nome_territorio]['movimenti'][] = $info_movimento;
        }
        ksort($struct);
        echo json_encode($struct);
    }

    public function stampaDatiPercorrenza(){
        include_once("ext_assets/fpdf181/fpdf.php");

        $info_percorrenza = $_SESSION['dati_percorrenza'];

        $periodo = $info_percorrenza['data_start']. " - ".$info_percorrenza['data_end'];
        $giorni = $info_percorrenza['giorni'];
        $territori = $info_percorrenza['tot_territori'];
        $rientri = $info_percorrenza['tot_rientri'];
        $percorrenza = $info_percorrenza['media'];

        $pdf = new \FPDF('P','mm','A4');
        $pdf->AddPage();

        $alt_1=10;
        $larg_1=80;
        $data=date("d-m-Y");
        /*** titolo ***/

        $pdf->SetX(20);
        $pdf->SetY(20);
        $pdf->SetFont('Arial','B',13);
        $pdf->Cell(190,15,'Congregazione Settimo Torinese Romena',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,5,'Calcolo percorrenza media',0,1,'C');
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(190,15,'dati aggiornati al '.$data,0,1,'C');

        /*** corpo pagina ***/

        $pdf->SetFont('Arial','B',11);
        $pdf->SetX(120);
        $pdf->SetY(60);
        $pdf->Cell($larg_1,$alt_1,'Periodo di riferimento:',0,0,'R');
        $pdf->SetFont('Arial','',11);
        $pdf->Cell($larg_1,$alt_1,$periodo,0,1,'L');

        $pdf->SetFont('Arial','B',11);
        $pdf->Cell($larg_1,$alt_1,'Totale Giorni:',0,0,'R');
        $pdf->SetFont('Arial','',11);
        $pdf->Cell($larg_1,$alt_1,$giorni,0,1,'L');

        $pdf->SetFont('Arial','B',11);
        $pdf->Cell($larg_1,$alt_1,'Territori Lavorati:',0,0,'R');
        $pdf->SetFont('Arial','',11);
        $pdf->Cell($larg_1,$alt_1,$territori,0,1,'L');

        $pdf->SetFont('Arial','B',11);
        $pdf->Cell($larg_1,$alt_1,'Totale rientri:',0,0,'R');
        $pdf->SetFont('Arial','',11);
        $pdf->Cell($larg_1,$alt_1,$rientri,0,1,'L');

        $pdf->SetFont('Arial','B',11);
        $pdf->Cell($larg_1,$alt_1,'Un Territorio � lavorato in media ogni',0,0,'R');
        $pdf->SetFont('Arial','',11);
        $pdf->Cell($larg_1,$alt_1,$percorrenza,0,1,'L');


        $pdf->Output();
    }
}