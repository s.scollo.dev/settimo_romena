<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace utente;
use comune\comuneDbManager;
use db\driverDbHandler;
use dtjssp\DataTableJsServerSideProcessingManagerMySQL;
use helper\helpDate;
use nominativo\nominativoController;
use territorio\territorioController;
use uac\driverUacController;


class utenteController
{

    public function pageLista(){
        $view = new utenteView();
        $view->showList();
    }

    public function  loadLista(){
        $array_ruoli = driverUacController::getRuoliUac($_SESSION['uac']['id_utente']);
        $check_admin = false;
        foreach($array_ruoli as $one_r){
            if($one_r['id'] == 1){
                $condizione = '';
            }else{
                $condizione = 'attivo = 1';
            }
        }

        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array( 'db' => 'id_utente',  'dt' => 'id_utente' ),
            array( 'db' => 'nome_utente',  'dt' => 'nome_utente' ),
            array( 'db' => 'cognome_utente',   'dt' => 'cognome_utente' ),
            array( 'db' => 'user_utente',   'dt' => 'user_utente' ),
            array( 'db' => 'attivo',   'dt' => 'attivo' )
        );
        //complex $res = DataTableJsServerSideProcessingManagerMySQL::complex( $_POST, $sql_details, 'approvato', 'chkin_job_item_id', $columns, $whereResult , $whereResult);
        $res = DataTableJsServerSideProcessingManagerMySQL::complex( $_POST, $sql_details, '_utente', 'id_utente', $columns ,$condizione );

        echo json_encode($res);
    }

    public function pageModulo($par = null){
        //proprieta utente: id_utente, nome_utente, cognome_utente, telefono_utente, mail_utente, id_congregazione_utente, user_utente, psw_utente, id_abbinamento_utente, approvato 
        if(isset($par['id_utente']) && trim($par['id_utente']) != ''){
            //modifica utente
            $id_utente = $par['id_utente'];
            $model = new utenteDbManager();
            $utente = $model->getUtenteById($id_utente);
        }else{
            //nuovo utente
        }

        //tutti utenti per lista abbinamento
        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array( 'db' => 'id_utente',  'dt' => 'id_utente' ),
            array( 'db' => 'nome_utente',  'dt' => 'nome_utente' ),
            array( 'db' => 'cognome_utente',   'dt' => 'cognome_utente' ),
            array( 'db' => 'mail_utente',   'dt' => 'mail_utente' ),
            array( 'db' => 'telefono_utente',   'dt' => 'telefono_utente' ),
            array( 'db' => 'approvato',   'dt' => 'approvato' )
        );
        $whereResult = "approvato = 1";
        $whereResult2 = "approvato = 1 ORDER BY nome_utente";
        $all_utenti_attivi = DataTableJsServerSideProcessingManagerMySQL::complex( $_POST, $sql_details, 'utente', 'id_utente', $columns, $whereResult , $whereResult2);

        //carico context
        $context = new utenteContext();
        $context->id_utente = $utente['id_utente'];
        $context->nome_utente = $utente['nome_utente'];
        $context->cognome_utente = $utente['cognome_utente'];
        $context->telefono_utente = $utente['telefono_utente'];
        $context->mail_utente = $utente['mail_utente'];
        $context->user_utente = $utente['user_utente'];
        $context->psw_utente = $utente['psw_utente'];
        $context->approvato = $utente['approvato'];
        $context->id_abbinamento_utente = $utente['id_abbinamento_utente'];
        $context->all_utenti_attivi = $all_utenti_attivi['data'];

        $view = new utenteView();
        $view->showModulo($context);
    }

    public function save()
    {
        $response = array("esito" => "ERR", "msg" => "Errore generico");
        $id_utente = null;
        $user_utente = null;
        $psw_utente = null;
        $cognome_utente = null;
        $nome_utente = null;
        $telefono_utente = null;
        $mail_utente = null;
        $id_abbinamento_utente = null;
        $approvato = 0;

        if (isset($_POST['id_utente']) && trim($_POST['id_utente']) != '') {
            $id_utente = trim($_POST['id_utente']);
        }
        if (isset($_POST['user_utente']) && trim($_POST['user_utente']) != '') {
            $user_utente = trim($_POST['user_utente']);
        }
        if (isset($_POST['psw_utente']) && trim($_POST['psw_utente']) != '') {
            $psw_utente = trim($_POST['psw_utente']);
        }
        if (isset($_POST['cognome_utente']) && trim($_POST['cognome_utente']) != '') {
            $cognome_utente = trim($_POST['cognome_utente']);
        }
        if (isset($_POST['nome_utente']) && trim($_POST['nome_utente']) != '') {
            $nome_utente = trim($_POST['nome_utente']);
        }
        if (isset($_POST['telefono_utente']) && trim($_POST['telefono_utente']) != '') {
            $telefono_utente = trim($_POST['telefono_utente']);
        }
        if (isset($_POST['mail_utente']) && trim($_POST['mail_utente']) != '') {
            $mail_utente = trim($_POST['mail_utente']);
        }
        if (isset($_POST['id_abbinamento_utente']) && trim($_POST['id_abbinamento_utente']) != '') {
            $id_abbinamento_utente = trim($_POST['id_abbinamento_utente']);
        }
        if (isset($_POST['approvato']) && trim($_POST['approvato']) != '') {
            $approvato = 1;
        }

        $model = new utenteModel();
        $model->setOldUtente($id_utente, $user_utente, $psw_utente, $cognome_utente, $nome_utente, $telefono_utente, $mail_utente, $id_abbinamento_utente, $approvato);
        if (utenteDbManager::saveUtente($model)) {
            $response = array("esito" => "OK", "msg" => "Operazione completata");
        };

        echo json_encode($response);
    }

    public function ajaxGetAllUtenti(){
        echo json_encode(utenteDbManager::getAllUtenti());
    }

    public function pageGuestTerritori(){
        $uac = driverUacController::getUacSession();
        $id_utente = $uac->id_utente;
        $territori = territorioController::getTerritorioByUtente($id_utente);
        $context = new utenteContext();
        $context->territori = $territori;
        //pre($context);
        $view = new utenteView();
        $view->showGuestTerritori($context);
    }

    public function pageGuestNominativi($par){
        $id_territorio = $par['id_territorio'];
        $info_territorio = territorioController::getTerritorioById($id_territorio);
        $nominativi = nominativoController::getNominativiByTerritorio($id_territorio);
        $context = new utenteContext();
        $context->nominativi = $nominativi;
        $context->nome_territorio = $info_territorio['nome_territorio'];
        $context->id_territorio = $id_territorio;
        $view = new utenteView();
        $view->showGuestNominativi($context);
    }

    public function pageGuestMappa($par){
        $id_territorio = $par['id_territorio'];
        $info_territorio = territorioController::getTerritorioById($id_territorio);
        $nome_territorio = $info_territorio['nome_territorio'];
        $info = array();
        $info_nominativi = nominativoController::getNominativiByTerritorio($id_territorio);
        $latitudine_centro_mappa = 0;
        $longitudine_centro_mappa = 0;
        foreach($info_nominativi as $k => $v){
            $info_comune = comuneDbManager::getComuneById($info_nominativi[$k]['id_comune']);

            $info_nominativi[$k]['nome_comune'] = $info_comune['nome_comune'];

            //CALCOLO CENTRO MAPPA
            if($latitudine_centro_mappa == 0){
                $latitudine_centro_mappa = $info_nominativi[$k]['latitudine'];
            }else{
                $latitudine_centro_mappa = ($latitudine_centro_mappa+$info_nominativi[$k]['latitudine'])/2;
            }
            if($longitudine_centro_mappa == 0){
                $longitudine_centro_mappa = $info_nominativi[$k]['longitudine'];
            }else{
                $longitudinee_centro_mappa = ($longitudine_centro_mappa+$info_nominativi[$k]['longitudine'])/2;
            }

        }
        $centro_mappa = array(
            "latitudine" => $latitudine_centro_mappa,
            "longitudine" => $longitudine_centro_mappa
        );


        $context = new utenteContext();
        $context->info_nominativi = $info_nominativi;
        $context->centro_mappa = $centro_mappa;
        $context->info_territorio = $info_territorio;
        $context->nome_territorio = $nome_territorio;
        $context->id_territorio = $id_territorio;
        $view = new utenteView();
        $view->showGuestMappa($context);
    }

    public function getTerritoriByUtente($par){
        $id_utente = $par['id_utente'];
        $territori = territorioController::getTerritorioByUtente($id_utente);
        $info_out = array();
        foreach($territori as $one){
            $info_out[] = array(
                "id_territorio" => $one['id_territorio'],
                "nome_territorio" =>  $one['nome_territorio'],
                "data_out" =>  helpDate::data_dash2slash($one['data_out']),
                "data_in" =>  helpDate::data_dash2slash($one['data_in'])
            );
        }

        echo json_encode($info_out);

    }



}