<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace utente;

class utenteModel
{
    /**
    * @var int
    */
    protected $id;

    /*
     * @var string
     */
    protected $user;

    /*
     * @var string
     */

    protected $password;
    /*
     * @var string
     */
    protected $nome;

    /*
     * @var string
     */
    protected $cognome;

    /**
     * @var string
     */
    protected $telefono;

    /**
     * @var string
     */
    protected $mail;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getCognome()
    {
        return $this->cognome;
    }

    /**
     * @param string $cognome
     */
    public function setCognome($cognome)
    {
        $this->cognome = $cognome;
    }

    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }


    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }



    public function setNewUtente($id_utente,$user_utente,$psw_utente,$cognome_utente,$nome_utente,$telefono_utente,$mail_utente)
    {

    }
    
    public function setOldUtente($id_utente,$user_utente,$psw_utente,$cognome_utente,$nome_utente,$telefono_utente,$mail_utente)
    {
        $this->id = $id_utente;
        $this->user = $user_utente;
        $this->password = $psw_utente;
        $this->cognome = $cognome_utente;
        $this->nome = $nome_utente;
        $this->telefono = $telefono_utente;
        $this->mail = $mail_utente;
    }





}