<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace utente;
use twig\driverTwigContext;
use twig\driverTwigView;


class utenteView
{
    public function showList()
    {
        $context = new driverTwigContext();
        //per aggingere parametri  $context->nuovoParametro = 'valore';
        driverTwigView::show('utente/lista_utente.twig',$context);
    }
    public function showModulo($context)
    {
        driverTwigView::show('utente/modulo_utente.twig',$context);
    }

    public function showGuestTerritori($context)
    {
        driverTwigView::show('guest/guest_territori.twig',$context);
    }

    public function showGuestNominativi($context)
    {
        driverTwigView::show('guest/guest_nominativi.twig',$context);
    }

    public function showGuestMappa($context)
    {
        driverTwigView::show('guest/guest_mappa.twig',$context);
    }


}