<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 08/02/2017
 * Time: 08:38
 */

namespace utente;
use twig\driverTwigContext;

class utenteContext extends driverTwigContext
{

    public $all_utenti_attivi;
    public $id_utente;
    public $nome_utente;
    public $cognome_utente;
    public $mail_utente;
    public $telefono_utente;
    public $user_utente;
    public $psw_utente;
    public $id_abbinamento_utente;
    public $attivo;
}