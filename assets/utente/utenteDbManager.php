<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace utente;

use db\driverDbHandler;

class utenteDbManager
{
    const TAB_UTENTE = "_utente";
    const TAB_UTENTE_RUOLO = "_utente_ruolo";

    public function getUtenteById($id){
        $query = "SELECT * FROM ".self::TAB_UTENTE." WHERE id_utente = ? ";
        try{
            $cn = driverDbHandler::getCon();
            $sth = $cn->prepare($query);
            $sth->execute(array($id));
            $resp = $sth->fetch(\PDO::FETCH_ASSOC);
            $sth->closeCursor();
            unset($sth,$cn);
            if (is_array($resp) && count($resp) > 0) {
                return $resp;
            }
            else {
                die('ERRORE: UTENTE NON TROVATO');
            }
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }
/*
    public static function saveUtente( utenteModel $model)
    {
        $id_utente = $model->getId();
        if (isset($id_utente)) {
            //UPDATE
            $info_update_utente = array(
                ":id_utente" => $model->getId(),
                ":nome_utente" => $model->getNome(),
                ":cognome_utente" => $model->getCognome(),
                ":telefono_utente" => $model->getTelefono(),
                ":mail_utente" => $model->getMail(),
                ":id_congregazione_utente" => $model->getIdCongregazione(),
                ":user_utente" => $model->getUser(),
                ":psw_utente" => $model->getPassword(),
                ":id_abbinamento_utente" => $model->getIdAbbinamento(),
                ":approvato" => $model->getApprovato()
            );
            $q_update_utente = "UPDATE " . self::TAB_UTENTE . " SET
                nome_utente = :nome_utente,
                cognome_utente = :cognome_utente,
                telefono_utente = :telefono_utente,
                mail_utente = :mail_utente,
                id_congregazione_utente = :id_congregazione_utente,
                user_utente = :user_utente,
                psw_utente = :psw_utente,
                id_abbinamento_utente = :id_abbinamento_utente,
                approvato = :approvato
                WHERE 
                id_utente = :id_utente";
            try {
                $cn = driverDbHandler::getCon();
                $cn->beginTransaction();
                //update utente
                $q_update_utente = $cn->prepare($q_update_utente);
                $q_update_utente->execute($info_update_utente);
                $cn->commit();
                $stato_ruolo = 0;
                if($model->getApprovato() == 1){
                    $stato_ruolo = 1;
                }
                $id_utente = $model->getId();
                utenteDbManager::gestioneRuoloUtente($id_utente,3,$stato_ruolo);
                return true;
            } catch (Exception $e) {
                $cn->rollBack();
                trigger_error("Error" . $e->getMessage());
                return false;
            }

        } else {
            //INSERT
            $info_insert_utente = array(
                ":nome_utente" => $model->getNome(),
                ":cognome_utente" => $model->getCognome(),
                ":telefono_utente" => $model->getTelefono(),
                ":mail_utente" => $model->getMail(),
                ":id_congregazione_utente" => $model->getIdCongregazione(),
                ":user_utente" => $model->getUser(),
                ":psw_utente" => $model->getPassword(),
                ":id_abbinamento_utente" => $model->getIdAbbinamento(),
                ":approvato" => $model->getApprovato()
            );
            $q_insert_utente = "INSERT INTO" . self::TAB_UTENTE . " (nome_utente,cognome_utente,telefono_utente,mail_utente,id_congregazione_utente,user_utente,psw_utente,id_abbinamento_utente,approvato)
                VALUES(:nome_utente,:cognome_utente,:telefono_utente,:mail_utente,:id_congregazione_utente,:user_utente,:psw_utente,:id_abbinamento_utente,:approvato)";

            try {
                $cn = driverDbHandler::getCon();
                $cn->beginTransaction();
                //insert utente
                $q_insert_utente = $cn->prepare($q_insert_utente);
                $q_insert_utente->execute($info_insert_utente);
                if($model->getApprovato() == 1){
                    $stato_ruolo = 1;
                }
                $id_utente = $cn->lastInsertId();
                utenteDbManager::gestioneRuoloUtente($id_utente,3,$stato_ruolo);
                $cn->commit();
                return true;
            } catch (Exception $e) {
                $cn->rollBack();
                trigger_error("Error" . $e->getMessage());
                return false;
            }
        }
    }
*/
    public static function getAllUtenti(){
        $query = "SELECT * FROM  ".self::TAB_UTENTE." WHERE attivo=1";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($c_userinfo,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function updateCredenzialiUtente($id_utente, $user_utente, $psw_utente, $psw_utente_chiaro, $attivo){

        $query = "UPDATE
                    _utente
                  SET
                    user_utente = :user_utente,
                    psw_utente = :psw_utente,
                    psw_utente_chiaro = :psw_utente_chiaro,
                    attivo = :attivo
                  WHERE
                    id_utente = :id_utente";
        try{
            $cn = driverDbHandler::getCon();
            $cn->quote($user_utente);
            $cn->quote($psw_utente);
            $cn->quote($psw_utente_chiaro);
            $stmt = $cn->prepare($query);
            $stmt->execute(
                array(
                    ":user_utente" => $user_utente,
                    ":psw_utente" => $psw_utente,
                    ":psw_utente_chiaro" => $psw_utente_chiaro,
                    ":id_utente" => $id_utente,
                    ":attivo" => $attivo
                )
            );
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($c_userinfo,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

}