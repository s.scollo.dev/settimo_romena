<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 21/02/2018
 * Time: 10:44
 */

namespace guest;

use twig\driverTwigContext;
use twig\driverTwigView;

class guestView
{
    public function showPage($context)
    {
        driverTwigView::show('guest/page.twig',$context);
    }
}