<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 21/02/2018
 * Time: 10:42
 */

namespace guest;

use nominativo\nominativoController;
use territorio\territorioController;
use comune\comuneDbManager;
use uac\driverUacController;

class guestController
{
    public function loadPage(){
        $uac = driverUacController::getUacSession();
        $id_utente = $uac->id_utente;
        $context = new guestContext();
        $context->id_utente = $id_utente;
        $view = new guestView();
        $view->showPage($context);
    }

    public function loadTerritori($par){
        $id_utente = $par['id_user'];
        $result = territorioController::getTerritorioByUtente($id_utente);
        echo json_encode($result);

    }

    public function loadNominativi($par){
        $id_territorio = $par['id_territorio'];
        $result = nominativoController::getNominativiByTerritorio($id_territorio);
        echo json_encode($result);
    }

    public function loadMappa($par){
        $id_territorio = $par['id_territorio'];
        $info = array();
        $info_nominativi = nominativoController::getNominativiByTerritorio($id_territorio);
        $latitudine_centro_mappa = 0;
        $longitudine_centro_mappa = 0;
        foreach($info_nominativi as $k => $v){
            $info_comune = comuneDbManager::getComuneById($info_nominativi[$k]['id_comune']);
            $info_nominativi[$k]['nome_comune'] = $info_comune['nome_comune'];

            //CALCOLO CENTRO MAPPA
            if($latitudine_centro_mappa == 0){
                $latitudine_centro_mappa = $info_nominativi[$k]['latitudine'];
            }else{
                $latitudine_centro_mappa = ($latitudine_centro_mappa+$info_nominativi[$k]['latitudine'])/2;
            }
            if($longitudine_centro_mappa == 0){
                $longitudine_centro_mappa = $info_nominativi[$k]['longitudine'];
            }else{
                $longitudinee_centro_mappa = ($longitudine_centro_mappa+$info_nominativi[$k]['longitudine'])/2;
            }

        }
        $centro_mappa = array(
            "latitudine" => $latitudine_centro_mappa,
            "longitudine" => $longitudine_centro_mappa
        );

        $info = array(
            "info_nominativi" =>   $info_nominativi,
            "centro_mappa" =>$centro_mappa
        );
        echo json_encode($info);
    }


}