<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 04/02/2017
 * Time: 08:13
 */

namespace twig;

use driver\config;
use uac\driverUacController;

class driverTwigContext
{
    public $version = config::VERSION;
    public $nameApp = config::NAMEAPP;
    public $dirDefault = config::DIR_DEFAULT;
    public $dirFileIn = config::DIR_FILE_IN;
    public $dirFileOut= config::DIR_FILE_OUT;
    public $dirImg = config::DIR_IMG;
    public $menu = NULL;
    public $nome_uac = NULL;

    public function __construct(){
        $uac  =  driverUacController::getUacSession();
        if($uac != FALSE){
            $menu = driverUacController::getMenuUac($uac->id_utente);
            $this->menu = ($menu);
            $this->nome_uac = $uac->nome_utente.' '.$uac->cognome_utente;
        }
    }
}