<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 04/02/2017
 * Time: 08:02
 */

namespace twig;

use driver\config;


class driverTwigView
{
    public static function show($page, driverTwigContext $context ){
        $loader = new \Twig_Loader_Filesystem(config::DIR_TEMPLATE);
        $twig = new \Twig_Environment($loader,array());
        echo $twig->display($page, (array) $context);
    }

    public static function show404()
    {
        $loader = new \Twig_Loader_Filesystem(config::DIR_TEMPLATE);
        $twig = new \Twig_Environment($loader,array());
        echo $twig->display('404.twig', (array) new driverTwigContext());
        //TODO migliorare accesso di non autorizzati
        //echo $twig->display('logout.twig', (array) new driverTwigContext());
    }
}