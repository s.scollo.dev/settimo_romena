<?php

namespace route;


use driver\config;

class driverRouteConfig
{

    const HOME_REDIRECT = config::HOME_DIRECT;

    public static function startRoute(){
        $x = array(
            array(
                "id" => 0,
                "regex" => "login",
                "classe" => "\\login\\driveLoginController",
                "metodo" => "startFormLogin"
            ),
            array(
                "id" => 1,
                "regex" => "checklogin",
                "classe" => "\\login\\driveLoginController",
                "metodo" => "checkLogin"
            )
        );
        return $x;
    }

    public static function defaultRoutes(){
        $x =  array(
            array(
                "id" => 0,
                "regex" => "logout",
                "classe" => "\\login\\driveLoginController",
                "metodo" => "logout"
            ),
            array(
                "id" => 0,
                "regex" => "test",
                "classe" => "\\test\\test",
                "metodo" => "main"
            ),
            array(
                "id" => 0,
                "regex" => "updateCooGps",
                "classe" => "\\script\\scriptController",
                "metodo" => "updateCooGps"
            ),
            array(
                "id" => 0,
                "regex" => "setPasswordUtente",
                "classe" => "\\script\\scriptController",
                "metodo" => "setPasswordUtente"
            )
        );
        return $x;
    }
}