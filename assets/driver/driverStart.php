<?php
namespace driver;

use driver\driverAutoloader\driverPSR4Autoloader;
use driver\driverAutoloader\driverMappedClassAutoloader;
use driver\driverAutoloader\externalAssetAutoloader;

class driverStart
{

    const DRIVER_CDN_ROOT = '';//'/var/www/webapp';

    /**
     * @var string
     */
    private $driver_CDN_config_filename;

    /**
     * @return string
     */
    private function getDriverStartConfigFilename()
    {
        return $this->driver_CDN_config_filename;
    }

    /**
     *
     */
    private function setDriverStartConfigFilename()
    {
        $driver_CDN_config_filename = 'assets/driver/driverStartConfig.json';
        if (!file_exists($driver_CDN_config_filename)) {
            file_put_contents($driver_CDN_config_filename,'');
        }
        $this->driver_CDN_config_filename = $driver_CDN_config_filename;
    }

    private static $instance;

    private function __construct()
    {
        $this->setDriverStartConfigFilename();
        $this->setDriverPSR4AutoloaderFilename();
    }

    private function __clone()
    {

    }

    /**
     * @return driverStart
     */
    public static function getInstance()
    {
        if(self::$instance == null)
        {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    /**
     *
     */
    public function callLoaders()
    {
        $this->loadCDNConfig();

        require_once $this->getDriverPSR4AutoloaderFilename();

        $this->setDriverPSR4Autoloader(new driverPSR4Autoloader());


        if (!empty($this->getCDNConfig()->driverPSR4Autoloader->namespaces)){
            $ls_namespaces = $this->getCDNConfig()->driverPSR4Autoloader->namespaces;
            if (is_array($ls_namespaces) && count($ls_namespaces) > 0) {
                foreach($ls_namespaces as $namespace_info){
                    $this->getDriverPSR4Autoloader()->addNamespace(
                        $namespace_info->prefix,
                        /*self::DRIVER_CDN_ROOT.DIRECTORY_SEPARATOR.*/$namespace_info->base_dir,
                        $namespace_info->prepend
                    );
                }
            }
        }

        $this->getDriverPSR4Autoloader()->register();

        //Fixed class autoloader
        $this->setDriverMappedClassAutoloader(new driverMappedClassAutoloader());

        if (!empty($this->getCDNConfig()->driverMappedClassAutoloader)){
            $ls_mapped_class = $this->getCDNConfig()->driverMappedClassAutoloader;
            if (is_array($ls_mapped_class) && count($ls_mapped_class) > 0) {
                foreach($ls_mapped_class as $mapped_class_info){
                    $this->getDriverMappedClassAutoloader()->mapClass($mapped_class_info->class_name,self::DRIVER_CDN_ROOT.DIRECTORY_SEPARATOR.$mapped_class_info->class_filename);
                }
            }
        }

        $this->getDriverMappedClassAutoloader()->register();

        //External classes autoloaders
        if (!empty($this->getCDNConfig()->externalAssetAutoloader)){
            $ls_external_autoloader = $this->getCDNConfig()->externalAssetAutoloader;
            if (is_array($ls_external_autoloader) && count($ls_external_autoloader) > 0) {
                foreach($ls_external_autoloader as $ext_autoloader_info){
                    $external_caller = new externalAssetAutoloader();
                    $external_caller->setFilePath(/*self::DRIVER_CDN_ROOT.DIRECTORY_SEPARATOR.*/$ext_autoloader_info->external_autoloder_filepath);
                    if (!empty($ext_autoloader_info->external_autoloder_callable->class) && !empty($ext_autoloader_info->external_autoloder_callable->method)) {
                        $external_caller->setCallable(array($ext_autoloader_info->external_autoloder_callable->class,$ext_autoloader_info->external_autoloder_callable->method));
                    }
                    $external_caller->invokeAutoloader();
                    $this->addExternalAssetAutoloader($external_caller);
                    unset($external_caller);
                }
            }
        }
    }



    /**
     * @var \StdClass
     */
    private $CDNConfig;

    /**
     * @return \StdClass
     */
    public function getCDNConfig()
    {
        return $this->CDNConfig;
    }

    /**
     * @param \StdClass $CDNConfig
     */
    protected function setCDNConfig($CDNConfig)
    {
        $this->CDNConfig = $CDNConfig;
    }

    protected function loadCDNConfig()
    {
        if (file_exists($this->getDriverStartConfigFilename())) {
            $this->setCDNConfig(json_decode(file_get_contents($this->getDriverStartConfigFilename())));
        }
        else {
            trigger_error("CONFIGURATION FILE NOT EXISTENT",E_USER_ERROR);
        }
    }

    /**
     * @var string
     */
    private $driverPSR4AutoloaderFilename;

    /**
     * @return driverPSR4Autoloader
     */
    private function getDriverPSR4AutoloaderFilename()
    {
        return $this->driverPSR4AutoloaderFilename;
    }

    /**
     *
     */
    private function setDriverPSR4AutoloaderFilename()
    {
        //$driverPSR4AutoloaderFile = self::DRIVER_CDN_ROOT.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'driver'.DIRECTORY_SEPARATOR.'driverAutoloader'.DIRECTORY_SEPARATOR.'driverPSR4Autoloader.php';
        $driverPSR4AutoloaderFile = 'assets/driver/driverAutoloader/driverPSR4Autoloader.php';
        if (file_exists($driverPSR4AutoloaderFile)) {
            $this->driverPSR4AutoloaderFilename = $driverPSR4AutoloaderFile;
        }
        else {
            trigger_error("CDN autoloader doesn't exists",E_USER_ERROR);
        }
    }

    /**
     * @var driverPSR4Autoloader
     */
    private $driverPSR4Autoloader;

    /**
     * @return driverPSR4Autoloader
     */
    public function getDriverPSR4Autoloader()
    {
        return $this->driverPSR4Autoloader;
    }

    /**
     * @param driverPSR4Autoloader $driverPSR4Autoloader
     */
    protected function setDriverPSR4Autoloader($driverPSR4Autoloader)
    {
        $this->driverPSR4Autoloader = $driverPSR4Autoloader;
    }

    /**
     * @var driverMappedClassAutoloader
     */
    private $driverMappedClassAutoloader;

    /**
     * @return driverMappedClassAutoloader
     */
    public function getDriverMappedClassAutoloader()
    {
        return $this->driverMappedClassAutoloader;
    }

    /**
     * @param driverMappedClassAutoloader $driverMappedClassAutoloader
     */
    protected function setDriverMappedClassAutoloader($driverMappedClassAutoloader)
    {
        $this->driverMappedClassAutoloader = $driverMappedClassAutoloader;
    }

    /**
     * @var externalAssetAutoloader[]
     */
    private $ls_externalAssetAutoloader;

    /**
     * @return externalAssetAutoloader[]
     */
    public function getLsExternalAssetAutoloader()
    {
        return $this->ls_externalAssetAutoloader;
    }

    /**
     * @param externalAssetAutoloader $external_asset_autoloader_instance
     */
    protected function addExternalAssetAutoloader($external_asset_autoloader_instance)
    {
        $this->ls_externalAssetAutoloader[] = $external_asset_autoloader_instance;
    }
}