<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 02/02/2017
 * Time: 17:57
 */

namespace login;

use uac\driverUacController;
use route\driverRouteConfig;
use log\log;

class driveLoginController
{
    public function startFormLogin()
    {
        $view = new driverLoginView();
        $view->showForm();
    }

    public function checkLogin()
    {
        if (!empty($_POST["user"]) && !empty($_POST["password"])) {
            //verifica credenziali

            $result = driverUacController::checkUac($_POST["user"],$_POST["password"]);

            if($result === false){
                $valid_login = "ERR_GENERIC";
            }
            else{
                $valid_login = "OK";
                //uac in sessione
                driverUacController::setUacSession($result);
                $home = driverUacController::getHome($result['id_utente']);

                //TODO estrarre ruolo da db
            }
            //risposta
            switch($valid_login){
                case "OK":
                    $json_ret = array(
                        "esito" => "SI",
                        "url_go" => $home
                    );
                    log::setLog('login');
                    break;
                case "ERR_USER":
                    $json_ret = array(
                        "esito" => "NO",
                        "messaggio" => "Username non riconosciuto"
                    );
                    break;
                case "ERR_PSWD":
                    $json_ret = array(
                        "esito" => "NO",
                        "messaggio" => "Password errata"
                    );
                    break;
                case "ERR_GENERIC":
                    $json_ret = array(
                        "esito" => "NO",
                        "messaggio" => "Username o Password errati"
                    );
                    break;
                case "NON_ABILITATO":
                    $json_ret = array(
                        "esito" => "NO",
                        "messaggio" => "Utente non abilitato"
                    );
                    break;
                default:
                    $json_ret = array(
                        "esito" => "NO",
                        "messaggio" => "Errore autenticazione generico"
                    );
            }
        }
        else {
            $json_ret = array(
                "esito" => "NO",
                "messaggio" => "Informazioni mancanti"
            );


        }
        echo json_encode($json_ret);
    }

    public function logout(){
        log::setLog('logout');
        $_SESSION['uac'] = "";
        header('Location: login');
    }
}