<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 02/02/2017
 * Time: 18:21
 */

namespace login;
use twig\driverTwigContext;
use twig\driverTwigView;

class driverLoginView
{

    public function showForm()
    {
        //echo '<pre>';
        //print_r($_SESSION);
        $context = new driverTwigContext();
        //per aggingere parametri  $context->nuovoParametro = 'valore';
        driverTwigView::show('login/login.twig',$context);
    }

}