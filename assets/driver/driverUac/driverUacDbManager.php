<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 02/02/2017
 * Time: 18:11
 */

namespace uac;

use db\driverDbHandler;

class driverUacDbManager
{
    const UTENTE_RUOLO = '_utente_ruolo';
    const UTENTE = '_utente';
    const RUOLO = '_ruolo';
    const MENU_UTENTE = '_v_menu_utente';
    const ROUTES_UTENTE = '_v_routes_utente';

    public function searchUac($user, $psw){
        //id_utente AS id, nome_utente AS nome, cognome_utente AS cognome
        //$query = "SELECT * FROM ".self::UTENTE." WHERE user_utente = ? AND psw_utente = ?";
        $query = "SELECT * FROM ".self::UTENTE." WHERE user_utente = ? AND psw_utente_chiaro = ?";
        try{
            $cn = driverDbHandler::getConGui();
            $c_userinfo = $cn->prepare($query);
            $c_userinfo->execute(array($user, $psw));
            $r_userinfo = $c_userinfo->fetch(\PDO::FETCH_ASSOC);
            $c_userinfo->closeCursor();
            unset($c_userinfo,$cn);
            if (is_array($r_userinfo) && count($r_userinfo) > 0) {
                return $r_userinfo;
            }
            else {
                return false;
            }
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public function getRuoloByUac($id_uac){
        $query = "
            SELECT
                id_ruolo AS id,
                nome_ruolo AS nome,
                home_ruolo AS home
            FROM ".self::RUOLO."
            INNER JOIN ".self::UTENTE_RUOLO."
                ON ".self::UTENTE_RUOLO.".id_ruolo_ur=".self::RUOLO.".id_ruolo
            WHERE
                ".self::UTENTE_RUOLO.".id_utente_ur = ?
        ";
        $cn = driverDbHandler::getConGui();
        $obj= $cn->prepare($query);
        $obj->execute(array($id_uac));
        $result = $obj->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getRouteByUac($id_uac)
    {
        $query = "
            SELECT
                id_route AS id,
                regex_route AS regex,
                classe_route AS classe,
                metodo_route AS metodo
            FROM ".self::ROUTES_UTENTE."
            WHERE
                id_utente = ?
        ";
        $cn = driverDbHandler::getConGui();
        $obj= $cn->prepare($query);
        $obj->execute(array($id_uac));
        $result = $obj->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getMenuByUac($id_uac)
    {
        $query = "
          SELECT
            id,
            label,
            regex,
            padre,
            icona
          FROM
            ".self::MENU_UTENTE."
          WHERE
            id_utente = ?
        ";
        $cn = driverDbHandler::getConGui();
        $obj= $cn->prepare($query);
        $obj->execute(array($id_uac));
        $result = $obj->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }


}