<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 01/02/2017
 * Time: 17:32
 */

namespace uac;


class driverUacModel
{
    public function __construct($resulCheckUac){
        $_SESSION['uac'] = $resulCheckUac;
    }

    public function getSession(){
        return $_SESSION['uac'];
    }

    public static function getIdUac(){
        return $_SESSION['uac']['id_utente'];
    }

    public static function getUacSession(){
        if(isset($_SESSION['uac']) && count($_SESSION['uac']) > 0 && $_SESSION['uac'] != ''){
            return (object) $_SESSION['uac'];
        }else{
            return FALSE;
        }
    }
}