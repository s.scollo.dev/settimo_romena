<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 01/02/2017
 * Time: 17:32
 */

namespace uac;

use route\driverRouteConfig;

class driverUacController
{

    public static function checkUac($user, $psw){
        if(isset($user) && trim($user) != '' && isset($psw) && trim($psw) != '')
        {
            $dbManager = new driverUacDbManager();
            $resultSearch = $dbManager->searchUac(trim($user), trim($psw));
            return $resultSearch;
        }
    }

    public static function setUacSession($resulCheckUac){
        unset($resulCheckUac['psw_utente']);
        unset($resulCheckUac['psw_utente_chiaro']);
        new driverUacModel($resulCheckUac);
    }

    public static function getUacSession(){
       return driverUacModel::getUacSession();
    }

    public static function getHome($id_uac){
        //$uac = driverUacController::getUacSession();
        $ruoli_uac = self::getRuoliUac($id_uac);
        $home = $ruoli_uac[0]['home'];
        return $home;
    }

    public static function getRoutesUac($id_iuac){
        $uacDbManager =  new driverUacDbManager();
        $routesUac = $uacDbManager->getRouteByUac($id_iuac);
        //accodo route standard
        $std_routes =  driverRouteConfig::defaultRoutes();
        foreach($std_routes as $one_route){
            $routesUac[] = $one_route;
        }

        return $routesUac;
    }

    public static function getRuoliUac($id_iuac){
        $uacDbManager =  new driverUacDbManager();
        return $ruoliUac = $uacDbManager->getRuoloByUac($id_iuac);
    }

    public static function getMenuUac($id_iuac){
        $uacDbManager =  new driverUacDbManager();
        $itemList = $uacDbManager->getMenuByUac($id_iuac);
        $menuUac = array();
        $count = 0;
        $id_temp = 0;

        foreach($itemList as $key => $val){
            if($itemList[$key]['padre'] == 0){
                if($itemList[$key]['id'] != $id_temp){
                    $id_temp = $itemList[$key]['id'];
                    $count++;
                }
                $menuUac[$count] = $itemList[$key];
            }else{
                $menuUac[$count]['lista_figli'][] = $itemList[$key];
                $menuUac[$count]['prole'] = 1;
            }
        }

        return $menuUac;
    }


}