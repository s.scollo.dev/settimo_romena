<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 31/01/2017
 * Time: 21:16
 */

//carico autoloader
require "driverStart.php";

use driver\driverStart;
use uac\driverUacController;
use twig\driverTwigView;
use route\driverRouteConfig;
use log\log;

driverStart::getInstance()->callLoaders();

function appStart(){
    $array_route = array();
    //intercetta regex
    $path_info = isset($_SERVER['PATH_INFO']) ? trim($_SERVER['PATH_INFO'], '/') : '/';

    //verifica se esiste uac valido (by uac)
    $uac = driverUacController::getUacSession();

    if($uac === FALSE){
        //invia a pagina di login
        if(!isset($path_info) || $path_info == '/'){
            $path_info = 'login';
        }
        //carico route di base
        $array_route = driverRouteConfig::startRoute();
        $res_disp = dispatch($array_route,$path_info);
        if($res_disp == false){
            //echo "pagina login";
            $res_disp = dispatch($array_route,'login');
        }
    }else{
        $uac = driverUacController::getUacSession();
        $array_route = driverUacController::getRoutesUac($uac->id_utente);

        $res_disp = dispatch($array_route,$path_info);
        if($res_disp == false){
            //echo "pagina inesistente";
            driverTwigView::show404();
        }
    }
}

function dispatch($array_route,$path_info){
    if($path_info == '/' || $path_info == ''){
        $uac = driverUacController::getUacSession();

        if($uac === FALSE){
            //punta a pagina home generica
            $path_info = driverRouteConfig::HOME_REDIRECT;
        }else{
            //punta a home relativa a ruolo
            // NB non si vede indirizzo
            $path_info = driverUacController::getHome($uac->id_utente);
        }

    }
    //creo log
    if($path_info != "logout" && $path_info != "login"){
        log::setLog("page",$path_info);
    }

    //creo backup db
    \db\driverDbHelper::backupSettimanale();

    //cicla route per trovare corrispondenza
    $parametri = array();
    $res_disp = false;
    foreach($array_route as $one_route){
        $regex = $one_route['regex'];
        $result_match = matchRegex($path_info,$regex);
        if(count($result_match) > 0){
            //trovato corrispondenza
            // estraggo lista parametri
            $count_param_route = preg_match_all('/\\:(\\w+)/', $regex, $array_param_route);
            if($count_param_route > 0){
                $array_valori = $result_match;
                unset($array_param_route[0]);
                sort($array_param_route);
                $array_chiavi = $array_param_route[0];
                //assegno valori a parametri
                for($c = 0; $c <$count_param_route; $c++){
                    $parametri[$array_chiavi[$c]] = $array_valori[$c+1];
                }
            }
            //richiamo classe e metodo
            $classe =  $one_route['classe'];
            $metodo =  $one_route['metodo'];
            if (class_exists($classe)) {
                $classe_go = new $classe;
                $reflector = new ReflectionObject($classe_go);
                if ($reflector->hasMethod($metodo)) {
                    $method_go = $reflector->getMethod($metodo);
                    $method_go->invoke($classe_go, $parametri);
                    $res_disp = true;
                }
            }
        }
    }
    return $res_disp;
}

function matchRegex($path_info,$regex)
{
    $tokens = null;
    preg_match_all('/\:(\w+)/', $regex, $tokens);

    $match = null;
    $regex = preg_replace('/\:(\w+)/', '(\w+)', str_replace('/', '\/', $regex));
    $regex = preg_replace('/\*/', '\/.*', $regex);
    preg_match('/^' . $regex . '$/', $path_info, $match);

    return $match;
}