<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 01/04/2018
 * Time: 11:39
 */

$versione = '3.0.9.5';


define("VERSIONE",$versione);


/*
 * Elenco versioni
 *
 * 08/01/2018   0.0        inizio lavori
 * 08/01/2018   0.1        pulizia codice
 * 08/01/2018   0.2        snellita session
 * 08/01/2018   0.3        criptata password
 * 09/01/2018   0.4        log utente
 * 16/01/2018   0.5        loggate pagine visitate
 * 16/02/2018   3.0.6      mappe, comuni, bug risolti
 * 17/02/2018   3.0.7      interfaccia guest
 * 18/02/2018   3.0.7.1    interfaccia guest
 * 18/02/2018   3.0.7.3    interfaccia guest single page
 * 07/03/2018   3.0.7.4    non suonare
 * 12/03/2018   3.0.7.5    modifica territorio al volo in comuni, corretto bug in torritori/dettaglio/nominativi che non verificava le coordinate gps
 * 12/03/2018   3.0.7.6    grafico percorrenza territori
 * 12/03/2018   3.0.7.7    inizio nuova versione guest
 * 12/03/2018   3.0.7.8    pulizia e correzione db nominativi, utenti,ruoli
 * 12/03/2018   3.0.7.9    info territori in lista utenti
 * 31/03/2018   3.0.8       gestione note guest e modifiche nominativi con note guest
 * 01/04/2018   3.0.8.1    inserito file versione
 * 01/04/2018   3.0.8.2    inseriento nuovo nominativo da guest e gestione lato admin
 * 01/04/2018   3.0.8.3    pulsante dove sono
 * 01/04/2018   3.0.8.4    correzione insert/modifica  nominativo  , problema con geocodifica
 * 25/05/2018   3.0.8.5    corretto pdf non suonare
 * 18/07/2018   3.0.8.6    correzione date e altri bug
 * 19/07/2018   3.0.8.7    inserita verifica data movimento inserita
 * 22/07/2018   3.0.8.8    debug check data, varie, inserimento stato forzato viste le note
 * 22/07/2018   3.0.8.9    debug pdf
 * 23/07/2018   3.0.9      stampa statistica percorrenza
 * 23/07/2018   3.0.9.1    modifica mappa nominativi in comuni
 * 11/09/2018   3.0.9.2    correzione bag apostrofo e log
 * 19/09/2018   3.0.9.3    correzione bag visualizzazione nominativi in guest #347
 * 23/01/2019   3.0.9.4	   correzione Lombardore, ora si visualizzano anche territori con 0 nominati a patto che il comune sia attivo	
 * 23/03/2019	3.0.9.5	   bugfix pdf non visitare - non romeno
 */