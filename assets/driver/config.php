<?php
/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 03/02/2017
 * Time: 08:53
 */

//contenitore di parametri di funzionamento

namespace driver;

include_once("versione.php");
class config
{
    const NAMEAPP = 'TERRITORI';
    const VERSION = VERSIONE;
    const DIR_TEMPLATE = 'template';

    const DOC_ROOT = DOC_ROOT;

    //DA DEFINIRE
    const HOME_DIRECT = ''; //inutile , la home � definita per ogni ruolo nel db
    const DIR_FILE_IN = 'repo_file_in';
    const DIR_FILE_OUT = 'repo_file_out';
    const DIR_IMG = '../repo_img';
    //const DIR_LOCAL = 'F:/\XAMPP/htdocs';
    const DIR_LOCAL = DOC_ROOT;

    //LOCALE
    //const DIR_DEFAULT = '/TERRITORI_3';
    //const PATH_ROOT = 'http://localhost';

    //PRODUZIONE
    const DIR_DEFAULT = '';
    const PATH_ROOT = 'https://www.settimo-romena.com';

///*TODO PER SPOSTAMENTI MODIFICARE ANCHE HTACCESS*/

}
