<?php

namespace driver\driverAutoloader;

/**
 * Class externalAssetAutoloader
 * @package DriverLib\driverAutoloader
 */

class externalAssetAutoloader
{
    /**
     * @var string
     */
    private $file_path;

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->file_path;
    }

    /**
     * @param string $file_path
     */
    public function setFilePath($file_path)
    {
        if (file_exists($file_path)) {
            $this->file_path = $file_path;
        }
        else {
            trigger_error("FILE OF EXTERNAL ASSETS NOT FOUND: ".basename($file_path),E_USER_ERROR);
        }
    }

    /**
     * @var string|array
     */
    private $callable;

    /**
     * @return string|array
     */
    public function getCallable()
    {
        return $this->callable;
    }

    /**
     * @param string|array $callable
     */
    public function setCallable($callable)
    {
        $this->callable = $callable;
    }

    public function invokeAutoloader()
    {
        require $this->getFilePath();

        if (is_array($this->getCallable())) {
            list($class,$method) = $this->getCallable();
            $reflector = new \ReflectionClass($class);
            if ($reflector->hasMethod($method)) {
                if ($reflector->getMethod($method)->isStatic()) {
                    call_user_func(array($class,$method));
                }
                else {
                    $caller = $reflector->newInstance();
                    $reflector->getMethod($method)->invoke($caller);
                    unset($caller);
                }
            }
            else {
                trigger_error("METHOD (ext autoloading) ".$method." NOT FOUND",E_USER_ERROR);
            }
            unset($reflector);
        }
        else {
            call_user_func($this->getCallable());
        }
    }
}