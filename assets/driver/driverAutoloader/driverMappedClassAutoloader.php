<?php

namespace driver\driverAutoloader;

class driverMappedClassAutoloader
{

    /**
     * @var array
     */
    private $mapped_classes = array();

    /**
     * @return array
     */
    public function getMappedClasses()
    {
        return $this->mapped_classes;
    }

    public function mapClass($class_name,$class_filename)
    {
        if (file_exists($class_filename)) {
            $this->mapped_classes[$class_name] = $class_filename;
        }
        else {
            trigger_error("Error while mapping Class: ".$class_name.", file ".$class_filename." doesn't exists.",E_USER_ERROR);
        }
    }

    public function loadMappedClass($class_name)
    {
        if (!class_exists($class_name,false) && array_key_exists($class_name,$this->getMappedClasses())) {
            $file_php =  $this->getMappedClasses()[$class_name];
            require $file_php;
            return true;
        }
        else {
            return false;
        }
    }


    public function register()
    {
        spl_autoload_register(array($this, 'loadMappedClass'));
    }

}