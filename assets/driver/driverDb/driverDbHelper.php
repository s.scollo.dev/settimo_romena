<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 02/02/2017
 * Time: 18:14
 */

namespace db;


class driverDbHelper
{
    public function simpleQuery($query){
        try{
            $cn = dbAppHandler::getCon();
            $obj = $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetch(PDO::FETCH_ASSOC);
            $obj->closeCursor();
            unset($obj,$cn);
            if (is_array($result) && count($result) > 0) {
                return $result;
            }
            else {
                return false;
            }
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function backupSettimanale(){

        //TODO SISTEMARE .. PRODUCE FILE VUOTI
        $anno =  date("Y");
        $settimana = date("W");

        $name_file_back_db = "backup_db_territori_".$anno."_".$settimana.".sql";

        $database = 'settimor06381';
        $user = 'settimor06381';
        $pass = 'sett53154';
        $host = 'sql.settimo-romena.com';

        if(!file_exists('file_db_bak/'.$name_file_back_db)){

            //repo_file_db_backup
            $dir = "repo_file_db_backup/";
            $path = $dir.$name_file_back_db;

            //VERSIONE 1
            exec("mysqldump -u=$user --password=$pass --host=$host $database > $path");

            //VERSIONE 2
            //$command = "mysqldump --opt -h $host -u $user -p $pass $database  > $path";
            //exec($command);
        }

    }
}

