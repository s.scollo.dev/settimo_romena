<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 02/02/2017
 * Time: 18:14
 */

namespace db;


class driverDbHelper
{
    public function simpleQuery($query){
        try{
            $cn = dbAppHandler::getCon();
            $obj = $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetch(PDO::FETCH_ASSOC);
            $obj->closeCursor();
            unset($obj,$cn);
            if (is_array($result) && count($result) > 0) {
                return $result;
            }
            else {
                return false;
            }
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }
}