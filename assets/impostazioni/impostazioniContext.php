<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 03/11/2017
 * Time: 15:31
 */

namespace impostazioni;
use twig\driverTwigContext;

class impostazioniContext extends driverTwigContext
{
    public $alert1_out;
    public $scadenza_out;
    public $data_start_grafico;
    public $mappa_marker;
    public $mappa_confini;
}