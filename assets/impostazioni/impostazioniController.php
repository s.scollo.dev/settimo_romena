<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 03/11/2017
 * Time: 15:29
 */

namespace impostazioni;
use helper\helpDate;

class impostazioniController
{
    public function pageImpostazioni(){
        $context = new impostazioniContext();
        $impostazioni = impostazioniDbManager::getImpostazioni();
        $context->alert1_out = $impostazioni['alert1_out'];
        $context->scadenza_out = $impostazioni['scadenza_out'];
        $context->start_count_lavorazione = $impostazioni['start_count_lavorazione'];
        $context->data_start_grafico = helpDate::data_dash2slash($impostazioni['data_start_grafico']);
        $context->mappa_marker = $impostazioni['mappa_marker'];
        $context->mappa_confini = $impostazioni['mappa_confini'];

        $view = new impostazioniView();
        $view->showImpostazioni($context);
    }

    public function salvaImpostazioni(){
        $check = true;
        $info = array();
        if(isset($_POST['alert1_out']) && trim($_POST['alert1_out']) != ''){
            $info['alert1_out'] = $_POST['alert1_out'];
        }else{
            $check = false;
        }
        if(isset($_POST['scadenza_out']) && trim($_POST['scadenza_out']) != ''){
            $info['scadenza_out'] = $_POST['scadenza_out'];
        }else{
            $check = false;
        }
        if(isset($_POST['data_start_grafico']) && trim($_POST['data_start_grafico']) != ''){
            $info['data_start_grafico'] = helpDate::data_slash2dash($_POST['data_start_grafico']);
        }else{
            $check = false;
        }
        if($check){
            if(impostazioniDbManager::salvaImpostazioni($info)){
                $resp = array("esito" => "OK", "msg" => "Operazione effettuata");
            }else{
                $resp = array("esito" => "KO", "msg" => "Operazione fallita");
            }
        }else{
            $resp = array("esito" => "KO", "msg" => "Alcuni valori mancano");
        }

        echo json_encode($resp);
    }

    public static function getImpostazioni(){
        return impostazioniDbManager::getImpostazioni();
    }

    public static function getImpostazioniByKey($key){
        $result = impostazioniDbManager::getImpostazioniByKey($key);
        return $result[$key];
    }
}