<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 03/11/2017
 * Time: 15:29
 */

namespace impostazioni;

use db\driverDbHandler;

class impostazioniDbManager
{
    public static function getImpostazioni(){
        $query = "SELECT * FROM impostazioni";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function salvaImpostazioni($info){

        if(isset($info['alert1_out']) && $info['alert1_out']!= ''){
            $key_val[] =  "alert1_out=".$info['alert1_out'];
        }
        if(isset($info['scadenza_out']) && $info['scadenza_out']!= ''){
            $key_val[] =  "scadenza_out=".$info['scadenza_out'];
        }
        if(isset($info['data_start_grafico']) && $info['data_start_grafico']!= ''){
            $key_val[] =  "data_start_grafico='".$info['data_start_grafico']."'";
        }
        $key_val_string = implode(", ",$key_val);
        $query = "UPDATE impostazioni SET ".$key_val_string." WHERE id_impostazione = 1";
        try{
            $cn = driverDbHandler::getCon();
            //$cn->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $stmt->closeCursor();
            unset($stmt,$cn);
            return true;
        } catch (PDOException $e){
            // print_r($e->getMessage(),E_USER_ERROR);
            return false;
        }
    }

    public static function getImpostazioniByKey($key){
        $query = "SELECT $key FROM impostazioni";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }
}