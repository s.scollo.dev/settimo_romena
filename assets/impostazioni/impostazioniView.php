<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 03/11/2017
 * Time: 15:31
 */

namespace impostazioni;

use twig\driverTwigContext;
use twig\driverTwigView;

class impostazioniView
{
    public function showImpostazioni($context)
    {
        driverTwigView::show('impostazioni/impostazioni.twig',$context);
    }
}