<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 16/01/2018
 * Time: 09:16
 */

namespace log;

use db\driverDbHandler;
use dtjssp\DataTableJsServerSideProcessingManagerMySQL;

class logController
{
    public function pageLista(){
        $view = new logView();
        $view->showList();
    }

    public function  loadLista(){
        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array( 'db' => 'data_log',  'dt' => 'data_log' ),
            array( 'db' => 'operazione',  'dt' => 'operazione' ),
            array( 'db' => 'parametro',  'dt' => 'parametro' ),
            array( 'db' => 'id_utente',  'dt' => 'id_utente' ),
            array( 'db' => 'nome_utente',  'dt' => 'nome_utente' ),
            array( 'db' => 'cognome_utente',  'dt' => 'cognome_utente' ),
            array( 'db' => 'user_utente',  'dt' => 'user_utente' ),
            array( 'db' => 'psw_utente',  'dt' => 'psw_utente' ),
            array( 'db' => 'psw_utente_chiaro',  'dt' => 'psw_utente_chiaro' ),
        );
        $res = DataTableJsServerSideProcessingManagerMySQL::simple( $_POST, $sql_details, '_v_log', 'id_log', $columns );

        echo json_encode($res);
    }
}