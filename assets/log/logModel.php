<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 09/01/2018
 * Time: 10:32
 */

namespace log;


use uac\driverUacController;

class logModel
{
    public $id_user;
    public $ip;
    public $operazione;
    public $parametro;

    public function __construct($operazione, $parametro = null){
        $uac = driverUacController::getUacSession();
        if($uac){
            $this->id_user = $uac->id_utente;
        }


        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->operazione = trim($operazione);
        $this->parametro = trim($parametro);

        return $this;
    }
    /*
     * OPERAZIONI LOG
     * login
     * logout
     * page
     * ...
     */
}