<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 09/01/2018
 * Time: 10:32
 */

namespace log;


class log
{
    public static function setLog($operazione,$parametro = null){
        $log = new logModel($operazione,$parametro);


        //elenco pagine da ignorare
        $ignora = array(
            "log/lista",
            "log/load_lista",
            "utente/lista",
            "utente/load_lista",
            "utente/all",
            "territorio/lista",
            "territorio/load_lista"
        );
        if(!in_array($parametro,$ignora)){
            //TODO CONSIDERARE ULTERIORI ELABORAZIONI
            logDbManager::saveLog($log);
        }


        //elenco pagine da tracciare
        $considera = array();
        if(in_array($parametro,$considera)){
            //TODO CONSIDERARE ULTERIORI ELABORAZIONI
            logDbManager::saveLog($log);
        }

    }


}