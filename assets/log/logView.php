<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 16/01/2018
 * Time: 09:14
 */

namespace log;
use twig\driverTwigContext;
use twig\driverTwigView;

class logView
{
    public function showList()
    {
        $context = new driverTwigContext();
        driverTwigView::show('log/lista_log.twig',$context);
    }

}