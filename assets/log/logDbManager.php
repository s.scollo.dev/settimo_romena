<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 09/01/2018
 * Time: 10:32
 */

namespace log;


use db\driverDbHandler;

class logDbManager
{

    public static function saveLog($log){
        if ($log instanceof logModel) {
            //inserisco log solo se id_user � valorizzato
            if($log->id_user){
                $id_user = $log->id_user;
                $ip = $log->ip;
                $operazione = $log->operazione;
                if($log->parametro){
                    $parametro = ",parametro = '$log->parametro'";
                }else{
                    $parametro = "";
                }
                //salvo log

                $query = "
                    INSERT INTO
                     _utente_log
                     SET
                     id_utente = $id_user,
                     ip_log = '$ip',
                     operazione = '$operazione'
                     $parametro
                     ";
                try{
                    $cn = driverDbHandler::getConGui();
                    $obj= $cn->prepare($query);
                    $obj->execute();
                    return true;
                } catch (PDOException $e){
                    trigger_error($e->getMessage(),E_USER_ERROR);
                }
            }

        }
    }
}