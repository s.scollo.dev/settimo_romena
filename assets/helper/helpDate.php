<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 12/02/2017
 * Time: 10:13
 */

namespace helper;


class helpDate
{
   /* const GIORNI = array(
        array(
            "id_giorno" => 1,
            "nome_giorno" => "lunedi"
        ),
        array(
            "id_giorno" => 2,
            "nome_giorno" => "martedi"
        ),
        array(
            "id_giorno" => 3,
            "nome_giorno" => "mercoledi"
        ),
        array(
            "id_giorno" => 4,
            "nome_giorno" => "giovedi"
        ),
        array(
            "id_giorno" => 5,
            "nome_giorno" => "venerdi"
        ),
        array(
            "id_giorno" => 6,
            "nome_giorno" => "sabato"
        ),
        array(
            "id_giorno" => 7,
            "nome_giorno" => "domenica"
        )
    );*/

    public static function getGiorniSettimana(){
        $giorni = array(
            array(
                "id_giorno" => 1,
                "nome_giorno" => "lunedi"
            ),
            array(
                "id_giorno" => 2,
                "nome_giorno" => "martedi"
            ),
            array(
                "id_giorno" => 3,
                "nome_giorno" => "mercoledi"
            ),
            array(
                "id_giorno" => 4,
                "nome_giorno" => "giovedi"
            ),
            array(
                "id_giorno" => 5,
                "nome_giorno" => "venerdi"
            ),
            array(
                "id_giorno" => 6,
                "nome_giorno" => "sabato"
            ),
            array(
                "id_giorno" => 7,
                "nome_giorno" => "domenica"
            )
        );
        return $giorni;
    }

    public static function data_dash2slash($data_dash){
        list($y, $m, $d) = explode("-",trim($data_dash));
        $data_slash = $d."/".$m."/".$y;
        return $data_slash;
    }

    public static function data_slash2dash($data_slash){
        //$data = \DateTime::createFromFormat('d/m/Y', $data_slash);
        //$data_dash = $data->format('Y-m-d');
        list($d, $m, $y) = explode("/",$data_slash);
        $data_dash = $y."-".$m."-".$d;
        return $data_dash;
    }

    public static function ora_simple($ora_complex){
        $ora = \DateTime::createFromFormat('H:i:s', $ora_complex);
        $ora_simple = $ora->format('H:i');
        return $ora_simple;
    }
    public static function ora_complex($ora_simple){
        $ora = \DateTime::createFromFormat('H:i', $ora_simple);
        $ora_complex = $ora->format('H:i:s');
        return $ora_complex;
    }

    public static function getGiorno($id_giorno){
        $array_giorni = help::getGiorniSettimana();
        foreach($array_giorni as $key => $val){
            $id = $array_giorni[$key]['id_giorno'];
            if($id == $id_giorno){
                return $array_giorni[$key]['nome_giorno'];
                break;
            }
        }
    }

    public static function myDateDiff($start, $end){
        $start = new \DateTime($start);
        $end = new \DateTime($end);
        $intervallo = $start->diff($end);
        $diff = $intervallo->format("%d");
        $diff_m = $intervallo->format("%m");
        if($diff_m > 0){
            $diff = 1;
        }
        return $diff;
    }

    public static function checkDataRange($date1, $date2){
        $start = new \DateTime($date1);
        $end = new \DateTime($date2);
        $intervallo = $start->diff($end);
        $invert = $intervallo->invert;
        if($invert == 0){
            //intervallo valido
            return true;
        }else{
            //date invertite
            return false;
        }
    }

}