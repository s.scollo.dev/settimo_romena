<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 03/08/2017
 * Time: 08:40
 */

namespace helper;


class helpString
{
    public static function truncate($stringa, $max_char, $holder="...", $tag_ammessi = ""){
            if(strlen($stringa)>$max_char){
                $stringa_tagliata=substr($stringa, 0,$max_char);
                $last_space=strrpos($stringa_tagliata," ");
                $stringa_ok=substr($stringa_tagliata, 0,$last_space);
                return strip_tags(trim($stringa_ok), $tag_ammessi).$holder;
            }else{
                return strip_tags(trim($stringa), $tag_ammessi);
            }
    }

    public static function clearString($txt){
        $txt = str_replace("' "," ",$txt);
        //$txt = str_replace("'","",$txt);
        return $txt;
    }
}