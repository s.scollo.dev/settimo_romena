<?php


/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace movimento;
use db\driverDbHandler;
use dtjssp\DataTableJsServerSideProcessingManagerMySQL;
use helper\helpDate;


class movimentoController
{

    public function pageLista(){
        $view = new movimentoView();
        $view->showList();
    }

    //dettagli del job archiviato
    public function loadListaMovimentiUtente($par)
    {
        $id_utente = $par['id_utente'];
        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array(
                'db'        => 'data_out',
                'dt'        => 'data_out',
                'formatter' => function( $d, $row ) {
                    return date( 'd/m/Y', strtotime($d));
                }
            ),            array(
                'db'        => 'data_in',
                'dt'        => 'data_in',
                'formatter' => function( $d, $row ) {
                    return date( 'd/m/Y', strtotime($d));
                }
            ),
            array('db' => 'nome_territorio', 'dt' => 'nome_territorio'),
            array('db' => 'nome_lavorazione', 'dt' => 'nome_lavorazione')
        );

        $whereResult = "id_utente = $id_utente";
        $whereResult2 = "";
        $res = DataTableJsServerSideProcessingManagerMySQL::complex($_POST, $sql_details, 'v_movimenti_territori_proclamatori', 'id_movimento', $columns, $whereResult, $whereResult2);
        echo json_encode($res);
    }

    public function loadListaAssegnati()
    {
        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array('db' => 'id_movimento', 'dt' => 'id_movimento'),
            array(
                'db'        => 'data_out',
                'dt'        => 'data_out',
                'formatter' => function( $d, $row ) {
                    return date( 'd/m/Y', strtotime($d));
                }
            ),            array(
                'db'        => 'data_in',
                'dt'        => 'data_in',
                'formatter' => function( $d, $row ) {
                    return date( 'd/m/Y', strtotime($d));
                }
            )
        );

        $whereResult = "";
        $whereResult2 = "";
        $res = DataTableJsServerSideProcessingManagerMySQL::complex($_POST, $sql_details, 'movimenti', 'id_movimento', $columns, $whereResult, $whereResult2);
        echo json_encode($res);
    }

    public function loadListaByTerritorio($par){
        {
            $id_territorio = $par['id_territorio'];
            $sql_details = driverDbHandler::$DB_INFO;
            $columns = array(
                array('db' => 'id_movimento', 'dt' => 'id_movimento'),
                array('db' => 'nome_utente', 'dt' => 'nome_utente'),
                array('db' => 'cognome_utente', 'dt' => 'cognome_utente'),
                array(
                    'db'        => 'data_out',
                    'dt'        => 'data_out',
                    'formatter' => function( $d, $row ) {
                        return date( 'd/m/Y', strtotime($d));
                    }
                ),            array(
                    'db'        => 'data_in',
                    'dt'        => 'data_in',
                    'formatter' => function( $d, $row ) {
                        return date( 'd/m/Y', strtotime($d));
                    }
                ),
                array('db' => 'nome_lavorazione', 'dt' => 'nome_lavorazione')
            );

            $whereResult = "id_territorio = $id_territorio";
            $whereResult2 = "";
            $res = DataTableJsServerSideProcessingManagerMySQL::complex($_POST, $sql_details, 'v_movimenti_territori_utenti_lavorazioni', 'id_movimento', $columns, $whereResult, $whereResult2);
            echo json_encode($res);
        }

    }

    public static function getLastMovimentoByTerritorio($id_territorio){
        return movimentoDbManager::getLastMovimentoByTerritorio($id_territorio);
    }

    public static function getMovimentoById($par){
        $id_movimento = $par['id_movimento'];
        $movimento = movimentoDbManager::getMovimentoById($id_movimento);
        $movimento['data_in_slash'] = helpDate::data_dash2slash($movimento['data_in']);
        $movimento['data_out_slash'] = helpDate::data_dash2slash($movimento['data_out']);
        echo json_encode($movimento);
    }

    public static function cancellaMovimentoById($par){
        $id_movimento = $par['id_movimento'];
        $esito = movimentoDbManager::cancellaMovimentoById($id_movimento);
        if($esito){
            $response = array("esito" => "OK", "msg" => "Cancellazione effettuta con successo");

        }else{
            $response = array("esito" => "KO", "msg" => "Cancellazione fallita");
        }
        echo json_encode($response);
    }

    public function salvaMovimento(){
        if(isset($_POST['id_movimento']) && $_POST['id_movimento']!= ''){
            $info_movimento['id_movimento'] = $_POST['id_movimento'];
        }
        if(isset($_POST['id_proclamatore']) && $_POST['id_proclamatore']!= ''){
            $info_movimento['id_proclamatore'] = $_POST['id_proclamatore'];
        }
        if(isset($_POST['id_territorio']) && $_POST['id_territorio']!= ''){
            $info_movimento['id_territorio'] = $_POST['id_territorio'];
        }
        if(isset($_POST['data_out']) && $_POST['data_out']!= ''){
            $info_movimento['data_out'] = helpDate::data_slash2dash($_POST['data_out']);
            $check_data = self::checkUscita($info_movimento['id_territorio'], $info_movimento['data_out']);
        }
        if(isset($_POST['data_in']) && $_POST['data_in']!= ''){
            $info_movimento['data_in'] = helpDate::data_slash2dash($_POST['data_in']);
            $check_data = self::checkRientro($info_movimento['id_territorio'], $info_movimento['data_in']);

        }
        if(isset($_POST['lavorazione']) && $_POST['lavorazione']!= ''){
            $info_movimento['lavorazione'] = $_POST['lavorazione'];
        }



        if($check_data){
            if(movimentoDbManager::salvaMovimento($info_movimento)){
                $resp = json_encode(array("esito" => "OK", "msg" => "Operazione effettuata"));
            }else{
                $resp = json_encode(array("esito" => "KO", "msg" => "Operazione fallita"));
            }
        }else{
            $resp = json_encode(array("esito" => "KO", "msg" => "Qualcosa non va nelle date"));
        }

        echo $resp;

    }

    public function checkUscita($id_terr,$data_out){
        $esit = false;
        //cerco ultimo rientro
        $last_m = movimentoController::getLastMovimentoByTerritorio($id_terr);
        $ultimo_rientro = $last_m['max_in'];
        if($ultimo_rientro != ""){
            $ultimo_rientro = date_create($ultimo_rientro);
            $data_out = date_create($data_out); //oggetto data confronto
            //DIFFERENZA DATE
            $interval = date_diff($ultimo_rientro,$data_out);
            //DIFFERENZA IN GIORNI
            $total = $interval->format("%R%a"); //differenza in giorni da uscita
            if($total > 0){
                //echo 'date ok';
                $esit = true;
            }
            return $esit;
        }
        return $esit;
    }

    public function checkRientro($id_terr,$data_in){
        $esit = false;
        //cerco ultima assegnazione
        $last_m = movimentoController::getLastMovimentoByTerritorio($id_terr);
        $ultima_assegnazione = $last_m['max_out'];
        if($ultima_assegnazione != ""){
            $data_in = date_create($data_in);
            $ultima_assegnazione = date_create($ultima_assegnazione); //oggetto data confronto
            //DIFFERENZA DATE
            $interval = date_diff($ultima_assegnazione,$data_in);
            //DIFFERENZA IN GIORNI
            $total = $interval->format("%R%a"); //differenza in giorni da uscita
            if($total >= 0){
                //echo 'date ok';
                $esit = true;
            }
        }
        return $esit;

    }
}