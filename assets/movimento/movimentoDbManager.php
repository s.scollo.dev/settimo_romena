<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace movimento;

use db\driverDbHandler;

class movimentoDbManager
{

    public static function getMovimentiByTerritorio($id_territorio){
        $query = "SELECT * FROM  v_movimenti_territori_proclamatori WHERE id_territorio = $id_territorio ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            if (is_array($result) && count($result) > 0) {
                return $result;
            }
            else {
                return false;
            }
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function getLastMovimentoByTerritorio($id_territorio){
        $query = "
        SELECT
            id_movimento,
            data_out as max_out,
            data_in as max_in,
            nome_utente,
            cognome_utente
        FROM
            v_movimenti_territori_proclamatori
        WHERE
            id_territorio = $id_territorio
        ORDER BY
            id_movimento DESC";

        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            if (is_array($result) && count($result) > 0) {
                return $result;
            }
            else {
                return false;
            }
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function getMovimentoById($id_movimento){
        $query = "SELECT * FROM movimenti
                    JOIN territori ON territori.id_territorio = movimenti.id_territorio
                    JOIN _utente ON _utente.id_utente = movimenti.id_proclamatore
                    JOIN lavorazione ON lavorazione.id_lavorazione = movimenti.id_lavorazione
                    WHERE id_movimento = $id_movimento ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function getMovimentoiByUtente($id_utente){
        $query = "SELECT * FROM v_movimenti_territori_proclamatori WHERE id_utente = $id_utente ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function cancellaMovimentoById($id_movimento){
        $query = "DELETE FROM movimenti WHERE id_movimento = $id_movimento ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $stmt->closeCursor();
            unset($stmt,$cn);
            return true;
        } catch (PDOException $e){
            return false;
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function salvaMovimento($info){
        $key_val = array();
        if(isset($info['id_movimento']) && $info['id_movimento']!= ''){
            $query = "UPDATE movimenti SET ";
            $where = " WHERE id_movimento = ".$info['id_movimento'];
        }else{
            $query = "INSERT INTO movimenti SET ";
            $where = "";
        }
        if(isset($info['id_proclamatore']) && $info['id_proclamatore']!= ''){
            $key_val[] =  "id_proclamatore=".trim($info['id_proclamatore']);
        }
        if(isset($info['data_out']) && $info['data_out']!= ''){
            $key_val[] =  "data_out='".$info['data_out']."'";
        }
        if(isset($info['data_in']) && $info['data_in']!= ''){
            $key_val[] =  "data_in='".trim($info['data_in'])."'";
        }
        if(isset($info['lavorazione']) && $info['lavorazione']!= ''){
            $key_val[] =  "id_lavorazione=".trim($info['lavorazione']);
        }
        if(isset($info['id_territorio']) && $info['id_territorio']!= ''){
            $key_val[] =  "id_territorio=".trim($info['id_territorio']);
        }
        $key_val_string = implode(", ",$key_val);
        $query = $query.$key_val_string.$where;
        try{
            $cn = driverDbHandler::getCon();
            //$cn->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $stmt->closeCursor();
            unset($stmt,$cn);
            return true;
        } catch (PDOException $e){
            // print_r($e->getMessage(),E_USER_ERROR);
            return false;
        }
    }


}