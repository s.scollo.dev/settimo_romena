<?php
/**
 * Created by PhpStorm.
 * User: gabry
 * Date: 07/10/2016
 * Time: 14:32
 */

namespace dtjssp;


class DataTableJsServerSideProcessingManager
{
    /**
     * @var \PDO
     */
    protected $db_connection;

    /**
     * @return \PDO
     */
    public function getDbConnection()
    {
        return $this->db_connection;
    }

    /**
     * @param \PDO $db_connection
     */
    public function setDbConnection($db_connection)
    {
        $this->db_connection = $db_connection;
    }
}