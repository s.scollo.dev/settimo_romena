<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace territorio;
use twig\driverTwigContext;
use twig\driverTwigView;


class territorioView
{
    public function showList()
    {
        $context = new driverTwigContext();
        //per aggingere parametri  $context->nuovoParametro = 'valore';
        driverTwigView::show('territorio/lista_territorio.twig',$context);
    }

    public function showDettagliGenerale($context)
    {
        driverTwigView::show('territorio/dettagli_territorio_generale.twig',$context);
    }

    public function showDettagliMovimenti($context)
    {
        driverTwigView::show('territorio/dettagli_territorio_movimenti.twig',$context);
    }

    public function showDettagliNominativi($context)
    {
        driverTwigView::show('territorio/dettagli_territorio_nominativi.twig',$context);
    }

    public function showDettagliMappa($context)
    {
        driverTwigView::show('territorio/dettagli_territorio_mappa.twig',$context);
    }
}