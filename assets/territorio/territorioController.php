<?php


/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace territorio;
use comune\comuneDbManager;
use db\driverDbHandler;
use dtjssp\DataTableJsServerSideProcessingManagerMySQL;
use helper\helpDate;
use impostazioni\impostazioniController;
use movimento\movimentoController;
use nominativo\nominativoController;
use twig\driverTwigContext;


class territorioController
{

    public function pageLista(){
        $context = new territorioContext();
        $view = new territorioView();
        $view->showList($context);
    }

    public function loadLista()
    {
        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array('db' => 'id_territorio', 'dt' => 'id_territorio'),
            array('db' => 'nome_territorio', 'dt' => 'nome_territorio'),
            array('db' => 'nominativi', 'dt' => 'nominativi')
        );
        $whereResult = "";
        $whereResult2 = "";
        $impostazioni = impostazioniController::getImpostazioni();
        $alert_out = $impostazioni['alert1_out'];
        $scadenza_out = $impostazioni['scadenza_out'];
        $res = DataTableJsServerSideProcessingManagerMySQL::complex($_POST, $sql_details, 'v_territori_nominativi', 'id_territorio', $columns, $whereResult, $whereResult2);
        $data = $res['data'] ;
        foreach($data as $k => $v){
            $last_m = movimentoController::getLastMovimentoByTerritorio($data[$k]['id_territorio']);
            $ultima_assegnazione = $last_m['max_out'];
            $ultimo_rientro = $last_m['max_in'];
            if($ultima_assegnazione >= $ultimo_rientro){
                //territorio in lavorazione
                $stato = "lavorazione";
                $data_riff = $ultima_assegnazione;
            }else{
                //territorio in reparto
                $stato = "reparto";
                $data_riff = $ultimo_rientro;
            }

            //ELABORAZIONE STATO TERRITORIO
            //OGGETTO DATA OGGI
            $today = date("Y-m-d");
            $today = date_create($today);
            //OGGETTO DATA RIFERIMENTO (BY ULTIMO MOVIMENTO)
            $data_out = date_create($data_riff); //oggetto data confronto
            //DIFFERENZA DATE
            $interval = date_diff($data_out,$today);
            //DIFFERENZA IN GIORNI
             $total = $interval->format("%R%a"); //differenza in giorni da uscita



            //DEFINIZIONE SCADENZA TERRITORIO
            //0 : territorio in reparto
            //1 : territorio assegnato
            //2 : territorio assegnato in scadenza
            //3 : territorio assegnato scaduto

            if($stato == "reparto"){
                //TERRITORIO IN REPARTO
                $scadenza = 0;
                //calcolo giorni
                $giorni = str_replace("+","",$total);
            }else{
                //calcolo giorni
                $giorni = $total - $scadenza_out;
                if($total  > $scadenza_out){
                    //TERRITORIO ASSEGNATO E SCADUTO
                    $scadenza = 3;
                }else{
                    if($total  > $alert_out){
                        //TERRITORIO ASSEGNATO E IN SCADENZA
                        $scadenza = 2;
                    }else{
                        //TERRITORIO ASSEGNATO E OK
                        $scadenza = 1;
                    }
                }
            }

            $ultima_assegnazione_out = helpDate::data_dash2slash($ultima_assegnazione);
            if(helpDate::data_dash2slash($ultimo_rientro) == "//"){
                $ultimo_rientro_out = "";
            }else{
                $ultimo_rientro_out = helpDate::data_dash2slash($ultimo_rientro);
            }
            //DATI IN USCITA
            $data[$k]['giorni'] = $giorni;
            $data[$k]['stato'] = $stato;
            $data[$k]['scadenza'] = $scadenza;
            $data[$k]['ultima_uscita'] = $ultima_assegnazione_out;
            $data[$k]['ultimo_rientro'] = $ultimo_rientro_out;
            $data[$k]['proclamatore'] = $last_m['nome_utente']." ".$last_m['cognome_utente'];

        }
        $res['data'] = $data;

        echo json_encode($res);
    }

    public function loadListaAssegnati()
    {
        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array('db' => 'id_territorio', 'dt' => 'id_territorio'),
            array('db' => 'nome_territorio', 'dt' => 'nome_territorio'),
            array('db' => 'nominativi', 'dt' => 'nominativi')
        );
        $whereResult = "";
        $whereResult2 = "";
        $res = DataTableJsServerSideProcessingManagerMySQL::complex($_POST, $sql_details, 'v_territori_nominativi', 'id_territorio', $columns, $whereResult, $whereResult2);
        $data = $res['data'] ;
        foreach($data as $k => $v){

        }
        $res['data'] = $data;
        echo json_encode($res);
    }

    public function pageDettagli($par){

        $context = new driverTwigContext();
        $id_territorio = $par['id_territorio'];
        $pagina = $par['pagina'];

        $info_comuni = territorioDbManager::getComuniByTerritorio($id_territorio);
        $nome_comune =  $info_comuni['nome_comune'];
        $id_comune =  $info_comuni['id_comune'];
        $info_territorio = territorioDbManager::getTerritorioById($id_territorio);
        $context->nome_territorio = $info_territorio['nome_territorio'];
        $context->url_territorio = urlencode($info_territorio['nome_territorio']);
        $context->id_territorio = $info_territorio['id_territorio'];
        $context->id_comune = $id_comune;

        $view = new territorioView();
        switch ($pagina) {
            case 'generale':
                //recupero dati generali territorio
                $view->showDettagliGenerale($context);
                break;
            case 'nominativi':
                //recupero nominativi territorio
                $view->showDettagliNominativi($context);
                break;
            case 'mappa':
                //recupero mappa territorio
                $view->showDettagliMappa($context);
                break;
            case 'movimenti':
                //recupero movimenti territorio
                $view->showDettagliMovimenti($context);
                break;
        }


    }

    //INFO PER MARKER SU MAPPA
    public function loadListaMarker($par){
        $info = array();
        $id_territorio = $par['id_territorio'];
        $info_nominativi = nominativoController::getNominativiByTerritorio($id_territorio);
        $latitudine_centro_mappa = 0;
        $longitudine_centro_mappa = 0;
        foreach($info_nominativi as $k => $v){
            $info_comune = comuneDbManager::getComuneById($info_nominativi[$k]['id_comune']);
            $info_nominativi[$k]['nome_comune'] = $info_comune['nome_comune'];

            //CALCOLO CENTRO MAPPA
            if($latitudine_centro_mappa == 0){
                $latitudine_centro_mappa = $info_nominativi[$k]['latitudine'];
            }else{
                $latitudine_centro_mappa = ($latitudine_centro_mappa+$info_nominativi[$k]['latitudine'])/2;
            }
            if($longitudine_centro_mappa == 0){
                $longitudine_centro_mappa = $info_nominativi[$k]['longitudine'];
            }else{
                $longitudinee_centro_mappa = ($longitudine_centro_mappa+$info_nominativi[$k]['longitudine'])/2;
            }

        }
        $centro_mappa = array(
            "latitudine" => $latitudine_centro_mappa,
            "longitudine" => $longitudine_centro_mappa
        );

        $info = array(
            "info_nominativi" =>   $info_nominativi,
            "centro_mappa" =>$centro_mappa
        );
        echo json_encode($info);
    }

    public static function getTerritorioById($id_territorio){
        return territorioDbManager::getTerritorioById($id_territorio);
    }

    public static function getTerritorioByUtente($id_utente){
        //TODO COLORE IN BASE A SCADENZA
        return territorioDbManager::getTerritorioByUtente($id_utente);
    }

    public function generaCsvNominativi($par){
        $oggi = date("d/m/y");
        include_once("ext_assets/fpdf181/fpdf.php");
        $id_territorio = $par['id_territorio'];
        $info_territorio = territorioDbManager::getTerritorioById($id_territorio);
        //$nominativi = nominativoController::getNominativi($id_territorio);
        $nominativi = nominativoController::getNominativiByTerritorio($id_territorio);

        //LARGHEZZA PAGINA LANDSCAPE : 296 (colonna 184)
        //ALTEZZA PAGINA LANDSCAPE : 205
        //COORDINATE SI CALCOLANO DALL'ANGOLO IN ALTO A SINISTRA

        $pdf = new \FPDF('L','mm','A4');
        $pdf->AddPage();
        $pdf->SetMargins(5, 5, 5);
        $pdf->SetAutoPageBreak(false, 0);

        $pdf->SetX(0);
        $pdf->SetY(0);


        $height_cell = 7;
        $x_colonna_2 = 153;
        $y_fine_pagina = 180;

        $y = $pdf->GetY();
        $n_pagina = 1;
        $col = 1;

        //INTESTAZIONE
        $pdf->SetFont('Arial','B',14);
        $nome_territorio = $info_territorio['nome_territorio'];
        $pdf->SetY(5);
        $pdf->SetX(5);
        $pdf->Cell(60,8,"Settimo T.se Romena",0,0);
        $pdf->Cell(75,8,$nome_territorio,0,0,"R");
        $pdf->SetFont('Arial','',10);
        //FINE INTESTAZIONE

        $x_start_lista = 16;
        $pdf->SetY($x_start_lista);

        foreach ($nominativi as $one) {
            $via = $one['via'];
            $civico = $one['civico'];
            $interno = $one['interno'];
            $coordinata = $one['coordinata'];
            $notes = $one['notes'];
            $status = "";
            if($one['status'] == 2){
                $status = "NON VISITARE ";
            }elseif($one['status'] == 1){
                $status = "NON ROMENO ";
            }

            $y = $pdf->GetY();
            $pdf->Cell(60,$height_cell,$via,1,0);
            $pdf->Cell(15,$height_cell,$civico,1,0);
            $pdf->Cell(10,$height_cell,$interno,1,0);
            $pdf->Cell(10,$height_cell,$coordinata,1,0);
            $pdf->SetFont('Arial','',8);
            $pdf->Cell(45,$height_cell,$status.$notes,1,1);
            $pdf->SetFont('Arial','',9);
            if($y >= $y_fine_pagina && $col == 1){
                $pdf->Cell(150,8,"pagina ".$n_pagina++." - stampato il ".$oggi,0,0,"C");

                //INTESTAZIONE
                $pdf->SetFont('Arial','B',14);
                $nome_territorio = $info_territorio['nome_territorio'];
                $pdf->SetY(5);
                $pdf->SetX($x_colonna_2);
                $pdf->Cell(60,8,"Settimo T.se Romena",0,0);
                $pdf->Cell(75,8,$nome_territorio,0,0,"R");
                $pdf->SetFont('Arial','',10);
                //FINE INTESTAZIONE

                $pdf->SetY($x_start_lista);
                $pdf->SetX($x_colonna_2);

                $col = 2;
            }elseif($y >= $y_fine_pagina && $col == 2){
                $pdf->SetX($x_colonna_2);
                $pdf->Cell(150,8,"pagina ".$n_pagina++." - stampato il ".$oggi,0,0,"C");

                $pdf->AddPage();

                //INTESTAZIONE
                $pdf->SetFont('Arial','B',14);
                $nome_territorio = $info_territorio['nome_territorio'];
                $pdf->SetY(5);
                $pdf->SetX(5);
                $pdf->Cell(60,8,"Settimo T.se Romena",0,0);
                $pdf->Cell(75,8,$nome_territorio,0,0,"R");
                $pdf->SetFont('Arial','',10);
                //FINE INTESTAZIONE

                $pdf->SetY($x_start_lista);
                $col = 1;

            }elseif($y < $y_fine_pagina && $col == 1){
                $col = 1;
            }elseif($y < $y_fine_pagina && $col == 2){
                $pdf->SetX($x_colonna_2);
                $col = 2;
            }
        }
        //PIE COLONNA
        $pdf->Cell(150,8,"pagina ".$n_pagina++." - stampato il ".$oggi,0,0,"C");




        $pdf->Output();
    }

    //INFO PER STATISTICA DATI PERCORRENZA
    public function getInfoPercorrenza($data_start, $data_end){
        return territorioDbManager::getInfoPercorrenza($data_start,$data_end);
    }
}