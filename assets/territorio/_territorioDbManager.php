<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace territorio;

use db\driverDbHandler;

class territorioDbManager
{
    const TAB_TERRITORIO = "territori";

    public static function getTerritorioById($id){
        $query = "SELECT * FROM  territori WHERE id_territorio = ? ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute(array($id));
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            if (is_array($result) && count($result) > 0) {
                return $result;
            }
            else {
                return false;
            }
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function getComuniByTerritorio($id_territorio){
        $query = "SELECT comuni.id_comune,comuni.nome_comune FROM comuni JOIN nominativi ON nominativi.id_comune=comuni.id_comune  WHERE nominativi.id_territorio = ? GROUP BY comuni.id_comune";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute(array($id_territorio));
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            if(isset($result[0])){
                return $result[0];
            }else{
                return false;
            }

        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }

    }

    public static function getTerritorioByUtente($id_utente){
        $query = "
            SELECT
              *
            FROM
              v_movimenti_territori_utenti_lavorazioni
            WHERE
            id_proclamatore=:id_utente AND
            data_in is null";
    //old data_in = '0000-00-00'";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute(
                array(
                    ":id_utente" => $id_utente
                ))
            ;
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }

    }

    public static function getInfoPercorrenza($data_start, $data_end){
        $query = "
          SELECT
            *
          FROM
            v_movimenti_territori_utenti_lavorazioni
          WHERE
            data_in BETWEEN :data_start AND :data_end;
          ORDER BY
            nome_territorio,
            data_out";
        try{
            $cn = driverDbHandler::getCon();
            $cn->quote($data_start);
            $cn->quote($data_end);
            $stmt = $cn->prepare($query);
            $stmt->execute(
                array(
                    ":data_start" => $data_start,
                    ":data_end" => $data_end,
                )
            );
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }


}