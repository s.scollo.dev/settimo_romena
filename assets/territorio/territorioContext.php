<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 08/02/2017
 * Time: 08:38
 */

namespace territorio;
use twig\driverTwigContext;

class territorioContext extends driverTwigContext
{
    public $id_territorio;
    public $comune;
    public $nominativi;
    public $stato;
    public $proclamatore;
}