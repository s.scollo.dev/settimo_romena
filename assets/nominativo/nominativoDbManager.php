<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace nominativo;

use db\driverDbHandler;
use uac\driverUacController;

class nominativoDbManager
{

    public static function getNominativoById($id){
        $query = "SELECT * FROM nominativi
                  JOIN comuni ON comuni.id_comune=nominativi.id_comune
                  JOIN territori ON territori.id_territorio=nominativi.id_territorio
                  WHERE id_nominativo = $id ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute(array($id));
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function cancellaNominativoById($id){
        $query = "DELETE FROM nominativi WHERE id_nominativo = $id ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $stmt->closeCursor();
            unset($stmt,$cn);
            return true;
        } catch (PDOException $e){
            return false;
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function updateCooGps($id_nominativo, $latitudine, $longitudine,$via){
        $query = "
          UPDATE
            nominativi
          SET
            latitudine = $latitudine,
            longitudine= $longitudine,
            via = '$via'
          WHERE id_nominativo =  $id_nominativo";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute(
            /*array(
                ":latitudine" => $latitudine,
                ":longitudine" => $longitudine,
                ":id_nominativo" => $id_nominativo
            )*/
            );
            $stmt->closeCursor();
            unset($stmt,$cn);
            return true;
        } catch (PDOException $e){
            return false;
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    
    public function salvaNominativo($info){
        $key_val = array();
        if(isset($info['id']) && $info['id']!= ''){
            $query = "UPDATE nominativi SET ";
            $where = " WHERE id_nominativo = ".$info['id'];
        }else{
            $query = "INSERT INTO nominativi SET ";
            $where = "";
        }
        if(isset($info['via']) && $info['via']!= ''){
            $key_val[] =  "via='".trim($info['via'])."'";
        }
        if(isset($info['civico']) && $info['civico']!= ''){
            $key_val[] =  "civico=".$info['civico'];
        }else{
            $key_val[] =  "civico='0'";
        }
        if(isset($info['interno']) && $info['interno']!= ''){
            $key_val[] =  "interno='".trim($info['interno'])."'";
        }else{
            $key_val[] =  "interno= ''";
        }
        if(isset($info['coordinata']) && $info['coordinata']!= ''){
            $key_val[] =  "coordinata='".trim($info['coordinata'])."'";
        }else{
            $key_val[] =  "coordinata='0'";
        }
        if(isset($info['note']) && $info['note']!= ''){
           $key_val[] =  "notes='".$info['note']."'";
        }else{
            $key_val[] =  "notes=''";
        }
        if(isset($info['status']) && $info['status']!= ''){
            $key_val[] =  "status=".$info['status'];
        }
        if(isset($info['id_comune']) && $info['id_comune']!= ''){
            $key_val[] =  "id_comune=".$info['id_comune'];
        }
        if(isset($info['id_territorio']) && $info['id_territorio']!= ''){
            $key_val[] =  "id_territorio=".$info['id_territorio'];
        }
        if(isset($info['latitudine']) && $info['latitudine'] != 0){
            $key_val[] =  "latitudine='".trim($info['latitudine'])."'";
        }else{
            $key_val[] =  "latitudine=0";
        }
        if(isset($info['longitudine']) && $info['longitudine'] != 0){
            $key_val[] =  "longitudine='".trim($info['longitudine'])."'";
        }else{
            $key_val[] =  "longitudine=0";
        }

        $key_val_string = implode(", ",$key_val);
        $query = $query.$key_val_string.$where;

        try{
            $cn = driverDbHandler::getCon();

            if(isset($info['id']) && $info['id']!= ''){
                $id_nominativo = $info['id'];
                //CANCELLO NOTE GUEST
                $query_delete = "
                DELETE FROM
                  guest_note_nominativi
                WHERE
                  id_nominativo = :id_nominativo";
                $stmt = $cn->prepare($query_delete);
                $stmt->execute(
                    array(":id_nominativo" => $id_nominativo)
                );
            }

            //$cn->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $id_nominativo = $cn->lastInsertId();
            unset($stmt,$cn);

            if($info['status'] == "4"){
                $uac = driverUacController::getUacSession();
                $id_guest = $uac->id_utente;
                SELF::saveGuestNotaNominativo($id_guest, $id_nominativo, "Nuovo nominativo");
            }
            return true;
        } catch (PDOException $e){
           // print_r($e->getMessage(),E_USER_ERROR);
            return false;
        }
    }

    public static function getNominativiByTerritorio($id_territorio){
        $query = "SELECT * FROM nominativi WHERE id_territorio = $id_territorio ORDER BY id_comune, via, civico, interno, coordinata ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function getNominativiByComune($id_comune){
        $query = "SELECT * FROM v_nominativi_territori_comuni WHERE id_comune = $id_comune ORDER BY id_territorio, via, civico, interno, coordinata ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function getNominativiByIndirizzo($id_comune,$via,$civico){
        $query = "
          SELECT
            *
          FROM
            nominativi
          WHERE
              id_comune = :id_comune AND
              via = :via AND
              civico = :civico
          ORDER BY
            id_territorio, via, civico, interno, coordinata ";
        try{
            $cn = driverDbHandler::getCon();
            $cn->quote($via);
            $cn->quote($civico);
            $stmt = $cn->prepare($query);
            $stmt->execute(
                array(
                    ":id_comune" => $id_comune,
                    ":via" => $via,
                    ":civico" => $civico
                )
            );
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function getAllNominativi(){
        $query = "SELECT * FROM v_nominativi_territori_comuni ORDER BY id_comune,via, civico, interno, coordinata ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function getNonSuonare(){
        $query = "SELECT * FROM v_nominativi_territori_comuni WHERE status <> 0 ORDER BY id_comune,via, civico, interno, coordinata ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            unset($stmt,$cn);
            return $result;
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function saveGuestNotaNominativo($id_guest, $id_nominativo, $nota){
        $query_delete = "
        DELETE FROM
          guest_note_nominativi
        WHERE
          id_nominativo = :id_nominativo";
        $query = "
          INSERT
            guest_note_nominativi
          SET
            id_guest = :id_guest,
            id_nominativo = :id_nominativo,
            nota = :nota
          ";
        try{
            $cn = driverDbHandler::getCon();
            //CANCELLO NOTA PRECEDENTE
            $stmt = $cn->prepare($query_delete);
            $stmt->execute(
                array(
                    ":id_nominativo" => $id_nominativo
                )
            );

            //INSERISCO NUOVA NOTA
            $cn->quote($nota);
            $stmt = $cn->prepare($query);
            $stmt->execute(
                array(
                    ":id_guest" => $id_guest,
                    ":id_nominativo" => $id_nominativo,
                    ":nota" => $nota
                )
            );
            $stmt->closeCursor();
            unset($stmt,$cn);
            return true;
        } catch (PDOException $e){
            return false;
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

    public static function updateTerritorio($id_nominativo, $id_territorio){
        $query = "
          UPDATE
            nominativi
          SET
            id_territorio = :id_territorio
          WHERE
            id_nominativo = :id_nominativo
          ";
        try{
            $cn = driverDbHandler::getCon();
            $stmt = $cn->prepare($query);
            $stmt->execute(
                array(
                    ":id_nominativo" => $id_nominativo,
                    ":id_territorio" => $id_territorio,
                )
            );
            $stmt->closeCursor();
            unset($stmt,$cn);
            return true;
        } catch (PDOException $e){
            return false;
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }

}