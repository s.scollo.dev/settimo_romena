<?php


/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace nominativo;
use db\driverDbHandler;
use dtjssp\DataTableJsServerSideProcessingManagerMySQL;
use helper\helpString;
use comune\comuneDbManager;
use uac\driverUacController;


class nominativoController
{

    public function pageLista(){
        $context = new nominativoContext();
        $view = new nominativoView();
        $view->showList($context);
    }

    public function pageListaNonSuonare(){
        $context = new nominativoContext();
        $view = new nominativoView();
        $view->showListNonSuonare($context);
    }

    public function loadListaNonSuonare()
    {
        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array('db' => 'nome_comune', 'dt' => 'comune'),
            array('db' => 'nome_territorio', 'dt' => 'nome_territorio'),
            array('db' => 'via', 'dt' => 'via'),
            array('db' => 'civico', 'dt' => 'civico'),
            array('db' => 'interno', 'dt' => 'interno'),
            array('db' => 'coordinata', 'dt' => 'coordinata'),
            array('db' => 'notes', 'dt' => 'note'),
            array('db' => 'id_nominativo', 'dt' => 'id_nominativo'),
            array('db' => 'data_aggiornamento', 'dt' => 'data_aggiornamento'),
            array('db' => 'status', 'dt' => 'status'),
        );

        $whereResult = "status <> 0";
        $whereResult2 = "";
        $res = DataTableJsServerSideProcessingManagerMySQL::complex($_POST, $sql_details, 'v_nominativi_territori_comuni', 'id_nominativo', $columns, $whereResult, $whereResult2);

        echo json_encode($res);
    }

    public function loadListaByTerritorio($par)
    {
        $id_territorio = $par['id_territorio'];
        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array('db' => 'nome_comune', 'dt' => 'comune'),
            array('db' => 'via', 'dt' => 'via'),
            array('db' => 'civico', 'dt' => 'civico'),
            array('db' => 'interno', 'dt' => 'interno'),
            array('db' => 'coordinata', 'dt' => 'coordinata'),
            array('db' => 'latitudine', 'dt' => 'latitudine'),
            array('db' => 'longitudine', 'dt' => 'longitudine'),
            array('db' => 'notes', 'dt' => 'note'),
            array('db' => 'id_nominativo', 'dt' => 'id_nominativo'),
            array('db' => 'status', 'dt' => 'status'),
            array('db' => 'nota_guest', 'dt' => 'nota_guest')
        );

        $whereResult = "id_territorio = $id_territorio";
        $whereResult2 = "";
        $res = DataTableJsServerSideProcessingManagerMySQL::complex($_POST, $sql_details, 'v_nominativi_territori_comuni', 'id_nominativo', $columns, $whereResult, $whereResult2);
        echo json_encode($res);
    }

    public function printListaNonSuonare(){
        include_once("ext_assets/fpdf181/fpdf.php");
        $lista_nominativi = nominativoDbManager::getNonSuonare();
        $struct = array();
        foreach($lista_nominativi as $one){
           if(!isset($struct[$one['nome_comune']])){
               $struct[$one['nome_comune']] = array();
               if(!isset($struct[$one['nome_comune']][$one['nome_territorio']])){
                   $struct[$one['nome_comune']][$one['nome_territorio']] = array();
               }
           }
            $struct[$one['nome_comune']][$one['nome_territorio']][] = $one;
        }


        //LARGHEZZA PAGINA LANDSCAPE : 296 (colonna 184)
        //ALTEZZA PAGINA LANDSCAPE : 205
        //COORDINATE SI CALCOLANO DALL'ANGOLO IN ALTO A SINISTRA

        $pdf = new \FPDF('L','mm','A4');
        $pdf->AddPage();
        $pdf->SetMargins(5, 5, 5);
        $pdf->SetAutoPageBreak(false, 0);

        $pdf->SetX(0);
        $pdf->SetY(0);


        $height_cell = 8;
        $x_colonna_2 = 148;
        $y_fine_pagina = 180;

        $y = $pdf->GetY();
        $n_pagina = 1;
        $col = 1;


        $nome_comune_temp = null;
        foreach($struct as $k_comune => $v_territorio){
            //echo $k_comune;
            if($nome_comune_temp != $k_comune){
                $nome_comune_temp == $k_comune;
                $pdf->AddPage();
                //CREO NUOVA PAGINA
                //INTESTAZIONE
                $pdf->SetFont('Arial','B',14);
                $pdf->SetY(5);
                $pdf->SetX(5);
                $pdf->Cell(60,8,"Settimo T.se Romena",0,0);
                $pdf->Cell(65,8,$k_comune,0,0,"R");
                $pdf->SetFont('Arial','',10);
                //FINE INTESTAZIONE
                $x_start_lista = 16;
                $pdf->SetY($x_start_lista);
            }

            foreach($struct[$k_comune] as $k_territorio => $v_nominativo){
               //RIGHE ELENCO
                $pdf->Cell(140,$height_cell,$k_comune.": ".$k_territorio,1,1);
                foreach($v_nominativo as $one){
                    $via = $one['via'];
                    $civico = $one['civico'];
                    $interno = $one['interno'];
                    $coordinata = $one['coordinata'];
                    $notes = $one['notes'];
                    if($one['status'] == 1){
                        $status = "Non suonare";
                    }elseif($one['status'] == 2){
                        $status = "Non romeno";
                    }

                    $y = $pdf->GetY();

                    $pdf->Cell(45,$height_cell,$via,1,0);
                    $pdf->Cell(10,$height_cell,$civico,1,0);
                    $pdf->Cell(10,$height_cell,$interno,1,0);
                    $pdf->Cell(10,$height_cell,$coordinata,1,0);
                    $pdf->Cell(10,$height_cell,$coordinata,1,0);
                    $pdf->Cell(25,$height_cell,$status,1,0);
                    $pdf->Cell(30,$height_cell,$notes,1,1);

                    if($y >= $y_fine_pagina && $col == 1){
                        $pdf->Cell(150,8,"pagina ".$n_pagina++,0,0,"C");

                        //INTESTAZIONE
                        $pdf->SetFont('Arial','B',14);
                        $pdf->SetY(5);
                        $pdf->SetX(5);
                        $pdf->Cell(60,8,"Settimo T.se Romena",0,0);
                        $pdf->Cell(75,8,$k_comune,0,0,"R");
                        $pdf->SetFont('Arial','',10);
                        //FINE INTESTAZIONE

                        $pdf->SetY($x_start_lista);
                        $pdf->SetX($x_colonna_2);

                        $col = 2;
                    }elseif($y >= $y_fine_pagina && $col == 2){
                        $pdf->SetX($x_colonna_2);
                        $pdf->Cell(150,8,"pagina ".$n_pagina++,0,0,"C");

                        $pdf->AddPage();

                        //INTESTAZIONE
                        $pdf->SetFont('Arial','B',14);
                        $pdf->SetY(5);
                        $pdf->SetX(5);
                        $pdf->Cell(60,8,"Settimo T.se Romena",0,0);
                        $pdf->Cell(75,8,$k_comune,0,0,"R");
                        $pdf->SetFont('Arial','',10);
                        //FINE INTESTAZIONE

                        $pdf->SetY($x_start_lista);
                        $col = 1;

                    }elseif($y < $y_fine_pagina && $col == 1){
                        $col = 1;
                    }elseif($y < $y_fine_pagina && $col == 2){
                        $pdf->SetX($x_colonna_2);
                        $col = 2;
                    }
                }
            }

        }
        //PIE COLONNA
        $pdf->Cell(150,8,"pagina ".$n_pagina++,0,0,"C");

        $pdf->Output();


        /*


        //INTESTAZIONE
        $pdf->SetFont('Arial','B',14);
        $nome_territorio = $struct['nome_territorio'];
        $pdf->SetY(5);
        $pdf->SetX(5);
        $pdf->Cell(60,8,"Settimo T.se Romena",0,0);
        $pdf->Cell(75,8,$nome_territorio,0,0,"R");
        $pdf->SetFont('Arial','',10);
        //FINE INTESTAZIONE

        $x_start_lista = 16;
        $pdf->SetY($x_start_lista);

        foreach ($nominativi as $one) {
            $via = $one['via'];
            $civico = $one['civico'];
            $interno = $one['interno'];
            $coordinata = $one['coordinata'];
            $notes = $one['notes'];
            $status = $one['status'];

            $y = $pdf->GetY();
            $pdf->Cell(45,$height_cell,$via,1,0);
            $pdf->Cell(15,$height_cell,$civico,1,0);
            $pdf->Cell(10,$height_cell,$interno,1,0);
            $pdf->Cell(10,$height_cell,$coordinata,1,0);
            $pdf->Cell(60,$height_cell,$notes,1,1);
            if($y >= $y_fine_pagina && $col == 1){
                $pdf->Cell(150,8,"pagina ".$n_pagina++,0,0,"C");

                //INTESTAZIONE
                $pdf->SetFont('Arial','B',14);
                $nome_territorio = $struct['nome_territorio'];
                $pdf->SetY(5);
                $pdf->SetX($x_colonna_2);
                $pdf->Cell(60,8,"Settimo T.se Romena",0,0);
                $pdf->Cell(75,8,$nome_territorio,0,0,"R");
                $pdf->SetFont('Arial','',10);
                //FINE INTESTAZIONE

                $pdf->SetY($x_start_lista);
                $pdf->SetX($x_colonna_2);

                $col = 2;
            }elseif($y >= $y_fine_pagina && $col == 2){
                $pdf->SetX($x_colonna_2);
                $pdf->Cell(150,8,"pagina ".$n_pagina++,0,0,"C");

                $pdf->AddPage();

                //INTESTAZIONE
                $pdf->SetFont('Arial','B',14);
                $nome_territorio = $struct['nome_territorio'];
                $pdf->SetY(5);
                $pdf->SetX(5);
                $pdf->Cell(60,8,"Settimo T.se Romena",0,0);
                $pdf->Cell(75,8,$nome_territorio,0,0,"R");
                $pdf->SetFont('Arial','',10);
                //FINE INTESTAZIONE

                $pdf->SetY($x_start_lista);
                $col = 1;

            }elseif($y < $y_fine_pagina && $col == 1){
                $col = 1;
            }elseif($y < $y_fine_pagina && $col == 2){
                $pdf->SetX($x_colonna_2);
                $col = 2;
            }
        }
        //PIE COLONNA
        $pdf->Cell(150,8,"pagina ".$n_pagina++,0,0,"C");




        $pdf->Output();
        */

    }

    public function loadListaByComune($par)
    {

        $id_comune = $par['id_comune'];
        $sql_details = driverDbHandler::$DB_INFO;
        $columns = array(
            array('db' => 'nome_territorio', 'dt' => 'territorio'),
            array('db' => 'via', 'dt' => 'via'),
            array('db' => 'civico', 'dt' => 'civico'),
            array('db' => 'interno', 'dt' => 'interno'),
            array('db' => 'coordinata', 'dt' => 'coordinata'),
            array('db' => 'latitudine', 'dt' => 'latitudine'),
            array('db' => 'longitudine', 'dt' => 'longitudine'),
            array('db' => 'notes', 'dt' => 'note'),
            array('db' => 'id_nominativo', 'dt' => 'id_nominativo'),
            array('db' => 'status', 'dt' => 'status'),
            array('db' => 'nota_guest', 'dt' => 'nota_guest')
        );

        $whereResult = "id_comune = $id_comune";
        $whereResult2 = "";
        $res = DataTableJsServerSideProcessingManagerMySQL::complex($_POST, $sql_details, 'v_nominativi_territori_comuni', 'id_nominativo', $columns, $whereResult, $whereResult2);
        echo json_encode($res);
    }

    public static function getNominativoById($par){
        $id_nominativo = $par['id_nominativo'];
        $nominativo = nominativoDbManager::getNominativoById($id_nominativo);
        echo json_encode($nominativo);
    }

    public static function cancellaNominativoById($par){
        $id_nominativo = $par['id_nominativo'];
        $esito = nominativoDbManager::cancellaNominativoById($id_nominativo);
        if($esito){
            $response = array("esito" => "OK", "msg" => "Cancellazione effettuta con successo");

        }else{
            $response = array("esito" => "KO", "msg" => "Cancellazione fallita");
        }
        echo json_encode($response);
    }

    public static function salvaNominativo(){
        $par_nominativo = array();
        $forza_status = null;
        if(isset($_POST['id']) && $_POST['id']!= ''){
            $par_nominativo['id'] = $_POST['id'];
        }
        if(isset($_POST['via']) && $_POST['via']!= ''){
            $par_nominativo['via'] = strtolower(trim(helpString::clearString($_POST['via'])));
        }
        if(isset($_POST['civico']) && $_POST['civico']!= ''){
            $par_nominativo['civico'] = trim($_POST['civico']);
        }
        if(isset($_POST['interno']) && $_POST['interno']!= ''){
            $par_nominativo['interno'] = trim(helpString::clearString($_POST['interno']));
        }
        if(isset($_POST['coordinata']) && $_POST['coordinata']!= ''){
            $par_nominativo['coordinata'] = strtoupper(trim(helpString::clearString($_POST['coordinata'])));
        }
        if(isset($_POST['note']) && $_POST['note']!= ''){
            $par_nominativo['note'] = strtolower(trim(helpString::clearString($_POST['note'])));
            if(strpos($par_nominativo['note'],'non romeno') !== false){
                $forza_status = 1;
                $par_nominativo['note'] = trim(str_replace('non romeno','',$par_nominativo['note']));
            }elseif(strpos($par_nominativo['note'],'non visitare') !== false){
                $forza_status = 2;
                $par_nominativo['note'] = trim(str_replace('non visitare','',$par_nominativo['note']));
            }
        }
        if(isset($_POST['status']) && $_POST['status']!= ''){
            if($forza_status){
                $par_nominativo['status'] = $forza_status;
            }else{
                $par_nominativo['status'] = $_POST['status'];
            }
        }
        if(isset($_POST['id_comune']) && $_POST['id_comune']!= ''){
            $par_nominativo['id_comune'] = $_POST['id_comune'];
        }
        if(isset($_POST['id_territorio']) && $_POST['id_territorio']!= ''){
            $par_nominativo['id_territorio'] = $_POST['id_territorio'];
        }
        if(isset($_POST['nome_comune']) && $_POST['nome_comune']!= ''){
            $par_nominativo['nome_comune'] = $_POST['nome_comune'];
        }else{
            $comune = comuneDbManager::getComuneById($par_nominativo['id_comune']);
            $par_nominativo['nome_comune'] = $comune['nome_comune'];
        }

        $address = urlencode($par_nominativo['nome_comune'].", ".$par_nominativo['via']." ".$par_nominativo['civico']);

        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?key=AIzaSyCWfN-lcWrUGrQNhjMGOAGDeAMsekB3aP0&address='.$address.'&sensor=false');
        $output= json_decode($geocode);

        if($output->status == 'OK'){
            //ANALISI DATI TROVATI IN GOOGLE
            //$via_by_google = $output->results[0]->address_components[1]->long_name;
            //if($via_by_google != $par_nominativo['nome_comune']){
            //    $par_nominativo['via'] = $via_by_google;
                $par_nominativo['latitudine'] = $output->results[0]->geometry->location->lat;
                $par_nominativo['longitudine'] = $output->results[0]->geometry->location->lng;
            //}
        }else{
            $par_nominativo['latitudine'] = 0;
            $par_nominativo['longitudine'] = 0;
        }

        if(nominativoDbManager::salvaNominativo($par_nominativo)){
            $resp = array("esito" => "OK", "msg" => "Operazione effettuata");
        }else{
            $resp = array("esito" => "KO", "msg" => "Operazione fallita");
        }
        echo json_encode($resp);

    }

    public static function getNominativiByTerritorio($id_territorio = null){
        if($id_territorio != null){
            $array_nominativi = nominativoDbManager::getNominativiByTerritorio($id_territorio);
        }else{
            $array_nominativi = nominativoDbManager::getAllNominativi();
        }
        return $array_nominativi;
    }

    public static function getNominativiByComune($id_comune = null){

        if($id_comune != null){
            $array_nominativi = nominativoDbManager::getNominativiByComune($id_comune);
        }else{
            $array_nominativi = nominativoDbManager::getAllNominativi();
        }
        //ASSEGNO UN ID SEGNAPOSTO PROGRESSIVO A SECONDA DELL ID TERRITORIO
        $id_segnaposto = 0;
        $array_id_segnaposto = array();
        $via_temp = "via_temp";
        $civico_temp = "civico_temp";
        $qta = 1;
        $array_unset = array();
        foreach($array_nominativi AS $k => $v){
            $id_terr =  $v['id_territorio'];
            $nome_territorio =  $v['nome_territorio'];
            $via =  trim($v['via']);
            $civico =  trim($v['civico']);
            //GESTIONE ID SEGNAPOSTO
            if(!array_key_exists($id_terr,$array_id_segnaposto)){
                $array_nominativi[$k]['segnaposto'] = $id_segnaposto;
                $array_id_segnaposto[$id_terr]=$id_segnaposto;
                $array_territori[$id_terr] = array("id_terr" => $id_terr, "nome" => $nome_territorio,"segnaposto" => $id_segnaposto);
                $id_segnaposto++;
            }else{
                $array_nominativi[$k]['segnaposto'] = $array_id_segnaposto[$id_terr];
            }

            //GESTIONE QTA NOMINATIVI PER INDIRIZZO
            if($civico != $civico_temp || $via != $via_temp){
                $qta = 1;
                $civico_temp = $civico;
                $via_temp = $via;
            }elseif($civico == $civico_temp && $via == $via_temp){
                $array_unset[]= $k-1;
                $qta++;
            }
            $array_nominativi[$k]['qta'] = $qta;
        }

        arsort($array_unset);
        foreach($array_unset as $one){
            unset($array_nominativi[$one]);
        }

        $response = array("array_nominativi" => $array_nominativi,"array_territori" => $array_territori);
        return $response;
    }

    public function salvaNotaNominativo(){
        $id_nominativo = trim($_POST['id_nominativo']);
        $new_nota = trim($_POST['new_nota']);
        $id_guest = driverUacController::getUacSession()->id_utente;
        $result_insert = nominativoDbManager::saveGuestNotaNominativo($id_guest, $id_nominativo, $new_nota);
        return $result_insert;
    }

    public function updateTerritorio(){
        $id_nominativo = trim($_POST["id_nominativo"]);
        $id_territorio = trim($_POST["id_territorio"]);
        //RECUPERO ID NOMINATIVI CON UGUALE COMUNE, VIA, CIVICO
        $info_nominativo = nominativoDbManager::getNominativoById($id_nominativo);
        $nominativi_stesso_indirizzo = nominativoDbManager::getNominativiByIndirizzo($info_nominativo['id_comune'],$info_nominativo['via'],$info_nominativo['civico']);
        foreach($nominativi_stesso_indirizzo as $one){
            $result = nominativoDbManager::updateTerritorio($one['id_nominativo'],$id_territorio);
        }
        if($result){
            $response = array("esito" => "OK");
        }else{
            $response = array("esito" => "OK");
        }
        echo json_encode($response);
    }

}