<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 23/01/2017
 * Time: 10:33
 */
namespace nominativo;
use twig\driverTwigContext;
use twig\driverTwigView;


class nominativoView
{
    public function showListNonSuonare()
    {
        $context = new driverTwigContext();
        driverTwigView::show('nominativi/lista_non_suonare.twig',$context);
    }

}