<?php
/**
 * Created by PhpStorm.
 * User: Utente
 * Date: 14/02/2018
 * Time: 09:35
 */

namespace script;


use nominativo\nominativoController;
use nominativo\nominativoDbManager;
use utente\utenteDbManager;

class scriptController
{
    //cicla tutti i nominativi senza coordinate gps e inserisce coordinate e strada corretta
    public function updateCooGps(){
        $key_google = "AIzaSyCWfN-lcWrUGrQNhjMGOAGDeAMsekB3aP0";
        //$key_google = "AIzaSyCWfN-lcWrUGrQNhjMGOAGDeAMsekB3aP0";
        //$key_google = "AIzaSyA7k3-AgrjZ4cX9EWxLQw40C6-aFHMuX9s";
        //$key_google = "AIzaSyAcuorc7JRb4KN1XTH_oEzwktDIKsdJ-Ls";



        $nominativi = nominativoDbManager::getAllNominativi();
        foreach($nominativi as $one){

            $id_nominativo = $one['id_nominativo'];
            $via = trim($one['via']);
            $civico = $one['civico'];
            $latitudine = $one['latitudine'];
            $longitudine = $one['longitudine'];
            $nome_comune = $one['nome_comune'];

            $address = urlencode("$nome_comune, $via $civico");

            echo "===>>>  Indirizzo: $nome_comune $via, $civico <br>";
            if($latitudine == 0){
                $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?key='.$key_google.'&address='.$address.'&sensor=false');
                $output= json_decode($geocode);

                if(count($output->results) > 0){
                    $via = $output->results[0]->address_components[1]->long_name;
                    echo ".....id: $id_nominativo   indirizzo grezzo $nome_comune, $via $civico via corretta: $via";
                    $latitudine = $output->results[0]->geometry->location->lat;
                    $longitudine = $output->results[0]->geometry->location->lng;

                    echo "lat: $latitudine,  lng: $longitudine <br>";
                    nominativoDbManager::updateCooGps($id_nominativo, $latitudine, $longitudine, $via);
                }else{
                    pre($output);
                    echo ".........non trovato id: $id_nominativo   indirizzo grezzo  $nome_comune, $via $civico<br>";
                }
            }
        }
    }

    //CREA USER E PASSWORD PER TUTTI GLI UTENTI
    public function setPasswordUtente(){
        $all_utenti = utenteDbManager::getAllUtenti();
        foreach($all_utenti as $k => $v){
            //SE IL NOME E PIU LUNGO DI 2 CARATTERI
            $id_utente = $all_utenti[$k]['id_utente'];
            $nome = $all_utenti[$k]['nome_utente'];
            $cognome = $all_utenti[$k]['cognome_utente'];
            if(strlen($nome) > 2){
                $attivo = 1;
                echo '<br>===>';
                $cut_nome = substr($nome, 0, 3);
                $cut_cognome = substr($cognome, 0, 3);
                //CREO USER PRIMI 3 CARATTERI DI NOME + PRIMI 3 DI CARATTERI DI COGNOME
                $user_utente = $cut_nome.$cut_cognome;
                //CREO PASSWORD PRIMI 3 CARATTERI DI COGNOME + PRIMI 3 DI CARATTERI DI NOME
                $psw_utente_chiaro = $cut_cognome.$cut_nome;
                $psw_utente = md5($psw_utente_chiaro);
                echo "creo credenziali utenti  $user_utente $psw_utente_chiaro $psw_utente  <br>";
                utenteDbManager::updateCredenzialiUtente($id_utente, $user_utente, $psw_utente, $psw_utente_chiaro,$attivo);
            }else{
                echo 'disabilito utente<br>';
                utenteDbManager::updateCredenzialiUtente($id_utente, $id_utente, 0, 0,0);
            }
        }
    }


}

