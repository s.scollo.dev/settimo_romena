<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 14/12/2016
 * Time: 18:21
 */
/*
 * ELENCO VERSIONI
 *
 * ??/12/2016    1.1     prima correzione bug storico oratori
 * 21/12/2016    1.2     correzione definitiva(?) storico oratori e versione su login
 * 22/12/2016    1.2.1   correzione storio punti su lettura biblica
 * 18/01/2017    1.3     correzione bug data settimana, aggiunta storico scuola, aggiunta note e lista note
 * 22/01/2017     1.3.1   gestione saggia di punti scuola
 * 16/02/2017     1.3.2   corretto script92 con path dinamico intelligente,  inserita data settimanale (dal-al)
 * 16/02/2017     1.3.3   inserito storico in form proclamatore
 * 17/12/2017     1.3.4   aggiornamento temporaneo (assegnazioni pdf)per 2018
 * 29/12/2017     1.3.5   modifica stampa programmi e assegnazioni parti 2018
 * 21/01/2019	  1.3.6   eliminate stampe di assegnazioni vuote, eliminato inserimento del punto in adunanza
 */

$_SESSION['version'] = "1.3.6";


// TODO
//debug per la lettura della scuola non va aggiornamento punti , visualizzare caricamento punti
//debug dopo cancellazione adunanza rimando a url 404
//elenco discorsi per proclamatore
