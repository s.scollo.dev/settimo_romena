<?php
session_start();
include_once("core/mainApp.php");
include_once("version.php");

if(empty($_GET['page']) || $_GET['page'] == ''){
    $page = "home";
}
else{
    $page = $_GET['page'];
}

mainApp::mainAppController($page);