<?php if(!class_exists('raintpl')){exit;}?>    <div class="row">
       <div class="col-lg-6 title-container">
            <h1 class="page-header">Elenco adunanze create</h1>
        </div>
        <div class="col-lg-6 btn-container" >
            <h1 class="page-header">
                <?php if( $admin_mode ){ ?>

                <a href="/life/form-adunanza"><button class="btn btn-primary" class="modify">Nuova adunanza</button></a>
                <?php } ?>

            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">

                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-adunanza">
                            <thead>
                                <tr>
                                    <th>DATA</th>
                                    <th>SETTIMANA</th>
                                    <th width="50%">NOTE</th>
                                    <th>AZIONI</th>
                                </tr>
                            </thead>
                            <tbody>
                           <?php $counter1=-1; if( isset($adunanza_list) && is_array($adunanza_list) && sizeof($adunanza_list) ) foreach( $adunanza_list as $key1 => $value1 ){ $counter1++; ?>

                            <tr class="odd gradeX">
                                <td class=""><?php echo $value1["adunanza_data"];?></td>
                                <td class=""><?php echo $value1["adunanza_settimana"];?></td>
                                <td class=""><?php if( $value1["adunanza_evento"] ){ ?><?php echo $value1["adunanza_evento"];?><?php } ?></td>

                                <?php if( $admin_mode ){ ?>

                                <td class="">
                                    <a href="/life/modifica-adunanza_<?php echo $value1["adunanza_id"];?>" class="btn btn-primary" class="modify">Modifica</a>
                                    <button class="btn btn-primary" class="delete" onclick="return deleteAdunanza(<?php echo $value1["adunanza_id"];?>)">Cancella</button>
                                </td>
                                <?php }else{ ?>

                                <td class="">
                                    <a href="/life/modifica-adunanza_<?php echo $value1["adunanza_id"];?>" class="btn btn-primary" class="modify">Dettagli</a>
                                </td>
                                <?php } ?>

                            </tr>
                            <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php if( $admin_mode ){ ?>

    <div class="modal fade" id="modal_confirm_delete" role="dialog">
        <div class="modal-dialog">
            <form name="form_modal" id="form_modal" method="get" action="">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p id="confirm_msg"><?php echo $modify_msg;?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Annulla</button>
                        <a href="/<?php echo $cancella_attivita;?>" class="btn btn-primary" id="confirm_delete_act" >Conferma cancellazione</a>
                    </div>
                </div>
            </form>
        </div>
    </div> <!-- /.Modal -->
    <?php } ?>


