<?php if(!class_exists('raintpl')){exit;}?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo $action;?> proclamatore</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form>
                            <input id="insert_proclamatore" value="<?php echo $url_insert_proclamatore;?>" type="hidden">
                            <input id="verify_proclamatore" value="<?php echo $url_verify_proclamatore;?>" type="hidden">
                        </form>
                        <p>TODO : verifica proclamatore doppio</p>
                        <p>TODO : caratteri uppercase in inserimento e update</p>
                        <form id="form_proclamatore" method="post" action="<?php echo $url_insert_proclamatore;?>">
                            <?php if( $update_mode ){ ?>

                            <div class="form-group">
                                <label>ID</label>
                                <input id="proclamatore_id" name="proclamatore_id" class="form-control" value="<?php echo $proclamatore_id;?>" readonly>
                            </div>
                            <?php } ?>

                            <div class="form-group">
                                <label>NOME</label>
                                <input id="proclamatore_nome" name="proclamatore_nome" class="form-control" value="<?php echo $proclamatore_nome;?>">
                            </div>
                            <div class="form-group">
                                <label>COGNOME</label>
                                <input id="proclamatore_cognome" name="proclamatore_cognome" class="form-control" value="<?php echo $proclamatore_cognome;?>">
                            </div>

                            <div class="form-group">
                                <label>TELEFONO </label>
                                <input id="proclamatore_telefono" name="proclamatore_telefono" class="form-control" value="<?php echo $proclamatore_telefono;?>">
                            </div>
                            <div class="form-group">
                                <label>EMAIL </label>
                                <input id="proclamatore_mail" name="proclamatore_mail" class="form-control" value="<?php echo $proclamatore_mail;?>">
                            </div>

                            <hr>
                            <div class="form-group col-md-6" >
                                <label>Ruoli</label>
                                <?php $counter1=-1; if( isset($ruoli_list) && is_array($ruoli_list) && sizeof($ruoli_list) ) foreach( $ruoli_list as $key1 => $value1 ){ $counter1++; ?>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="ruoli[]" value="<?php echo $value1["ruolo_id"];?>"  <?php echo $value1["checked"];?>><?php echo $value1["ruolo_desc"];?>

                                    </label>
                                </div>
                                <?php } ?>

                                <br>
                                <?php if( $admin_mode ){ ?>

                                <button type="submit" class="btn btn-primary">Salva</button>
                                <?php } ?>

                            </div>
                            <div class="form-group col-md-6" >
                                <label>Storico</label>
                                <?php $counter1=-1; if( isset($storia_proc) && is_array($storia_proc) && sizeof($storia_proc) ) foreach( $storia_proc as $key1 => $value1 ){ $counter1++; ?>

                                <div class="checkbox">
                                   <ul>
                                       <il><p><?php echo $value1["data"];?> : <?php echo $value1["ruolo"];?>  <?php echo $value1["punto"];?>  <?php echo $value1["superato"];?></p></il>
                                   </ul>

                                </div>
                                <?php } ?>

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if( $admin_mode ){ ?>

<div class="modal fade" id="modal_dialog_proclamatore" role="dialog">
    <div class="modal-dialog">
        <form name="form_modal" id="form_modal" method="get" action="">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p class="dialog_msg"><?php echo $modify_msg;?></p>
                </div>
                <div class="modal-footer">
                    <a href="/life/<?php echo $url_list_proclamatore;?>" class="btn btn-default">Continua</a>
                </div>
            </div>
        </form>
    </div>
</div>
<?php } ?>



