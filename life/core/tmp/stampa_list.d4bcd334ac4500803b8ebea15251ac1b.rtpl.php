<?php if(!class_exists('raintpl')){exit;}?><form name="form_stampa" id="form_stampa" method="post" action="avvia-stampa">
    <div class="row">
       <div class="col-lg-12 title-container">
            <h1 class="page-header">Scegli quali adunanze stampare e l'azione   <img id="loading" class="hidden" src="template/stampa/img/loading.gif"></h1>
        </div>
        <div class="col-lg-12 btn-container" >
            <input type="submit" id="start_operazioni" class="azione hidden btn btn-primary"  value="Stampa assegnazioni" azione="assegnazioni">
            <input type="submit" id="start_operazioni" class="azione hidden btn btn-primary"  value="Stampa programma tabella" azione="tabella">
            <input type="submit" id="start_operazioni" class="azione hidden btn btn-primary"  value="Stampa programma presidente" azione="presidente">
            <!--<input type="submit" id="start_operazioni" class="azione hidden btn btn-primary"  value="Invia programma" azione="mail">-->
            <input type="hidden" id="azione" name="azione">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">

                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-adunanza">
                            <thead>
                                <tr>
                                    <th>Data</th>
                                    <th width="190px">AZIONI</th>
                                </tr>
                            </thead>
                            <tbody>

                               <?php $counter1=-1; if( isset($adunanza_list) && is_array($adunanza_list) && sizeof($adunanza_list) ) foreach( $adunanza_list as $key1 => $value1 ){ $counter1++; ?>

                                <tr class="odd gradeX">
                                    <td class=""><?php echo $value1["adunanza_data"];?></td>
                                    <td class="">
                                        <input class="check_stampa" type="checkbox" name="selezione[]" value="<?php echo $value1["adunanza_id"];?>">
                                    </td>
                                </tr>
                                <?php } ?>


                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</form>
   <!-- <div class="modal fade" id="modal_stampa" role="dialog">
        <div class="modal-dialog">


                <div class="modal-content">
                    <div class="modal-body">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_operazione" name="stampa_programma_pub" value="1">
                                Stampa programma pubblico
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_operazione" name="stampa_programma_pres" value="1">
                                Stampa programma presidenti
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_operazione" name="invia_programma" value="1">
                                Invia programma
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="check_operazione" name="stampa_assegnazioni" value="1">
                                Stampa assegnazioni
                            </label>
                        </div>
                        <input type="hidden" name="da_stampare" id="da_stampare" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Annulla</button>
                        <input type="submit" id="start_operazioni" class="hidden btn btn-primary"  value="Via!!!">
                    </div>
                </div>
            </form>
        </div>
    </div>--> <!-- /.Modal -->

