<?php if(!class_exists('raintpl')){exit;}?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Modifica adunanza <a href="http://wol.jw.org/ro/wol/dt/r34/lp-m/<?php echo $data_link_wol;?>" id="open_wol" class="btn btn-primary" target="_blank">WOL</a></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div id="form_content" class="row">
                    <form id="form_adunanza_content" method="post" action="life/salva-adunanza">

                        <input id="adunanza_id" name="adunanza_id" type="hidden" value="<?php echo $adunanza_id;?>" >
                        <input id="adunanza_vis" name="adunanza_vis" type="hidden" value="<?php echo $adunanza_id;?>" >
                        <input id="adunanza_cong" name="adunanza_cong" type="hidden" value="<?php echo $adunanza_id;?>" >

                        <div class="form-group col-md-2">
                            <label>Data</label>
                            <input id="data" name="data" class="form-control" readonly value="<?php echo $data;?>">
                        </div>
                        <div class="form-group col-md-10" style="margin-top:30px">
                            <div class="checkbox-inline">
                                <input type="checkbox" disabled id="ad_visita" name="ad_visita" <?php if( $visita==1 ){ ?>checked<?php } ?> >
                                <label>Visita del sorvegliante</label>
                            </div>
                            <div class="checkbox-inline">
                                <input type="checkbox" disabled <?php if( $congresso=='cong_circ' ){ ?>checked<?php } ?> value="cong_circ">
                                <label>Assemblea di circoscrizione</label>
                            </div>
                            <div class="checkbox-inline">
                                <input type="checkbox" disabled <?php if( $congresso=='cong_reg' ){ ?>checked<?php } ?>  value="cong_reg">
                                <label>Assemblea regionale</label>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Settimana : <?php echo $settimana;?></label>
                        </div>
                        <div class="col-md-12"><hr></div>
                        <input type="hidden" id="visita" name="visita"  value="<?php echo $visita;?>" >
                        <?php if( $congresso ){ ?>

                        <input type="hidden" value="<?php echo $congresso;?>" class="form-control" id="ad_cong" name="ad_cong" >
                        <?php }else{ ?>

                        <div class="form-group col-md-6">
                            <label>Lettura settimanale</label>
                            <input id="lett_sett" name="lett_sett" class="form-control" value="<?php echo $lettura;?>" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Cantico iniziale</label>
                            <input id="cant_1" name="cant_1" class="form-control" value="<?php echo $cantico_1;?>" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Presidente</label>
                            <select id="pres" name="pres" class="form-control " >
                                <?php $counter1=-1; if( isset($presidente_nome) && is_array($presidente_nome) && sizeof($presidente_nome) ) foreach( $presidente_nome as $key1 => $value1 ){ $counter1++; ?>

                                <option value="<?php echo $value1["proclamatore_id"];?>" <?php if( $value1["selected"] ){ ?>selected="selected"<?php } ?>><?php if( $value1["proclamatore_id"]!=51 ){ ?><?php echo $value1["proclamatore_nome"];?> <?php echo $value1["proclamatore_cognome"];?> : <?php echo $value1["adunanza_data"];?><?php } ?></option>
                                <?php } ?>

                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Preghiera iniziale</label>
                            <select id="preg_1" name="preg_1" class="form-control " >
                                <?php $counter1=-1; if( isset($preghiera_1_nome) && is_array($preghiera_1_nome) && sizeof($preghiera_1_nome) ) foreach( $preghiera_1_nome as $key1 => $value1 ){ $counter1++; ?>

                                <option value="<?php echo $value1["proclamatore_id"];?>" <?php if( $value1["selected"] ){ ?>selected=selected"<?php } ?>><?php if( $value1["proclamatore_id"]!=51 ){ ?><?php echo $value1["proclamatore_nome"];?> <?php echo $value1["proclamatore_cognome"];?> : <?php echo $value1["adunanza_data"];?><?php } ?></option>
                                <?php } ?>

                            </select>
                        </div>

                        <div class="col-lg-12">

                            <div class="panel panel-blue">
                                <div class="panel-heading">
                                    COMORI DIN CUV&Acirc;NTUL LUI DUMNEZEU
                                </div>

                                <div class="panel-body">
                                    <?php $counter1=-1; if( isset($page_sec_A) && is_array($page_sec_A) && sizeof($page_sec_A) ) foreach( $page_sec_A as $key1 => $value1 ){ $counter1++; ?>

                                    <div class="row">
                                        <div class="form-group col-md-9">
                                            <label>Tema</label>
                                            <input id="<?php echo $value1["id_tema"];?>" name="<?php echo $value1["id_tema"];?>" class="form-control" value="<?php echo $value1["tema"];?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Tempo</label>
                                            <input id="<?php echo $value1["id_tempo"];?>" name="<?php echo $value1["id_tempo"];?>" class="form-control" value="<?php echo $value1["tempo"];?>">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input id="<?php echo $value1["id_type_ruolo_1"];?>" name="<?php echo $value1["id_type_ruolo_1"];?>" type="hidden" value="<?php echo $value1["type_ruolo_1"];?>" >
                                            <label><?php echo $value1["label_ruolo_1"];?></label>
                                            <select id="<?php echo $value1["id_ruolo_1"];?>" name="<?php echo $value1["id_ruolo_1"];?>" class="form-control select_scuola" >
                                                <?php $counter2=-1; if( isset($value1["ruolo_1_nome"]) && is_array($value1["ruolo_1_nome"]) && sizeof($value1["ruolo_1_nome"]) ) foreach( $value1["ruolo_1_nome"] as $key2 => $value2 ){ $counter2++; ?>

                                                <option value="<?php echo $value2["proclamatore_id"];?>" <?php if( $value2["selected"] ){ ?>selected=selected"<?php } ?>><?php if( $value2["proclamatore_id"]!=51 ){ ?><?php echo $value2["proclamatore_nome"];?> <?php echo $value2["proclamatore_cognome"];?> : <?php echo $value2["adunanza_data"];?> - <?php echo $value2["ruolo"];?><?php } ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>									
										<!--
                                        <?php if( $value1["punto"] ){ ?>

										
                                            <div class="form-group col-md-1">
                                                <label>Punto</label>
                                                <select id="<?php echo $value1["id_punto"];?>" name="<?php echo $value1["id_punto"];?>" class="form-control select_scuola" style="width:70px">
                                                <?php $counter2=-1; if( isset($value1["punto"]) && is_array($value1["punto"]) && sizeof($value1["punto"]) ) foreach( $value1["punto"] as $key2 => $value2 ){ $counter2++; ?>

                                                    <option  value="<?php echo $value2["punto_id"];?>" <?php if( $value2["selected"] ){ ?>selected=selected"<?php } ?>><?php echo $value2["punto_id"];?>____<?php echo $value2["punto_desc"];?></option>
                                                <?php } ?>

                                                </select>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <label>Superato</label>
                                                <input type="checkbox" style="margin-left:30px" <?php if( $value1["punto_ok"]==1 ){ ?>checked<?php } ?> id="<?php echo $value1["id_punto_ok"];?>" name="<?php echo $value1["id_punto_ok"];?>">
                                            </div>
                                            <div class="form-group col-md-1">
                                                <label>Nota</label>
                                                <input type="button"  id="<?php echo $value1["nota"];?>" name="<?php echo $value1["nota"];?>" id_proc=""  id_select="<?php echo $value1["id_ruolo_1"];?>" value="Inserisci" class="nota">
                                            </div>
                                            <div class="form-group col-md-1">
                                                <label>Lista</label>
                                                <select id="<?php echo $value1["id_lista_nota"];?>" name="<?php echo $value1["id_lista_nota"];?>" class="form-control" style="width:30px">
                                                </select>
                                            </div>
                                        <?php } ?>

										-->
                                    </div>
                                    <hr>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    SĂ FIM MAI EFICIENȚI ÎN PREDICARE
                                </div>
                                <div class="panel-body">
                                    <?php $counter1=-1; if( isset($page_sec_B) && is_array($page_sec_B) && sizeof($page_sec_B) ) foreach( $page_sec_B as $key1 => $value1 ){ $counter1++; ?>

                                    <div class="row">
                                        <div class="form-group col-md-9">
                                            <label>Tema</label>
                                            <input id="<?php echo $value1["id_tema"];?>" name="<?php echo $value1["id_tema"];?>" class="form-control" value="<?php echo $value1["tema"];?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Tempo</label>
                                            <input id="<?php echo $value1["id_tempo"];?>" name="<?php echo $value1["id_tempo"];?>" class="form-control" value="<?php echo $value1["tempo"];?>">
                                        </div>
                                        <div class="form-group col-md-3" >
                                            <input id="<?php echo $value1["id_type_ruolo_1"];?>" name="<?php echo $value1["id_type_ruolo_1"];?>" type="hidden" value="<?php echo $value1["type_ruolo_1"];?>" >
                                            <label><?php echo $value1["label_ruolo_1"];?></label>
                                            <select id="<?php echo $value1["id_ruolo_1"];?>" name="<?php echo $value1["id_ruolo_1"];?>" class="form-control select_scuola" >
                                                <?php $counter2=-1; if( isset($value1["ruolo_1_nome"]) && is_array($value1["ruolo_1_nome"]) && sizeof($value1["ruolo_1_nome"]) ) foreach( $value1["ruolo_1_nome"] as $key2 => $value2 ){ $counter2++; ?>

                                                <option value="<?php echo $value2["proclamatore_id"];?>" <?php if( $value2["selected"] ){ ?>selected=selected"<?php } ?>><?php if( $value2["proclamatore_id"]!=51 ){ ?><?php echo $value2["proclamatore_nome"];?> <?php echo $value2["proclamatore_cognome"];?> : <?php echo $value2["adunanza_data"];?> - <?php echo $value2["ruolo"];?><?php } ?> </option>
                                                <?php } ?>

                                            </select>
                                        </div>
										<div class="form-group col-md-1">
                                            <label>Storia</label>
                                            <select id="<?php echo $value1["id_storia"];?>" name="<?php echo $value1["id_storia"];?>" class="form-control" style="width:30px">
                                            </select>
                                        </div>
										<!--
                                        <?php if( $value1["punto"] ){ ?>	
                                        <div class="form-group col-md-1" >
                                            <label>Punto</label>
                                            <select id="<?php echo $value1["id_punto"];?>" name="<?php echo $value1["id_punto"];?>" class="form-control" style="padding: 3px">
                                                <?php $counter2=-1; if( isset($value1["punto"]) && is_array($value1["punto"]) && sizeof($value1["punto"]) ) foreach( $value1["punto"] as $key2 => $value2 ){ $counter2++; ?>

                                                <option  value="<?php echo $value2["punto_id"];?>" <?php if( $value2["selected"] ){ ?>selected=selected"<?php } ?> ><?php echo $value2["punto_id"];?>_____<?php echo $value2["punto_desc"];?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label>Superato</label>
                                            <input type="checkbox" style="margin-left:30px" <?php if( $value1["punto_ok"]==1 ){ ?>checked<?php } ?> id="<?php echo $value1["id_punto_ok"];?>" name="<?php echo $value1["id_punto_ok"];?>">
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label>Nota</label>
                                            <input type="button"  id="<?php echo $value1["nota"];?>" name="<?php echo $value1["nota"];?>" id_proc=""  id_select="<?php echo $value1["id_ruolo_1"];?>" value="Inserisci" class="nota">
                                        </div>
                                        <div class="form-group col-md-1">
                                            <label>Lista</label>
                                            <select id="<?php echo $value1["id_lista_nota"];?>" name="<?php echo $value1["id_lista_nota"];?>" class="form-control" style="width:30px">
                                            </select>
                                        </div>
                                        <?php } ?>

										-->
										
                                        <?php if( $value1["label_ruolo_2"] ){ ?>

                                        <div class="form-group col-md-4">
                                            <input id="<?php echo $value1["id_type_ruolo_2"];?>" name="<?php echo $value1["id_type_ruolo_2"];?>" type="hidden" value="<?php echo $value1["type_ruolo_2"];?>" >
                                            <label><?php echo $value1["label_ruolo_2"];?></label>
                                            <select id="<?php echo $value1["id_ruolo_2"];?>" name="<?php echo $value1["id_ruolo_2"];?>" class="form-control " >
                                                <?php $counter2=-1; if( isset($value1["ruolo_2_nome"]) && is_array($value1["ruolo_2_nome"]) && sizeof($value1["ruolo_2_nome"]) ) foreach( $value1["ruolo_2_nome"] as $key2 => $value2 ){ $counter2++; ?>

                                                <option value="<?php echo $value2["proclamatore_id"];?>" <?php if( $value2["selected"] ){ ?>selected=selected"<?php } ?>><?php if( $value2["proclamatore_id"]!=51 ){ ?><?php echo $value2["proclamatore_nome"];?> <?php echo $value2["proclamatore_cognome"];?> : <?php echo $value2["adunanza_data"];?> - <?php echo $value2["ruolo"];?><?php } ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <?php } ?>

                                    </div>
                                    <hr>
                                    <?php } ?>

                                </div>

                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    VIAȚA DE CREȘTIN
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Cantico</label>
                                            <input id="cant_2" name="cant_2" class="form-control" value="<?php echo $cantico_2;?>" >
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">

                                    </div>
                                    <?php $counter1=-1; if( isset($page_sec_C) && is_array($page_sec_C) && sizeof($page_sec_C) ) foreach( $page_sec_C as $key1 => $value1 ){ $counter1++; ?>

                                    <div class="row">
                                        <div class="form-group col-md-9">
                                            <label>Tema</label>
                                            <input id="<?php echo $value1["id_tema"];?>" name="<?php echo $value1["id_tema"];?>" class="form-control" value="<?php echo $value1["tema"];?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Tempo</label>
                                            <input id="<?php echo $value1["id_tempo"];?>" name="<?php echo $value1["id_tempo"];?>" class="form-control" value="<?php echo $value1["tempo"];?>">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <input id="<?php echo $value1["id_type_ruolo_1"];?>" name="<?php echo $value1["id_type_ruolo_1"];?>" type="hidden" value="<?php echo $value1["type_ruolo_1"];?>" >
                                            <label><?php echo $value1["label_ruolo_1"];?></label>
                                            <select id="<?php echo $value1["id_ruolo_1"];?>" name="<?php echo $value1["id_ruolo_1"];?>" class="form-control " >
                                                <?php $counter2=-1; if( isset($value1["ruolo_1_nome"]) && is_array($value1["ruolo_1_nome"]) && sizeof($value1["ruolo_1_nome"]) ) foreach( $value1["ruolo_1_nome"] as $key2 => $value2 ){ $counter2++; ?>

                                                <option value="<?php echo $value2["proclamatore_id"];?>" <?php if( $value2["selected"] ){ ?>selected=selected"<?php } ?>><?php if( $value2["proclamatore_id"]!=51 ){ ?><?php echo $value2["proclamatore_nome"];?> <?php echo $value2["proclamatore_cognome"];?> : <?php echo $value2["adunanza_data"];?> - <?php echo $value2["ruolo"];?><?php } ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <?php if( $value1["label_ruolo_2"] ){ ?>

                                        <div class="form-group col-md-5">
                                            <input id="<?php echo $value1["id_type_ruolo_2"];?>" name="<?php echo $value1["id_type_ruolo_2"];?>" type="hidden" value="<?php echo $value1["type_ruolo_2"];?>" >
                                            <label><?php echo $value1["label_ruolo_2"];?></label>
                                            <select id="<?php echo $value1["id_ruolo_2"];?>" name="<?php echo $value1["id_ruolo_2"];?>" class="form-control " >
                                                <?php $counter2=-1; if( isset($value1["ruolo_2_nome"]) && is_array($value1["ruolo_2_nome"]) && sizeof($value1["ruolo_2_nome"]) ) foreach( $value1["ruolo_2_nome"] as $key2 => $value2 ){ $counter2++; ?>

                                                <option value="<?php echo $value2["proclamatore_id"];?>" <?php if( $value2["selected"] ){ ?>selected=selected"<?php } ?>><?php if( $value2["proclamatore_id"]!=51 ){ ?><?php echo $value2["proclamatore_nome"];?> <?php echo $value2["proclamatore_cognome"];?> : <?php echo $value2["adunanza_data"];?> - <?php echo $value2["ruolo"];?><?php } ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <?php } ?>


                                    </div>
                                    <hr>
                                    <?php } ?>

                                </div>

                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Cantico finale</label>
                            <input id="cant_3" name="cant_3" class="form-control" value="<?php echo $cantico_3;?>" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Preghiera finale</label>
                            <select id="preg_2" name="preg_2" class="form-control " >
                                <?php $counter1=-1; if( isset($preghiera_2_nome) && is_array($preghiera_2_nome) && sizeof($preghiera_2_nome) ) foreach( $preghiera_2_nome as $key1 => $value1 ){ $counter1++; ?>

                                <option value="<?php echo $value1["proclamatore_id"];?>" <?php if( $value1["selected"] ){ ?>selected=selected"<?php } ?>><?php if( $value1["proclamatore_id"]!=51 ){ ?><?php echo $value1["proclamatore_nome"];?> <?php echo $value1["proclamatore_cognome"];?> : <?php echo $value1["adunanza_data"];?><?php } ?></option>
                                <?php } ?>

                            </select>
                        </div>
                        <?php } ?>

                        <div class="col-md-10"></div>
                        <?php if( $congresso ){ ?>

                        <div class="col-md-2" id="submit_program_content">
                            <a  href="/life/adunanza" class="btn btn-primary">Esci</a>
                        </div>
                        <?php }else{ ?>

                        <div class="col-md-2" id="submit_program_content">
                            <button  type="submit" id="submit_programma" class="btn btn-primary">Aggiorna adunanza</button>
                        </div>
                        <?php } ?>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
