<?php if(!class_exists('raintpl')){exit;}?><div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo $action;?> adunanza<a href="http://wol.jw.org/ro/" id="open_wol" class="btn btn-primary" target="_blank">WOL</a></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="form_adunanza_head" method="post" action="<?php echo $url_create_programma_adunanza;?>">
                            <?php if( $update_mode ){ ?>

                            <?php } ?>


                            <div class="form-group col-md-2">
                                <input id="ad_data" name="ad_data" class="form-control datepicker" readonly placeholder="Inserisci data">
                            </div>
                            <?php if( $admin_mode ){ ?>

                            <div class="form-group col-md-2">
                                <div id="submit_head_content">
                                    <button  type="submit" id="submit_head" class="btn btn-default">Crea programma <img id="loading" class="hidden" src="template/adunanza/img/loading.gif"></button>
                                </div>
                                <div id="reset_form_content" class="hidden">
                                    <a href="/life/form-adunanza" id="reset_form" class="btn btn-default">Reset Pagina</a>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="ad_visita" name="ad_visita" value="1" >Visita del sorvegliante
                                    </label>
                                <label class="radio-inline">
                                    <input type="radio" name="ad_cong"  value="cong_circ">Assemblea di corcoscrizione
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="ad_cong"  value="cong_reg">Assemblea regionale
                                </label>
                            </div>




                        </form>
                    </div>
                </div>
                <div id="form_content" class="row">

                </div>
            </div>
        </div>
    </div>
</div>
