<?php if(!class_exists('raintpl')){exit;}?><div class="row">
    <div class="col-lg-6">
        <h1 class="page-header title-container">Elenco proclamatori</h1>
    </div>
    <div class="col-lg-6">
        <h1 class="page-header btn-container">
            <!--<a href="/stampa-proclamatori" target="_blank"><button class="btn btn-primary" class="modify">Stampa elenco proclamatori</button></a>-->
            <?php if( $admin_mode ){ ?>

            <a href="/life/form-proclamatore"><button class="btn btn-primary" class="modify">Nuovo proclamatore</button></a>
            <?php } ?>

        </h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-proclamatore">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOME</th>
                            <th>COGNOME</th>
                            <th>TELEFONO</th>
                            <th width="190px">AZIONI</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $counter1=-1; if( isset($proclamatore_list) && is_array($proclamatore_list) && sizeof($proclamatore_list) ) foreach( $proclamatore_list as $key1 => $value1 ){ $counter1++; ?>

                        <tr class="odd gradeX">
                            <td class=""><?php echo $value1["proclamatore_id"];?></td>
                            <td class=""><?php echo $value1["proclamatore_nome"];?></td>
                            <td class=""><?php echo $value1["proclamatore_cognome"];?></td>
                            <td class=""><?php echo $value1["proclamatore_telefono"];?></td>
                            <td class="">
                                <?php if( $admin_mode ){ ?>

                                <a href="/life/modifica-proclamatore_<?php echo $value1["proclamatore_id"];?>" class="btn btn-primary" class="modify">Modifica</a>
                                <button class="btn btn-primary" class="delete" onclick="return deleteProclamatore('<?php echo $value1["proclamatore_id"];?>')">Cancella</button>
                                <?php }else{ ?>

                                <a href="/life/modifica-proclamatore_<?php echo $value1["proclamatore_id"];?>" class="btn btn-primary" class="modify">Dettagli</a>
                                <?php } ?>

                            </td>
                        </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php if( $admin_mode ){ ?>

<div class="modal fade" id="modal_confirm_delete" role="dialog">
    <div class="modal-dialog">
        <form name="form_modal" id="form_modal" method="get" action="">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p id="confirm_msg"><?php echo $modify_msg;?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Annulla</button>
                    <a href="/life/<?php echo $cancella_proclamatore;?>" class="btn btn-primary" id="confirm_delete_act" >Conferma cancellazione</a>
                </div>
            </div>
        </form>
    </div>
</div> <!-- /.Modal -->
<?php } ?>