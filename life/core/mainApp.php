<?php
/**
 * Created by PhpStorm.
 * User: SS
 * Date: 25/03/2016
 * Time: 23:38
 */

class mainApp
{

    public function mainAppController($par = null)
    {
        self::includeFile();
        self::rainConfigure();
        if(empty($_SESSION["user"]) && ($par != "checklogin")){
            $par = "login";
        }
        self::dispatcherController($par);
    }

    protected function dispatcherController($par = null)
    {
        $array_route = array(
            //"home" => array("appView","defaultView"),
            "home" => array("adunanzaController","adunanzaList"),
            "adunanza" => array("adunanzaController","adunanzaList"),
            "form-adunanza" => array("adunanzaController","adunanzaFormHead"),
            "form-content-adunanza" => array("adunanzaController","adunanzaFormContent"),
            "check-data-adunanza" => array("adunanzaController","checkDataAdunanza"),
            "check-settimana-adunanza" => array("adunanzaController","checkWeek"),
            "get-punti" => array("proclamatoreController","getPunti"),
            "get-storia" => array("proclamatoreController","getStoria"),
            "modifica-adunanza" => array("adunanzaController","loadAdunanzaFromId"),
            "salva-adunanza" => array("adunanzaController","adunanzaSave"),
            "cancella-adunanza" => array("adunanzaController","deleteAdunanza"),
            "inserisci-nota" => array("notaController","insertNota"),
            "get-lista-note" => array("notaController","getNote"),
            "proclamatori" => array("proclamatoreController","proclamatoreList"),
            "form-proclamatore" => array("proclamatoreController","proclamatoreForm"),
            "salva-proclamatore" => array("proclamatoreController","proclamatoreSave"),
            "modifica-proclamatore" => array("proclamatoreController","proclamatoreForm"),
            "cancella-proclamatore" => array("proclamatoreController","proclamatoreDelete"),
            "get-proclamatore" => array("proclamatoreController","getProclamatore"),
            "stampa" => array("stampaController","stampaList"),
            "avvia-stampa" => array("stampaController","avviaStampa"),
            "test" => array("testController","test"),


            "login" => array("uacController","openFormLogin"),
            "logout" => array("uacController","logout"),
            "checklogin" => array("uacController","checkLogin")
        );
        $ctrl = null;
        if(array_key_exists($par, $array_route)) {
           $controller =  $array_route[$par][0];
           $method =  $array_route[$par][1];
           if (class_exists($controller)) {
               $ctrl = new $controller;
               $reflector = new ReflectionObject($ctrl);
               if ($reflector->hasMethod($method)) {
                   $method_go = $reflector->getMethod($method);
                   $method_go->invoke($ctrl);
               }
           }
        }else{
           echo 'classe inesistente - errore 404';
        }
    }

    protected function includeFile()
    {
        include("core/raintpl-master/inc/rain.tpl.class.php");

        $all_class = array("app", "proclamatore", "iscrizione", "adunanza", "servizioStampa", "uac", "menu", "user", "helper","stampa", "test", "nota");
        foreach($all_class as $one_class){

            if(file_exists("assets/".$one_class."/".$one_class."Controller.php")){
               include_once("assets/" . $one_class . "/" . $one_class . "Controller.php");
            }
            if(file_exists("assets/".$one_class."/".$one_class."Model.php")){
               include_once("assets/".$one_class."/".$one_class."Model.php");
            }
            if(file_exists("assets/".$one_class."/".$one_class."View.php")){
                include_once("assets/".$one_class."/".$one_class."View.php");
            }
        }
        include_once("assets/helper/dateTimeHelper.php");
        include_once("assets/helper/labelHelper.php");
        include_once("dbApp.php");

    }

    protected function rainConfigure()
    {

        raintpl::configure("base_url", null);
        raintpl::configure("tpl_dir", "template/");
        raintpl::configure("cache_dir", "core/tmp/");
    }


}