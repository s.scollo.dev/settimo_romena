/**
 * Created by SS on 29/05/2016.
 */
$(function() {

    $('#dataTables-adunanza').DataTable({
        "ordering": false,
    });


});

function deleteAdunanza(id_adunanza){
    var conferma = confirm('Vuoi cancellare davvero questa adunanza?');
    if (conferma === true) {
        var request = $.ajax({
            method: "post",
            url: 'cancella-adunanza',
            data: "id_adunanza=" + id_adunanza,
            dataType: "json"
        });
        request.done(function( resp ) {
            console.log(resp);
            alert(resp.mess);
            window.location.href = '/life/adunanza';
        });
        request.fail(function( jqXHR, textStatus ) {
            console.log("Errore "+ jqXHR +" | "+ textStatus);
        });
    }

}