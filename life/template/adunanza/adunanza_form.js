/**
 * Created by SS on 28/05/2016.
 */


var form_adunanza_head = $("#form_adunanza_head").attr("action");
var form_adunanza_content = $("#form_adunanza_content").attr("action");
var ad_data = $("#ad_data").val();
var ad_visita = $('#ad_visita').attr('checked');

$(document).ready(function() {
    var datepicker = $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        weekStart: 1
    });


    $( ".select_scuola" ).change(function() {
        var proc_id = this.value;
        var select_id = this.id;
        //estraggo nome proclamatore
        var option = $('#'+select_id+' option:selected').text();
        var opt_array = option.split(':');
        var nome_proc = opt_array[0];

        var array_id_select = select_id.split("_");
        var position_select = array_id_select[0];
        var id_select_punto = position_select+"_punto_"+array_id_select[3];
        var id_check_punto = position_select+"_punto_ok_"+array_id_select[3];
        var id_select_storia = position_select+"_storia_"+array_id_select[3];
        var id_lista_note = position_select+"_lista_nota_"+array_id_select[3];
        var commento = position_select+"_commento_"+array_id_select[3];
        //storico punti
        var get_punti = $.ajax({
            url: "get-punti_"+proc_id,
            method: "post",
            dataType: "json",
            data: "select_id="+select_id
        });
        get_punti.done(function(data) {

            var option_content = '';
            for(var i in data)
            {
                var superato = '';
                var id = data[i].punto_id;
                var desc = data[i].punto_desc;
                var ruoli = data[i].ruoli;
                if(data[i].superato == 0){
                    superato = "FARE";
                }else if (data[i].superato  == 1){
                    superato = 'OK';
                }

                option_content += "<option  value="+id+" >"+superato+" "+id+"____"+desc+" - RUOLI:"+ruoli+"</option>"
            }
            $("#"+id_select_punto).html(option_content);
            $('#'+id_check_punto).removeAttr('checked');

            //aggiorno id proclamatore
            $("#"+commento).attr("id_proc", proc_id);
        });
        //storia
        var get_storia = $.ajax({
            url: "get-storia_"+proc_id,
            method: "post",
            dataType: "json",
        });
        get_storia.done(function(data) {
            console.log(data);
            var option_content = '';
            for(var i in data)
            {
                var content = data[i].adunanza_data+" "+data[i].nome_dim+"  "+data[i].cognome_dim+" punto:"+data[i].punto_id+" ("+data[i].parte_tema+"), ass: "+data[i].nome_ass+" "+data[i].cognome_ass;
                option_content += "<option disabled>"+content+"</option>"
            }
            $("#"+id_select_storia).html(option_content);
        });
        //lista note
        var get_note = $.ajax({
            url: "get-lista-note",
            method: "post",
            data: "proc_id=" + proc_id,
            dataType: "json",
        });
        get_note.done(function(data) {
            console.log(data);
            var option_content = '';
            for(var i in data)
            {
                var content = data[i].nota_data+" "+data[i].nota_text;
                option_content += "<option disabled>"+content+"</option>"
            }
            $("#"+id_lista_note).html(option_content);
        });
    });

    $( ".nota" ).click(function() {
        var id_select_proc = this.getAttribute("id_select");
        var proc_id = $("#"+id_select_proc).val();
        var data = $("#data").val();
        console.log("nota del "+data+ " per proc id "+proc_id);
        //trovo nome proc
        var get_proc = $.ajax({
            url: "get-proclamatore_"+proc_id,
            method: "post",
            dataType: "json",
        });
        get_proc.done(function(info) {
            console.log("nota del "+data+ " per "+proc_id);
            //apro prompt con nome proc
            var nota = prompt("Inserisci la nota per "+info);
            if(nota.trim() != ''){
                //recupero dati : nota, data, proc_id
                //ajax per inserimento in db
                var insert_nota = $.ajax({
                    url: "inserisci-nota",
                    method: "post",
                    data: "nota="+nota + "&data=" + data + "&proc_id=" + proc_id,
                });
                insert_nota.done(function(esit) {
                    console.log(esit);
                    if(esit == 'ok'){
                        alert("Nota inserita");
                    }else{
                        alert("Inserimento nota fallito");
                    }
                });
            }
        });



    });

    $("#form_adunanza_head").validate({
        rules: {
            ad_data: {
                required: true,
                //verifico se esiste gia un'adunanza per questa settimana
                remote: {
                    url : "check-settimana-adunanza",
                    type: "post",
                    dataType: "text"
                }
            },
        },
        submitHandler: function(form) {
            $("#loading").removeClass("hidden");

            var request = $.ajax({
                method: "post",
                url: form_adunanza_head,
                data: $('#form_adunanza_head').serialize(),
                dataType: "text"
            });
            request.done(function( resp ) {
                //crea bottone wol
                var data = $("#ad_data").val();
                var data_ok = reverseData(data);
                var link = "http://wol.jw.org/ro/wol/dt/r34/lp-m/"+data_ok;
                $("#open_wol").attr("href",link);
                $("#loading").addClass("hidden");
                $("#submit_head_content").addClass("hidden");
                $("#reset_form_content").removeClass("hidden");
                $("#ad_data").attr("disabled","true");
                $("#ad_visita").attr("disabled","true");
                $("#form_content").html(resp);
                validateContentForm();
                //
                $( ".select_scuola" ).change(function() {
                    console.log('creo lista punti');
                    var proc_id = this.value;
                    var select_id = this.id;
                    var array_id_select = select_id.split("_");
                    var id_select_punto = "sb_punto_"+array_id_select[3];
                    var id_check_punto = "sb_punto_ok_"+array_id_select[3];
                    var get_punti = $.ajax({
                        url: "get-punti_"+proc_id,
                        method: "post",
                        dataType: "json",
                    });
                    get_punti.done(function(data) {
                        console.log(data);
                        var option_content = '';

                        for(var i in data)
                        {
                            var superato = '';
                            var id = data[i].punto_id;
                            var desc = data[i].punto_desc;
                            if(data[i].superato == 0){
                                superato = "FARE";
                            }else if (superato == 1){
                                superato = 'OK';
                            }

                            option_content += "<option  value="+id+" >"+superato+" "+id+"____"+desc+"</option>"
                        }
                        $("#"+id_select_punto).html(option_content);
                        $('#'+id_check_punto).removeAttr('checked');
                    });
                });
                //
            });
            request.fail(function( jqXHR, textStatus ) {
                console.log("Errore "+ jqXHR +" | "+ textStatus);
            });
        },
        invalidHandler: function() {
            return false;
        }
    });
    validateContentForm();
});



function validateContentForm(){
    $("#form_adunanza_content").validate({
        onkeyup: function(element,event){
            if ( this.elementValue( element ) !== "" && (event.keyCode === 9 || event.keyCode === 13 || element.name in this.submitted) ) {
                this.element( element );
            }
        },
        rules: {
            lett_sett: {
                required: true,
            },
        },
        submitHandler: function(form) {
            var request = $.ajax({
                method: "post",
                url: "salva-adunanza",
                data: $('#form_adunanza_content').serialize(),
                dataType: "json"
            });
            request.done(function( resp ) {
                console.log(resp.esito);
                if(resp.esito == 'OK'){
                    alert(resp.mess);
                    $("#submit_programma").addClass("hidden");
                    window.location.href = '/life/adunanza';
                }else{
                    alert('QUALCOSA NON HA FUNZIONATO, RIPROVA');
                }


            });
            request.fail(function( jqXHR, textStatus ) {
                console.log("Errore "+ jqXHR +" | "+ textStatus);
            });
        },
        invalidHandler: function() {
            return false;
        }
    });
}

function reverseData(data){
    var array_data = data.split("/");
    var result = array_data[2]+"/"+array_data[1]+"/"+array_data[0];
    return result;
}