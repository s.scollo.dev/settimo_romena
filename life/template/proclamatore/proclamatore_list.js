/**
 * Created by salvatore on 05/04/2016.
 */

$(function() {

    $('#dataTables-proclamatore').DataTable({
        responsive: true
    });


});

function deleteProclamatore(id_proclamatore){
    var href = $("#confirm_delete_act").attr("href");
    var new_href = href+id_proclamatore;
    $("#confirm_delete_act").attr("href",new_href)
    $("#modal_confirm_delete").modal("show");
}
