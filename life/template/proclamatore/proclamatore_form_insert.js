/**
 * Created by salvatore on 05/04/2016.
 */

var insert_proclamatore = $("#insert_proclamatore").val();

$(document).ready(function() {

    $("#form_proclamatore").validate({
            onkeyup: function(element,event){
                if ( this.elementValue( element ) !== "" && (event.keyCode === 9 || event.keyCode === 13 || element.name in this.submitted) ) {
                    this.element( element );
                }
            },
            rules: {
                proclamatore_nome: {
                    required: true
                    },
                proclamatore_cognome: {
                    required: true
                },
                proclamatore_telefono:{
                    number: true
                }
            },
            submitHandler: function(form) {
            var request = $.ajax({
                method: "post",
                url: insert_proclamatore,
                data: $('#form_proclamatore').serialize(),
                dataType: "json"
            });
            request.done(function( resp ) {
                if ( resp.esito == "OK" || resp.esito == "KO" ) {
                    $(".dialog_msg").text( resp.mess );
                    $("#modal_dialog_proclamatore").modal("show");
                }
            });
            request.fail(function( jqXHR, textStatus ) {
                console.log("Errore "+ jqXHR +" | "+ textStatus);
            });
        },
        invalidHandler: function() {
            return false;
        }

    });
});