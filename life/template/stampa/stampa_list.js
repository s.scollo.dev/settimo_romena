/**
 * Created by SS on 29/05/2016.
 */
var id_list = '';
var azione = '';
$(function() {

    $('#dataTables-adunanza').DataTable({
        "ordering": false,
    });

    //mostra-nascondi pulsanti azioni
    $( ".check_stampa" ).click(function() {
        var ischecked = $(this).is(":checked");
        if (ischecked) {
            if(id_list == ""){
                id_list += $(this).val()
            }else{
                id_list +=   "," + $(this).val();
            }
        }
        if(id_list != ''){
            $(".azione").removeClass("hidden");
        }else{
            $(".azione").addClass("hidden");
        }
    });

    //valorizza input azione
    $( ".azione" ).click(function() {
        azione = $(this).attr("azione");
        $("#azione").val(azione);
    });
    //validazione ed invio dati form
    $("#form_stampa").validate({
        submitHandler: function(form) {
            $("#loading").removeClass("hidden");
            var request = $.ajax({
                method: "post",
                url: "avvia-stampa",
                data: $('#form_stampa').serialize(),
                dataType: "text"
            });
            request.done(function( file ) {
                $("#loading").addClass("hidden");
                window.open(file);
            });
            request.fail(function( jqXHR, textStatus ) {
                console.log("Errore "+ jqXHR +" | "+ textStatus);
            });
        },
        invalidHandler: function() {
            return false;
        }
    });
});

