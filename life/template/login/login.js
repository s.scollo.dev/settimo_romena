/**
 * Created by SS on 01/04/2016.
 */


(function($){
    $.getAlertError = function (alert_message) {
        return '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Errore login!</strong> <span id="alert-error-message">'+alert_message+'</span></div>';
    };
})(jQuery);

$(function(){


    var alert_error_container = $("#alert-error-container");

    var login_form = $("#login_form");

    var button_submit = login_form .find("button");

    login_form.submit(function(e){

       // button_submit.prop("disabled",true);
       // button_submit.find("i.fa").show();

        var form_data = $(this).serializeArray();
        var url_ajax = $(this).attr("action");
        var ajax_login = $.ajax({
            url: url_ajax,
            method: "post",
            dataType: "json",
            data: form_data
        });

        ajax_login.done(function(data) {
            console.log(data);
            if (data.esito == "SI") {
                window.location.href = data.url_go;
            }
            else {
                alert_error_container.children().html($.getAlertError(data.messaggio));
                alert_error_container.show();
                button_submit.find("i.fa").hide();
                button_submit.prop("disabled",false);
            }

        });

        ajax_login.fail(function() {
            alert_error_container.children().html($.getAlertError("Richiesta fallita. Verificare la connessione o contattare l'assistenza."));
            alert_error_container.show();
        });

        ajax_login.always(function() {

            window.location.href = data.url_go;
        });

        e.preventDefault();
        return false;
    });



});

