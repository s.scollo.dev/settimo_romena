<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 05/04/2016
 * Time: 14:57
 */
class dateTimeHelper
{
    public static function slash2dash($date)
    {
        if($date == "00/00/0000" || $date == ""){
            return null;
        }else{
            $date_array = explode("/", $date);
            $new_date = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
            return $new_date;
        }
    }

    public static function dash2slash($date)
    {
        if($date == "0000-00-00" || $date == ""){
            return null;
        }else{
            $date_array = explode("-", $date);
            $new_date = $date_array[2] . '/' . $date_array[1] . '/' . $date_array[0];
            return $new_date;
        }
    }

    public static function dataRomena($data){
        $mesi_ro = array(
            "01" =>"Ianuarie",
            "02" =>"Februarie",
            "03" =>"Martie",
            "04" =>"Aprilie",
            "05" =>"Mai",
            "06" =>"Iunie",
            "07" =>"Iulie",
            "08" =>"August",
            "09" =>"Septembrie",
            "10" =>"Octombrie",
            "11" =>"Noiembrie",
            "12" =>"Decembrie"
        );

        $array_data = explode("/",$data);
        $new_data = $array_data[0]." ".$mesi_ro[$array_data[1]]." ".$array_data[2];
        return $new_data;
    }

}