<?php

/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 11/04/2016
 * Time: 16:41
 */
class pdfHelper
{
    public function convPrint( $str, $utf8=true )
    {
        $str = (string)$str;

        if(!$utf8)
            $str = utf8_encode($str);

        $transliteration = array(                         // il valore di destra non serve, solo la chiave serve
            '�' => 'E',     '�' => '�',
            '�' => 'A',     '�' => 'a',     '�' => 'O',    '�' => 'o',
            '�' => "A'",    '�' => "A'",    '�' => 'A',    '�' => 'A',    '�' => 'A',    '�' => 'A',
            '�' => "a'",    '�' => "a'",    '�' => 'a',    '�' => 'a',    '�' => 'a',    '�' => 'a',
            '�' => "E'",    '�' => "E'",    '�' => 'E',    '�' => 'E',
            '�' => "e'",    '�' => "e'",    '�' => 'e',    '�' => 'e',
            '�' => "I'",    '�' => "I'",    '�' => 'I',    '�' => 'I',
            '�' => "i'",    '�' => "i'",    '�' => 'i',    '�' => 'i',
            '�' => "O'",    '�' => "O'",    '�' => 'O',    '�' => 'O',    '�' => 'O',    '�' => 'O',
            '�' => "o'",    '�' => "o'",    '�' => 'o',    '�' => 'o',    '�' => 'o',    '�' => 'o',
            '�' => "U'",    '�' => "U'",    '�' => 'U',    '�' => 'U',
            '�' => "u'",    '�' => "u'",    '�' => 'u',    '�' => 'u',
            '�' => "Y'",    '�' => "y'",    '�' => 'Y',    '�' => 'y',
            '�' => 'C',     '�' => 'c',     '�' => 'N',    '�' => 'n',    '�' => 'f',
            '�' => 'S',     '�' => 's',     '�' => 'Z',    '�' => 'z',    '�' => 's',
            '�' => 'D',     '�' => 'd',     '�' => 'T',    '�' => 't',    '�' => '�'
        );

        foreach (array_keys( $transliteration ) as $key) {
            if ( strpos($str, $key) !== false ) {
                $pdfkey = iconv( 'utf-8', 'cp1252', $key );
                $str = str_replace( $key, $pdfkey, $str );
            }
        }
        return $str;
    }
}