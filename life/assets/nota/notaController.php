<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 20/01/2017
 * Time: 06:57
 */
class notaController
{

    public function insertNota(){
        $exit = "ko";
        $data = trim($_POST['data']);
        $data = dateTimeHelper::slash2dash($data);
        $nota = trim($_POST['nota']);
        $proc_id = trim($_POST['proc_id']);
        $nota_model = new notaModel();
        $nota_model->setData($data);
        $nota_model->setNota($nota);
        $nota_model->setProcId($proc_id);
        if($nota_model->insertNota() == true){
            $esit = "ok";
        }
        echo $esit;
    }

    public function getNote(){
        $proc_id = trim($_POST['proc_id']);
        $nota_model = new notaModel();
        $nota_model->setProcId($proc_id);
        $result = $nota_model->getNoteByProcId();
        echo json_encode($result);
    }
}