<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 20/01/2017
 * Time: 06:57
 */
class notaModel
{
    protected $nota;
    protected $data;
    protected $proc_id;

    /**
     * @return mixed
     */
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * @param mixed $nota
     */
    public function setNota($nota)
    {
        $this->nota = $nota;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getProcId()
    {
        return $this->proc_id;
    }

    /**
     * @param mixed $proc_id
     */
    public function setProcId($proc_id)
    {
        $this->proc_id = $proc_id;
    }

    public function insertNota(){
        $q_info = array(
            ":nota_data" => $this->data,
            ":nota_proc_id" => $this->proc_id,
            ":nota_text" => $this->nota
        );
        $cn = dbAppHandler::getCon();
        $query = "SELECT * FROM nota WHERE nota_proc_id = :nota_proc_id AND nota_data=:nota_data AND nota_text <> :nota_text";
        $obj = $cn->prepare($query);
        $obj->execute($q_info);
        $result = $obj->fetch(PDO::FETCH_ASSOC);

        if(is_array($result) && count($result) > 0){
            $query = "UPDATE nota SET nota_text=:nota_text WHERE nota_proc_id = :nota_proc_id AND nota_data=:nota_data";
        }else{
            $query = "INSERT INTO nota (nota_data, nota_proc_id, nota_text) VALUES (:nota_data, :nota_proc_id, :nota_text)";
        }

        try {
            $cn = dbAppHandler::getCon();
            $insert_nota = $cn->prepare($query);
            $insert_nota->execute($q_info);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getNoteByProcId(){
        $proc_id = $this->proc_id;
        $cn = dbAppHandler::getCon();
        $query = "SELECT * FROM nota WHERE nota_proc_id = $proc_id";
        $obj = $cn->prepare($query);
        $obj->execute();
        $result = $obj->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}