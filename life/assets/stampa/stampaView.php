<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/05/2016
 * Time: 12:56
 */
class stampaView extends RainTPL
{
    const LISTA_JS = "../stampa/stampa_list.js";
    const TITOLO_LISTA = "Gestione stampa";


    public function renderAdunanzaList($var){
        $this->assign("adunanza_list",$var);
       // $this->assign("modify_msg","Vuoi cancellare adunanza?");
       // $this->assign("cancella_adunanza","cancella-adunanza_");

        return $this->draw("stampa/stampa_list",true);
    }


}