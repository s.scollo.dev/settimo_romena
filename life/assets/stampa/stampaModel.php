<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/05/2016
 * Time: 12:56
 */
class stampaModel
{
    const TAB_PROCLAMATORI = "proclamatori";

    protected $ad_data;
    protected $week;
    protected $visita;
    protected $ad_cong;
    protected $lett_sett;
    protected $pres;
    protected $preg_1;
    protected $preg_2;
    protected $cant_1;
    protected $cant_2;
    protected $cant_3;
    protected $sec_A = array();
    protected $sec_B = array();
    protected $sec_C = array();

    public function getAdData()
    {
        return $this->ad_data;
    }

    public function setAdData($ad_data)
    {
        $this->ad_data = $ad_data;
    }

    public function getWeek()
    {
        return $this->week;
    }

    public function setWeek($week)
    {
        $this->week = $week;
    }

    public function getVisita()
    {
        return $this->visita;
    }

    public function setVisita($visita)
    {
        $this->visita = $visita;
    }

    public function getAdCong()
    {
        return $this->ad_cong;
    }

    public function setAdCong($ad_cong)
    {
        $this->ad_cong = $ad_cong;
    }

    public function getLettSett()
    {
        return $this->lett_sett;
    }

    public function setLettSett($lett_sett)
    {
        $this->lett_sett = $lett_sett;
    }

    public function getPres()
    {
        return $this->pres;
    }

    public function setPres($pres)
    {
        $this->pres = $pres;
    }

    public function getPreg1()
    {
        return $this->preg_1;
    }

    public function setPreg1($preg_1)
    {
        $this->preg_1 = $preg_1;
    }

    public function getPreg2()
    {
        return $this->preg_2;
    }

    public function setPreg2($preg_2)
    {
        $this->preg_2 = $preg_2;
    }

    public function getCant1()
    {
        return $this->cant_1;
    }

    public function setCant1($cant_1)
    {
        $this->cant_1 = $cant_1;
    }

    public function getCant2()
    {
        return $this->cant_2;
    }

    public function setCant2($cant_2)
    {
        $this->cant_2 = $cant_2;
    }

    public function getCant3()
    {
        return $this->cant_3;
    }

    public function setCant3($cant_3)
    {
        $this->cant_3 = $cant_3;
    }

    public function getSecA()
    {
        return $this->sec_A;
    }

    public function setSecA($sec_A)
    {
        $this->sec_A = $sec_A;
    }

    public function getSecB()
    {
        return $this->sec_B;
    }

    public function setSecB($sec_B)
    {
        $this->sec_B = $sec_B;
    }

    public function getSecC()
    {
        return $this->sec_C;
    }

    public function setSecC($sec_C)
    {
        $this->sec_C = $sec_C;
    }

    public function getAllAdunanza(){
        try {
            $cn = dbAppHandler::getCon();
            $query = "SELECT * FROM adunanza order by adunanza_data desc";
            $obj= $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function loadFromId($adunanza_id)
    {

        $result = array();
        $result['adunanza_id'] = $adunanza_id;
        $result['sec_A'] = array();
        $result['sec_B'] = array();
        $result['sec_C'] = array();

        $query_adun = "SELECT
                        adunanza_data AS ad_data,
                        adunanza_cantico_1 AS cantico_1,
                        adunanza_cantico_2 AS cantico_2,
                        adunanza_cantico_3 AS cantico_3,
                        adunanza_lettura_sett AS lett_sett,
                        adunanza_visita AS visita,
                        adunanza_congresso AS congresso,
                        adunanza_pres AS pres,
                        adunanza_preg_1 AS preg_1,
                        adunanza_preg_2 AS preg_2
                        FROM adunanza
                        WHERE adunanza.adunanza_id  = $adunanza_id ";
        try {
            $cn = dbAppHandler::getCon();
            $obj = $cn->prepare($query_adun);
            $obj->execute();
            $all_1 = $obj->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }

        foreach ($all_1 as $key => $val) {
            $result[$key] = $all_1[$key];
        }

        if(!isset($all_1['congresso']) || $all_1['congresso'] == '' ){
           $query_parte = "SELECT parte_id , parte_tema AS tema, parte_tempo AS tempo, parte_id_adunanza AS adunanza_id, parte_id_sezione AS sezione_id, parte_order FROM parte
                            WHERE parte.parte_id_adunanza = $adunanza_id ORDER BY parte_id, parte_order";
            try {
                $cn = dbAppHandler::getCon();
                $obj = $cn->prepare($query_parte);
                $obj->execute();
                $all_2 = $obj->fetchAll(PDO::FETCH_ASSOC);
                // echo '<pre>';
                // print_r($all_2);
                // echo '<hr>';
            } catch (PDOException $e) {
                trigger_error($e->getMessage(), E_USER_ERROR);
            }
            $proc_model = new proclamatoreModel();
            foreach ($all_2 as $key => $val) {
                $temp = array();
                $temp['tema'] = $all_2[$key]['tema'];
                $temp['tempo'] = $all_2[$key]['tempo'];
                $id_adunanza = $all_2[$key]['adunanza_id'];
                $id_sezione = $all_2[$key]['sezione_id'];
                $id_parte = $all_2[$key]['parte_id'];

                $PR = $this->getPR4parte($id_adunanza, $id_sezione, $id_parte);

                $list_position_2 = array(6,9);
                foreach ($PR as $key => $val) {
                    $c = 1;
                    if (isset($PR[$key]['ap_id_punto'])) {
                        $temp['punto'] = $PR[$key]['ap_id_punto'];
                    }
                    $ruolo = $PR[$key]['ap_id_ruolo'];
                    if(in_array($ruolo,$list_position_2)){
                        $c = 2;
                    }
                    $temp['ruolo_' . $c] = $PR[$key]['ap_id_ruolo'];
                    $temp['proclamatore_' . $c] = $PR[$key]['ap_id_proclamatore'];
                    $temp['proclamatore_' . $c] = $proc_model->loadFromId($PR[$key]['ap_id_proclamatore'])->getProclamatoreNome() . ' ' . $proc_model->loadFromId($PR[$key]['ap_id_proclamatore'])->getProclamatoreCognome();
                }

                if ($id_sezione == 1) {
                    $result['sec_A'][] = $temp;
                }
                if ($id_sezione == 2) {
                    $result['sec_B'][] = $temp;
                }
                if ($id_sezione == 3) {
                    $result['sec_C'][] = $temp;
                }
            }

            //estrai nome preghiere, presidente
            $result['pres'] = $proc_model->loadFromId($result['pres'])->getProclamatoreNome() . ' ' . $proc_model->loadFromId($result['pres'])->getProclamatoreCognome();
            $result['preg_1'] = $proc_model->loadFromId($result['preg_1'])->getProclamatoreNome() . ' ' . $proc_model->loadFromId($result['preg_1'])->getProclamatoreCognome();
            $result['preg_2'] = $proc_model->loadFromId($result['preg_2'])->getProclamatoreNome() . ' ' . $proc_model->loadFromId($result['preg_2'])->getProclamatoreCognome();

        }

        return $result;
    }

    private function getPR4parte($id_adunanza,$id_sezione,$id_parte){
        try {
            $cn = dbAppHandler::getCon();
            $query = "SELECT
                            ap_id_parte,
                            ap_id_proclamatore,
                            ap_id_ruolo,
                            ap_id_punto,
                            ap_id_adunanza,
                            ap_id_sezione
                        FROM adunanza_proclamatore
                        WHERE ap_id_adunanza = $id_adunanza
                        AND ap_id_sezione = $id_sezione
                        AND ap_id_parte = $id_parte ";
            $obj= $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function getLabelByRuolo($id_ruolo){
        try {
            $cn = dbAppHandler::getCon();
            $query = "SELECT ruolo_desc FROM ruoli WHERE ruolo_id = $id_ruolo";
            $obj= $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetch();
            return $result[0];
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

}
