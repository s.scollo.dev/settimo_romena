<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/05/2016
 * Time: 12:56
 */
//MODIFICHE
// 29/12 gestione nuovo programma 2018 (aggiunto punto 0 in datatabse)

include_once("assets/plugin/script92/tfpdf.php");

class stampaController
{
    public function stampaList(){
        //definisco tipo di utente
        $admin_user = uacController::checkAdminUser();

        $ad_model = new stampaModel();
        $adunanza_list = $ad_model->getAllAdunanza();
        //print_R($adunanza_list);
        foreach($adunanza_list as $key =>$one_ad){
            $adunanza_list[$key]['adunanza_data'] = dateTimeHelper::dash2slash($adunanza_list[$key]['adunanza_data']);
        }

        $ad_view = new stampaView();
        $ad_view->assign("admin_mode", $admin_user);

        $app_update = new appView();
        $app_update->local_js = $ad_view::LISTA_JS;
        $app_update->titolo_pagina = $ad_view::TITOLO_LISTA;
        $app_update->html_wrapper = $ad_view->renderAdunanzaLIst($adunanza_list);
        $app_update->defaultView();

    }

    public function avviaStampa(){
        $da_stampare = $_POST['selezione'];
        $da_stampare = array_reverse($da_stampare);
        $type = $_POST['azione'];
        $result =  $this->generaProgramma($da_stampare,$type);
        switch ($type) {
            case 'assegnazioni':
                $result =  $this->generaAssegnazioni($da_stampare);
                break;
            case 'tabella':
                $result =  $this->generaProgramma($da_stampare,$type);
                break;
            case 'presidente':
                $result =  $this->generaProgramma($da_stampare,$type);
                break;
            case 'mail':
                $result =  $this->generaProgramma($da_stampare,$type);
                break;
        }
        echo $result;

    }



    private function generaProgramma($da_stampare,$type){
        // TODO : gestione nomi minuscoli-maiuscoli
        //set etichette
        $programma = labelHelper::getLabel('programma');
        $sezione_1 = labelHelper::getLabel('sezione_1');
        $sezione_2 = labelHelper::getLabel('sezione_2');
        $sezione_3 = labelHelper::getLabel('sezione_3');
        $preghiera = labelHelper::getLabel('preghiera');
        $cantico = labelHelper::getLabel('cantico');
        $ripasso = labelHelper::getLabel('ripasso');
        $presidente = labelHelper::getLabel('presidente');
        $lettore = labelHelper::getLabel('lettore');

        //pdf
        $pdf = new tFPDF();
        $pdf->SetAutoPageBreak(false, 0); //trucchetto per portare margin-bottom a 0
        $pdf->AddPage();
        $pdf->AddFont('DejaVu','B','DejaVuSans-Bold.ttf',true);

        $model = new stampaModel();

        $start = 40;
        $position = 0;
        $count = 0;
        $font_data = 11;
        $font_sezione = 9;
        $font_std = 8;

        $pdf->SetFont('DejaVu','',26);
        $pdf->SetY(10);
        $pdf->SetTextColor(91, 35, 20);
        $pdf->Cell(190,12,$programma, 1,0,'C');
        $my_y = $pdf->GetY() + 14;
        foreach($da_stampare as $one){
            if($count > 0){
                if($pdf->GetY() > 180){
                    $pdf->AddPage();
                    $pdf->SetFont('DejaVu','',26);
                    $pdf->SetTextColor(91, 35, 20);
                    $pdf->SetY(10);
                    $pdf->Cell(190,12,$programma, 1,0,'C');
                    $my_y = $pdf->GetY() + 14;
                }else{
                    $my_y = $pdf->GetY() + 7;
                }
            }



            $info_adunanza = $model->loadFromId($one);
            //ANALISI SINGOLA ADUNANZA

            //gestione data programma
            $data = $info_adunanza['ad_data'];
            $giovedi = new DateTime($data);
            $day_of_week = $giovedi->format('w');
            $step_day_domenica = 7 - $day_of_week;
            $step_day_lunedi = $day_of_week - 1 ;
            $domenica = clone $giovedi;
            $lunedi = clone $giovedi;
            $lunedi = $lunedi->modify( '-'.$step_day_lunedi.' day' )->format('d/m/Y');
            $lunedi = dateTimeHelper::dataRomena($lunedi);
            $domenica = $domenica->modify( '+'.$step_day_domenica.' day' )->format('d/m/Y');
            $domenica = dateTimeHelper::dataRomena($domenica);
            $data = $lunedi.' - '.$domenica;

            $cantico_1 = $info_adunanza['cantico_1'];
            $cantico_2 = $info_adunanza['cantico_2'];
            $cantico_3 = $info_adunanza['cantico_3'];
            if(isset($info_adunanza['lett_sett']) && trim($info_adunanza['lett_sett'])  != '' ){
                $lett_sett = " (".ucwords(strtolower($info_adunanza['lett_sett'])).")";
            }

            $visita = $info_adunanza['visita'];

            $pres = $info_adunanza['pres'];
            $preg_1 = $info_adunanza['preg_1'];
            $preg_2 = $info_adunanza['preg_2'];

            //data e lettura
            $pdf->SetFont('DejaVu','',$font_data);
            $pdf->SetFillColor(91, 35, 20);
            $pdf->SetTextColor(255,255,255);
            $pdf->SetY($my_y);
            $pdf->Cell(190,$font_data-3,$data.'  '.$lett_sett, 0,1,'C',true);
            $pdf->Cell(190,2,'', 0,1,'C');
            $pdf->SetTextColor(0,0,0);
            //congresso
            if(isset($info_adunanza['congresso']) && $info_adunanza['congresso'] != ''){
                $congresso = labelHelper::getLabel($info_adunanza['congresso']);
                $pdf->Cell(190,$font_data-3,$congresso, 0,1,'C');
                $pdf->SetTextColor(0, 0, 0);

            }else {

                $pdf->AddFont('DejaVu', '', 'DejaVuSansCondensed.ttf', true);
                $pdf->SetFont('DejaVu', '', $font_std);
                $pdf->SetY($my_y + 9);
                $pdf->Cell(110, $font_std - 2, $cantico_1, 0, 0, 'L');
                $pdf->Cell(25, $font_std - 2, $preghiera . ": ", 0, 0, 'L');
                $pdf->Cell(55, $font_std - 2, $preg_1, 0, 1, 'R');
                $pdf->Cell(135, $font_std - 2, "Cuvinte introductive", 0, 0, 'L');
                $pdf->Cell(55, $font_std - 2, $pres, 0, 1, 'R');

                //sezione 1
                $pdf->SetFont('DejaVu', '', $font_sezione);
                $pdf->SetFillColor(90, 106, 112);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Cell(190, $font_sezione - 3, $sezione_1, 0, 1, 'C', true);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(190, 1, '', 0, 1);
                foreach ($info_adunanza['sec_A'] as $sez_1) {
                    $pdf->SetFont('DejaVu', '', $font_std);
                    $pdf->Cell(85, $font_std - 2, $sez_1['tema'] . " " . $sez_1['tempo'], 0, 0, 'L');

                    if (($type == 'presidente') && (isset($sez_1['punto']) && trim($sez_1['punto']) != 0)) {
                        $punto = "Calitatea: " . $sez_1['punto'];
                    }else{
                        $punto = '';
                    }
                    $pdf->Cell(50, $font_std - 1, $punto, 0, 0, 'L');
                    $pdf->SetFont('DejaVu', '', $font_std);
                    $pdf->Cell(55, $font_std - 2, $sez_1['proclamatore_1'], 0, 1, 'R');

                }

                //sezione 2
                $pdf->Cell(190, 1, '', 0, 1);
                $pdf->SetFont('DejaVu', '', $font_sezione);
                $pdf->SetFillColor(193, 134, 38);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Cell(190, $font_sezione - 3, $sezione_2, 0, 1, 'C', true);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(190, 1, '', 0, 1);
                foreach ($info_adunanza['sec_B'] as $sez_2) {
                    if (isset($sez_2['proclamatore_2']) && trim($sez_2['proclamatore_2']) != '') {
                        $proc_2 = " - " . $sez_2['proclamatore_2'];
                    } else {
                        $proc_2 = '';
                    }
                    $pdf->SetFont('DejaVu', '', $font_std);
                    $pdf->Cell(85, $font_std - 1, $sez_2['tema'] . " " . $sez_2['tempo'], 0, 0, 'L');
                    if (($type == 'presidente') && (isset($sez_2['punto']) && $sez_2['punto'] != 0)) {
                        $punto = "Calitatea: " . $sez_2['punto'];
                    }else{
                        $punto = '';
                    }

                    $pdf->Cell(50, $font_std - 1, $punto, 0, 0, 'L');
                    $pdf->SetFont('DejaVu', '', $font_std);
                    $pdf->Cell(55, $font_std - 2, $sez_2['proclamatore_1'] . $proc_2, 0, 1, 'R');
                }

                //sezione 3
                $pdf->Cell(190, 1, '', 0, 1);
                $pdf->SetFont('DejaVu', '', $font_sezione);
                $pdf->SetFillColor(150, 21, 38);
                $pdf->SetTextColor(255, 255, 255);
                $pdf->Cell(190, $font_sezione - 3, $sezione_3, 0, 1, 'C', true);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->Cell(190, 1, '', 0, 1);
                $pdf->SetFont('DejaVu', '', $font_std);
                $pdf->Cell(190, $font_std - 2, $cantico_2, 0, 1, 'L');
                foreach ($info_adunanza['sec_C'] as $sez_3) {
                    if (isset($sez_3['tema']) && $sez_3['tema'] != '') {
                        $pdf->SetFont('DejaVu', '', $font_std);
                        $pdf->Cell(135, $font_std - 2, $sez_3['tema'] . " " . $sez_3['tempo'], 0, 0, 'L');
                        $pdf->SetFont('DejaVu', '', $font_std);
                        $pdf->Cell(55, $font_std - 2, $sez_3['proclamatore_1'], 0, 1, 'R');
                        if (isset($sez_3['ruolo_2']) ) { //&& $sez_3['ruolo_2'] == 'Lettore studio biblico'
                            $pdf->Cell(135, $font_std - 2, "Cititor: ", 0, 0, 'R');
                            $pdf->Cell(55, $font_std - 2, $sez_3['proclamatore_2'], 0, 1, 'R');
                        }
                    }
                }
                //fine
                $pdf->Cell(135, $font_std - 2, 'Recapitulare:', 0, 0, 'L');
                $pdf->Cell(55, $font_std - 2, $pres, 0, 1, 'R');
                $pdf->Cell(110, $font_std - 2, $cantico_3, 0, 0, 'L');
                $pdf->Cell(25, $font_std - 2, $preghiera . ": ", 0, 0, 'L');
                $pdf->Cell(55, $font_std - 2, $preg_2, 0, 1, 'R');
            }


            $count++;
        }

        $pdf->Output("repository/".$type.".pdf",'F',true);
        return "repository/".$type.".pdf";
    }

    private function generaAssegnazioni($da_stampare){
        $pdf = new tFPDF();
        $pdf->AddPage();
        //$pdf->SetFont('Arial','B',13);
        $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
        $pdf->SetFont('DejaVu','',12);
        $model = new stampaModel();
        $count = 1;
        foreach($da_stampare as $one) {
            $info_adunanza = $model->loadFromId($one);

            if (!isset($info_adunanza['congresso']) || $info_adunanza['congresso'] == '') {

                //ANALISI SINGOLA ADUNANZA
                $ad_data = $info_adunanza['ad_data'];
                $data = dateTimeHelper::dash2slash($ad_data);
                //stampo lettura
                $start = $this->getPosition(1);
                $citirea = $info_adunanza['sec_A'][2];
                $pdf->Image("assets/stampa/img/assegnazione.png", $start["x"], $start["y"], -300);
                $pdf->SetY($start["y"] + 22);
                $pdf->Cell(93, 9, $citirea['proclamatore_1'], 0, 1, 'R');
                $pdf->Cell(93, 9, '', 0, 1, 'R');
                $pdf->Cell(93, 9, $data, 0, 1, 'R');
                //$pdf->Cell(93, 9, $citirea['punto'], 0, 0, 'R');
                $pdf->Cell(93, 9, 'vedi programma', 0, 0, 'R');


                //flag
                $pdf->SetY($start["y"] + $this->getFlag(1));
                $pdf->Cell(93, 8, '             x', 0, 0, 'L');
                //sezione B
                if (count($info_adunanza['sec_B']) > 1) {
                    $position = 2;
                    foreach ($info_adunanza['sec_B'] as $one) {
						if(trim($one['proclamatore_1']) != ''){
							$start = $this->getPosition($position);
							$pdf->Image("assets/stampa/img/assegnazione.png", $start["x"] + 17, $start["y"], -300);
							$pdf->SetY($start["y"] + 22);
							$pdf->Cell($start["x"], 9, '', 0, 0, 'R');
							$pdf->Cell(93, 9, $one['proclamatore_1'], 0, 1, 'R');
							$pdf->Cell($start["x"], 9, '', 0, 0, 'R');
							$pdf->Cell(93, 9, $one['proclamatore_2'], 0, 1, 'R');
							$pdf->Cell($start["x"], 9, '', 0, 0, 'R');
							$pdf->Cell(93, 9, $data, 0, 1, 'R');
							$pdf->Cell($start["x"], 9, '', 0, 0, 'R');
							$pdf->Cell(93, 9, 'vedi programma', 0, 0, 'R');
							//MODIFICA TEMPORANEA AGGIORNAMENTO 2018
							$pdf->SetY($start["y"] + 60);
							$pdf->Cell($start["x"]+10, 9, '', 0, 0, 'R');
							$pdf->Cell(93, 8, $one['tema'], 0, 0, 'L');
							//POSIZIONAMENTO FLAG
							/*
							 * CORREGGERE METODO
							$pdf->SetY($start["y"] + $this->getFlag($position));
							$pdf->Cell($start["x"], 9, '', 0, 0, 'R');
							$pdf->Cell(93, 8, '             x', 0, 0, 'L');
							*/
							$position++;
						}
                    }
                }
                if (count($da_stampare) > $count) {
                    $pdf->AddPage();
                    $count++;
                }
            }
        }
        $pdf->Output("repository/assegnazioni.pdf",'F',true);
        return "repository/assegnazioni.pdf";
    }


    private function getPosition($position){
        $start = array("x"=> 0, "y"=> 0);
        switch ($position) {
            case 1:
                $start = array("x"=> 18, "y"=> 12);
                break;
            case 2:
                $start = array("x"=> 90, "y"=> 12);
                break;
            case 3:
                $start = array("x"=> 1, "y"=> 151);
                break;
            case 4:
                $start = array("x"=> 90, "y"=> 151);
                break;
        }
        return $start;
    }

    private function getSpace($n){
        $res = null;
        for($c=0; $c<=$n; $c++){
            $res .= ' ';
        }
        return $res;
    }

    private function getFlag($position){
        $h_flag = 0;
        if($position == 1){
            $h_flag = 70;
        }
        /*elseif($position == 2){
            $h_flag = 75;
        }elseif($position == 3){
            $h_flag = 80;
        }elseif($position == 4){
            $h_flag = 85;
        }
        */
        return $h_flag;
    }

    public function test(){
        $da_stampare = $_POST['selezione'];
        $da_stampare = array_reverse($da_stampare);
        echo '<pre>';
        print_r($da_stampare);
    }


}