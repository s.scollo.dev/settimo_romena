<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/03/2016
 * Time: 23:42
 */


class uacController
{
    protected $user_name;
    protected $user_type;

    public function openFormLogin()
    {
        $show = new RainTPL();
        $show->assign("version",$_SESSION['version']);
        $show->draw("login/login");
    }

    public function checkLogin()
    {

        if (!empty($_POST["user"]) && !empty($_POST["password"])) {

            //verifica credenziali

            $this->uac_model = new uacModel();
            $result  = $this->uac_model->checkUserLogin($_POST["user"],$_POST["password"]);

            if($result === false){
                $valid_login = "ERR_GENERIC";
            }else{
                $valid_login = "OK";
            }
            switch($valid_login){
                case "OK":
                    //valorizza $_SESSION["user"] con dati utente
                    $_SESSION["user"] = $result;
                    /*
                    $user_controller = new userSamanthaController();

                    $user_active = $user_controller->loadUser($_POST["email"]);

                    $user_controller->saveUserInSession($user_active);

                    unset($user_controller,$user_active);

                    $this->uac_model->setLoginActive();
                    */
                    $json_ret = array(
                        "esito" => "SI",
                        "url_go" => "home"
                    );
                    break;
                case "ERR_USER":
                    $json_ret = array(
                        "esito" => "NO",
                        "messaggio" => "Username non riconosciuto"
                    );
                    break;
                case "ERR_PSWD":
                    $json_ret = array(
                        "esito" => "NO",
                        "messaggio" => "Password errata"
                    );
                    break;
                case "ERR_GENERIC":
                    $json_ret = array(
                        "esito" => "NO",
                        "messaggio" => "Username o Password errati"
                    );
                    break;
                default:
                    $json_ret = array(
                        "esito" => "NO",
                        "messaggio" => "Errore autenticazione generico"
                    );
            }
        }
        else {
            $json_ret = array(
                "esito" => "NO",
                "messaggio" => "Informazioni mancanti"
            );

            //se campi vuoti carico dati guest
            /*$_SESSION["user"] = array( "id_user" => 2,"user_name"=>"leggo","user_psw" => "leggo","user_type"=>"guest");
            $json_ret = array(
                "esito" => "SI",
                "url_go" => "home"
            );*/
        }
        echo json_encode($json_ret);
    }



    public function setName($name)
    {
        $this->user_name = $name;
    }

    public function setType($type)
    {
        $this->user_type = $type;
    }

    public function getType()
    {
        return $this->user_type;
    }

    public function getName()
    {
        return $this->user_name;
    }

    public static function checkAdminUser(){
        $type_user = $_SESSION['user']['user_type'];
        if( $type_user == 'admin'){
            return true;
        }else{
            return false;
        }
    }

    public function logout(){
        $_SESSION['user'] = "";
        header('Location: login');
    }

}