<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/03/2016
 * Time: 23:42
 */
class uacModel
{
    public function checkUserLogin($username, $password)
    {
        $query = "SELECT * FROM user WHERE user_name = ? AND user_psw = ?";
        try{
            $cn = dbAppHandler::getCon();
            $c_userinfo = $cn->prepare($query);
            $c_userinfo->execute(array($username, $password));
            $r_userinfo = $c_userinfo->fetch(PDO::FETCH_ASSOC);
            $c_userinfo->closeCursor();
            unset($c_userinfo,$cn);
            if (is_array($r_userinfo) && count($r_userinfo) > 0) {
                return $r_userinfo;
            }
            else {
                return false;
            }
        } catch (PDOException $e){
            trigger_error($e->getMessage(),E_USER_ERROR);
        }
    }
}