<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/05/2016
 * Time: 12:56
 */
class adunanzaController
{


    public function adunanzaList(){
        //definisco tipo di utente
        $admin_user = uacController::checkAdminUser();

        $ad_model = new adunanzaModel();
        $adunanza_list = $ad_model->getAllAdunanza();
        foreach($adunanza_list as $key =>$one_ad){
            $adunanza_list[$key]['adunanza_evento'] = '';

            $giovedi = new DateTime($adunanza_list[$key]['adunanza_data']);
            $day_of_week = $giovedi->format('w');
            $step_day_domenica = 7 - $day_of_week;
            $step_day_lunedi = $day_of_week - 1 ;
            $domenica = clone $giovedi;
            $lunedi = clone $giovedi;
            $lunedi = $lunedi->modify( '-'.$step_day_lunedi.' day' )->format('d/m/Y');
            $domenica = $domenica->modify( '+'.$step_day_domenica.' day' )->format('d/m/Y');

            $adunanza_list[$key]['adunanza_data'] = $giovedi->format('d/m/Y');
            $adunanza_list[$key]['adunanza_settimana'] = $lunedi.' - ' .$domenica;

            if(isset($adunanza_list[$key]['adunanza_congresso'] )){
                $adunanza_list[$key]['adunanza_evento'] = labelHelper::getLabel($adunanza_list[$key]['adunanza_congresso']);
            }
            if($adunanza_list[$key]['adunanza_visita'] == 1){
                $adunanza_list[$key]['adunanza_evento'] .= ' Vizita supraveghetorul';
            }
        }

        $ad_view = new adunanzaView();
        $ad_view->assign("admin_mode", $admin_user);

        $app_update = new appView();
        $app_update->local_js = $ad_view::LISTA_JS;
        $app_update->titolo_pagina = $ad_view::TITOLO_LISTA;
        $app_update->html_wrapper = $ad_view->renderAdunanzaLIst($adunanza_list);
        $app_update->defaultView();

    }

    public function adunanzaFormHead(){
        {
            //definisco tipo di utente
            $admin_user = uacController::checkAdminUser();

            $ad_model = new adunanzaModel();
            $ad_view = new adunanzaView();

            if (isset($_GET['id'])) {
                $id_adunanza = $_GET['id'];
            }

            if (!isset($id_adunanza)) {
                //insert
                $ad_view->assign("update_mode", false);
                $ad_view->assign("admin_mode", $admin_user);

                $app_update = new appView();
                $app_update->local_js = $ad_view::FORM_HEAD_JS;
                $app_update->titolo_pagina = $ad_view::TITOLO_FORM_MODIFY;
                $app_update->html_wrapper = $ad_view->renderAdunanzaFormHead();
                $app_update->defaultView();

            }

        }
    }

    public function adunanzaFormContent(){

        $GLOBALS['nomi_in_uso'] = array();
        $ad_data = dateTimeHelper::slash2dash($_POST['ad_data']);
        if(isset($_POST['ad_visita'])){
            $ad_visita = $_POST['ad_visita'];
        }else{
            $ad_visita = 0;
        }
        if(isset($_POST['ad_cong']) && $_POST['ad_cong'] != ''){
            $ad_cong = $_POST['ad_cong'];
            $ad_cong_label = labelHelper::getLabel($_POST['ad_cong']);
        }else{
            $ad_cong = null;
            $ad_cong_label = null;
        }
        // recupero parsing
        $ad_model = new adunanzaModel();
        $parsing = $ad_model->parsPage($ad_data, $ad_visita);
        if(!$parsing){
            echo    '<div class="panel-body">
                      <div class="alert alert-danger"><strong>ATTENZIONE! CONNESSIONE INTERNET ASSENTE</strong><br>Non &egrave; possibile caricare i dati dal sito</div>
                    </div>';

        }else {

            // creazione pagina

            //  LEGENDA RUOLI
            // 1 	PRES 	Presidente
            // 2 	PREG 	Preghiera
            // 3 	OR  	Oratore
            // 4 	GS 	    Gemme spirituali
            // 5 	C_SB 	Conduttore studio biblico
            // 6 	L_SB 	Lettore studio biblico
            // 7 	S_LET 	Scuola - lettura
            // 8 	S_DIM 	Scuola - dimostrazione
            // 9 	S_ASS 	Scuola - assistente

            //gestione intro ed end

            $lettura = $parsing['lett_sett'];
            $cantico_1 = $parsing['cantico_1'];
            $cantico_2 = $parsing['cantico_2'];
            $cantico_3 = $parsing['cantico_3'];
            $presidente_nome = $this->getProclamatore4ruolo(1);
            $preghiera_1_nome = $this->getProclamatore4ruolo(2, 1);
            $preghiera_2_nome = $this->getProclamatore4Ruolo(2, 2);

            //gestione sezioni
            $page_sec_A = $parsing["sec_A"];
            $page_sec_B = $parsing["sec_B"];
            $page_sec_C = $parsing["sec_C"];


            $c = 0;
            foreach ($page_sec_A as $key => $info) {
                $page_sec_A[$key]['id_tema'] = 'sa_tema_' . $key;
                $page_sec_A[$key]['id_tempo'] = 'sa_tempo_' . $key;
                $page_sec_A[$key]['id_ruolo_1'] = 'sa_ruolo_1_' . $key;
                $page_sec_A[$key]['id_punto'] = 'sa_punto_' . $key;
                $page_sec_A[$key]['id_punto_ok'] = 'sa_punto_ok_' . $key;
                $page_sec_A[$key]['id_ruolo_2'] = 'sa_ruolo_2_' . $key;
                $page_sec_A[$key]['id_type_ruolo_1'] = 'sa_type_ruolo_1_' . $key;
                $page_sec_A[$key]['id_type_ruolo_2'] = 'sa_type_ruolo_2_' . $key;
                $page_sec_A[$key]['type_ruolo_1'] = $page_sec_A[$key]['ruolo_1'];
                $page_sec_A[$key]['type_ruolo_2'] = $page_sec_A[$key]['ruolo_2'];
                $page_sec_A[$key]['label_ruolo_1'] = $ad_model->getLabelByRuolo($page_sec_A[$key]['ruolo_1']);
                $page_sec_A[$key]['label_ruolo_2'] = $ad_model->getLabelByRuolo($page_sec_A[$key]['ruolo_2']);
                $page_sec_A[$key]['ruolo_1_nome'] = $this->getProclamatore4Ruolo($page_sec_A[$key]['ruolo_1']);
                if ($c == 2) {
                    $page_sec_A[$key]['punto'] = $ad_model->allPunti();
                } else {
                    $page_sec_A[$key]['punto'] = false;
                }
                $c++;
            }
            $c = 0;
            foreach ($page_sec_B as $key => $info) {
                $page_sec_B[$key]['id_tema'] = 'sb_tema_' . $key;
                $page_sec_B[$key]['id_tempo'] = 'sb_tempo_' . $key;
                $page_sec_B[$key]['id_ruolo_1'] = 'sb_ruolo_1_' . $key;
                $page_sec_B[$key]['id_punto'] = 'sb_punto_' . $key;
                $page_sec_B[$key]['id_punto_ok'] = 'sb_punto_ok_' . $key;
                $page_sec_B[$key]['id_ruolo_2'] = 'sb_ruolo_2_' . $key;
                $page_sec_B[$key]['id_type_ruolo_1'] = 'sb_type_ruolo_1_' . $key;
                $page_sec_B[$key]['id_type_ruolo_2'] = 'sb_type_ruolo_2_' . $key;
                $page_sec_B[$key]['type_ruolo_1'] = $page_sec_B[$key]['ruolo_1'];
                $page_sec_B[$key]['type_ruolo_2'] = $page_sec_B[$key]['ruolo_2'];
                $page_sec_B[$key]['label_ruolo_1'] = $ad_model->getLabelByRuolo($page_sec_B[$key]['ruolo_1']);
                $page_sec_B[$key]['label_ruolo_2'] = $ad_model->getLabelByRuolo($page_sec_B[$key]['ruolo_2']);
                $page_sec_B[$key]['ruolo_1_nome'] = $this->getProclamatore4Ruolo($page_sec_B[$key]['ruolo_1']);
                $page_sec_B[$key]['ruolo_2_nome'] = $this->getProclamatore4Ruolo($page_sec_B[$key]['ruolo_2']);
                if (count($page_sec_B) > 1) {
                    $page_sec_B[$key]['punto'] = $ad_model->allPunti();
                } else {
                    $page_sec_B[$key]['punto'] = false;
                }
            }

            $c = 0;
            foreach ($page_sec_C as $key => $info) {
                $page_sec_C[$key]['id_tema'] = 'sc_tema_' . $key;
                $page_sec_C[$key]['id_tempo'] = 'sc_tempo_' . $key;
                $page_sec_C[$key]['id_ruolo_1'] = 'sc_ruolo_1_' . $key;
                $page_sec_C[$key]['id_ruolo_2'] = 'sc_ruolo_2_' . $key;
                $page_sec_C[$key]['id_type_ruolo_1'] = 'sc_type_ruolo_1_' . $key;
                $page_sec_C[$key]['id_type_ruolo_2'] = 'sc_type_ruolo_2_' . $key;
                $page_sec_C[$key]['type_ruolo_1'] = $page_sec_C[$key]['ruolo_1'];
                $page_sec_C[$key]['type_ruolo_2'] = $page_sec_C[$key]['ruolo_2'];
                $page_sec_C[$key]['label_ruolo_1'] = $ad_model->getLabelByRuolo($page_sec_C[$key]['ruolo_1']);
                $page_sec_C[$key]['label_ruolo_2'] = $ad_model->getLabelByRuolo($page_sec_C[$key]['ruolo_2']);
                $page_sec_C[$key]['ruolo_1_nome'] = $this->getProclamatore4Ruolo($page_sec_C[$key]['ruolo_1']);
                $page_sec_C[$key]['ruolo_2_nome'] = $this->getProclamatore4Ruolo($page_sec_C[$key]['ruolo_2']);
            }

            unset($GLOBALS['nomi_in_uso']);

            $ad_view = new adunanzaView();
            $ad_view->assign("data", $ad_data);
            $ad_view->assign("visita", $ad_visita);
            $ad_view->assign("congresso", $ad_cong);
            $ad_view->assign("congresso_label", $ad_cong_label);
            $ad_view->assign("lettura", $lettura);
            $ad_view->assign("cantico_1", $cantico_1);
            $ad_view->assign("cantico_2", $cantico_2);
            $ad_view->assign("cantico_3", $cantico_3);

            $ad_view->assign("page_sec_A", $page_sec_A);
            $ad_view->assign("page_sec_B", $page_sec_B);
            $ad_view->assign("page_sec_C", $page_sec_C);

            $ad_view->assign("presidente_nome", $presidente_nome);
            $ad_view->assign("preghiera_1_nome", $preghiera_1_nome);
            $ad_view->assign("preghiera_2_nome", $preghiera_2_nome);

            echo $ad_view->renderAdunanzaFormContent();
        }
    }

    public function loadAdunanzaFromId(){
        $ad_settimana = null;
        if(isset($_GET['id'])){

            $admin_user = uacController::checkAdminUser();
            $id_adunanza =  $_GET['id'];
            $ad_model = new adunanzaModel();
            $adunanza = $ad_model->loadFromId($id_adunanza);

            // creazione pagina

            //  LEGENDA RUOLI
            // 1 	PRES 	Presidente
            // 2 	PREG 	Preghiera
            // 3 	OR  	Oratore
            // 4 	GS 	    Gemme spirituali
            // 5 	C_SB 	Conduttore studio biblico
            // 6 	L_SB 	Lettore studio biblico
            // 7 	S_LET 	Scuola - lettura
            // 8 	S_DIM 	Scuola - dimostrazione
            // 9 	S_ASS 	Scuola - assistente

            //gestione intro ed end
            $ad_data = dateTimeHelper::dash2slash($adunanza['ad_data']);
            $data_link_wol = $this->reverseData($ad_data);
            $ad_visita = $adunanza['visita'];
            $ad_cong = $adunanza['congresso'];
            $lettura = $adunanza['lett_sett'];
            $cantico_1 = $adunanza['cantico_1'];
            $cantico_2 = $adunanza['cantico_2'];
            $cantico_3 = $adunanza['cantico_3'];
            $presidente_id = $adunanza['pres'];
            $preghiera_1_id = $adunanza['preg_1'];
            $preghiera_2_id = $adunanza['preg_2'];
            $presidente_nome = $this->getProclamatoreOption(1,$presidente_id);
            $preghiera_1_nome = $this->getProclamatoreOption(2,$preghiera_1_id,1);
            $preghiera_2_nome = $this->getProclamatoreOption(2,$preghiera_2_id,2);

            //gestione sezioni
            $page_sec_A = $adunanza['sec_A'];
            $page_sec_B = $adunanza['sec_B'];
            $page_sec_C = $adunanza['sec_C'];

            $c = 0;
            foreach ($page_sec_A as $key => $info) {
                //creazione id
                $page_sec_A[$key]['id_tema'] = 'sa_tema_' . $key;
                $page_sec_A[$key]['id_tempo'] = 'sa_tempo_' . $key;
                $page_sec_A[$key]['id_ruolo_1'] = 'sa_ruolo_1_' . $key;
                $page_sec_A[$key]['id_punto'] = 'sa_punto_' . $key;
                $page_sec_A[$key]['id_punto_ok'] = 'sa_punto_ok_' . $key;
                $page_sec_A[$key]['id_ruolo_2'] = 'sa_ruolo_2_' . $key;
                $page_sec_A[$key]['id_type_ruolo_1'] = 'sa_type_ruolo_1_' . $key;
                $page_sec_A[$key]['id_type_ruolo_2'] = 'sa_type_ruolo_2_' . $key;
                $page_sec_A[$key]['id_storia'] = 'sa_storia_' . $key;
                $page_sec_A[$key]['id_lista_nota'] = 'sa_lista_nota_' . $key;

                //gestione valori
                $page_sec_A[$key]['punto_ok'] = $page_sec_A[$key]['punto_ok_1'];
                if(isset($page_sec_A[$key]['punto'])){
                    $punto =$page_sec_A[$key]['punto'];
                } else{
                    $punto = null;
                }

                if(isset($page_sec_A[$key]['ruolo_1'])){
                    $page_sec_A[$key]['type_ruolo_1'] = $page_sec_A[$key]['ruolo_1'];
                }else{
                    $page_sec_A[$key]['type_ruolo_1'] = null;
                }
                if(isset($page_sec_A[$key]['proclamatore_1'])) {
                    $proc_1_id = $page_sec_A[$key]['proclamatore_1'];
                }else{
                    $proc_1_id = NULL;
                }
                if(isset($page_sec_A[$key]['ruolo_1'])){
                    $page_sec_A[$key]['label_ruolo_1'] = $ad_model->getLabelByRuolo($page_sec_A[$key]['ruolo_1']);
                }else{
                    $page_sec_A[$key]['label_ruolo_1'] = NULL;
                }
                if(isset($page_sec_A[$key]['ruolo_2'])){
                    $page_sec_A[$key]['label_ruolo_2'] = $ad_model->getLabelByRuolo($page_sec_A[$key]['ruolo_2']);
                }else{
                    $page_sec_A[$key]['label_ruolo_2'] = NULL;
                }


                $page_sec_A[$key]['ruolo_1_nome'] = $this->getProclamatoreOption($page_sec_A[$key]['type_ruolo_1'],$proc_1_id);


                if ($c == 2) {
                    $array_punti = $this->getPunti4Proc($proc_1_id);
                    $page_sec_A[$key]['punto'] = $this->allPunti($punto,$array_punti);
                } else {
                    $page_sec_A[$key]['punto'] = false;
                }
                $c++;
            }
            $c = 0;
            foreach ($page_sec_B as $key => $info) {
                $page_sec_B[$key]['id_tema'] = 'sb_tema_' . $key;
                $page_sec_B[$key]['id_tempo'] = 'sb_tempo_' . $key;
                $page_sec_B[$key]['id_ruolo_1'] = 'sb_ruolo_1_' . $key;
                $page_sec_B[$key]['id_punto'] = 'sb_punto_' . $key;
                $page_sec_B[$key]['id_punto_ok'] = 'sb_punto_ok_' . $key;
                $page_sec_B[$key]['id_ruolo_2'] = 'sb_ruolo_2_' . $key;
                $page_sec_B[$key]['id_type_ruolo_1'] = 'sb_type_ruolo_1_' . $key;
                $page_sec_B[$key]['id_type_ruolo_2'] = 'sb_type_ruolo_2_' . $key;
                $page_sec_B[$key]['id_storia'] = 'sb_storia_' . $key;
                $page_sec_B[$key]['nota'] = 'sb_nota_' . $key;
                $page_sec_B[$key]['id_lista_nota'] = 'sb_lista_nota_' . $key;

                $page_sec_B[$key]['punto_ok'] = $page_sec_B[$key]['punto_ok_1'];
                if(isset($page_sec_B[$key]['punto'])){
                    $punto =$page_sec_B[$key]['punto'];
                } else{
                    $punto = null;
                }

                if(isset($page_sec_B[$key]['ruolo_1'])){
                    $page_sec_B[$key]['type_ruolo_1'] = $page_sec_B[$key]['ruolo_1'];
                }else{
                    $page_sec_B[$key]['type_ruolo_1'] = null;
                }
                if(isset($page_sec_B[$key]['ruolo_2'])){
                    $page_sec_B[$key]['type_ruolo_2'] = $page_sec_B[$key]['ruolo_2'];
                }else{
                    $page_sec_B[$key]['type_ruolo_2'] = null;
                }
                if(isset($page_sec_B[$key]['ruolo_1'])){
                    $page_sec_B[$key]['label_ruolo_1'] = $ad_model->getLabelByRuolo($page_sec_B[$key]['ruolo_1']);
                }else{
                    $page_sec_B[$key]['label_ruolo_1'] = null;
                }
                if(isset($page_sec_B[$key]['ruolo_2'])){
                    $page_sec_B[$key]['label_ruolo_2'] = $ad_model->getLabelByRuolo($page_sec_B[$key]['ruolo_2']);
                }else{
                    $page_sec_B[$key]['label_ruolo_2'] = null;
                }
                if(isset($page_sec_B[$key]['proclamatore_1'])) {
                    $proc_1_id = $page_sec_B[$key]['proclamatore_1'];
                }else{
                    $proc_1_id = NULL;
                }
                if(isset($page_sec_B[$key]['proclamatore_2'])) {
                    $proc_2_id = $page_sec_B[$key]['proclamatore_2'];
                }else{
                    $proc_2_id = NULL;
                }

                $page_sec_B[$key]['ruolo_1_nome'] = $this->getProclamatoreOption($page_sec_B[$key]['type_ruolo_1'],$proc_1_id);
                $page_sec_B[$key]['ruolo_2_nome'] = $this->getProclamatoreOption($page_sec_B[$key]['type_ruolo_2'],$proc_2_id);
                $array_punti = $this->getPunti4Proc($proc_1_id);
                if (count($page_sec_B) > 1) {
                    $page_sec_B[$key]['punto'] = $this->allPunti($punto,$array_punti);
                } else {
                    $page_sec_B[$key]['punto'] = false;
                }
            }
            $c = 0;
            foreach ($page_sec_C as $key => $info) {
                $page_sec_C[$key]['id_tema'] = 'sc_tema_' . $key;
                $page_sec_C[$key]['id_tempo'] = 'sc_tempo_' . $key;
                $page_sec_C[$key]['id_ruolo_1'] = 'sc_ruolo_1_' . $key;
                $page_sec_C[$key]['id_ruolo_2'] = 'sc_ruolo_2_' . $key;
                $page_sec_C[$key]['id_type_ruolo_1'] = 'sc_type_ruolo_1_' . $key;
                $page_sec_C[$key]['id_type_ruolo_2'] = 'sc_type_ruolo_2_' . $key;

                if(isset($page_sec_C[$key]['ruolo_1'])){
                    $page_sec_C[$key]['type_ruolo_1'] = $page_sec_C[$key]['ruolo_1'];
                }else{
                    $page_sec_C[$key]['type_ruolo_1'] = null;
                }
                if(isset($page_sec_C[$key]['ruolo_2'])){
                    $page_sec_C[$key]['type_ruolo_2'] = $page_sec_C[$key]['ruolo_2'];
                }else{
                    $page_sec_C[$key]['type_ruolo_2'] = null;
                }
                if(isset($page_sec_C[$key]['ruolo_1'])){
                    $page_sec_C[$key]['label_ruolo_1'] = $ad_model->getLabelByRuolo($page_sec_C[$key]['ruolo_1']);
                }else{
                    $page_sec_C[$key]['label_ruolo_1'] = null;
                }
                if(isset($page_sec_C[$key]['ruolo_2'])){
                    $page_sec_C[$key]['label_ruolo_2'] = $ad_model->getLabelByRuolo($page_sec_C[$key]['ruolo_2']);
                }else{
                    $page_sec_C[$key]['label_ruolo_2'] = null;
                }

                if(isset($page_sec_C[$key]['proclamatore_1'])) {
                    $proc_1_id = $page_sec_C[$key]['proclamatore_1'];
                }else{
                    $proc_1_id = NULL;
                }
                if(isset($page_sec_C[$key]['proclamatore_2'])) {
                    $proc_2_id = $page_sec_C[$key]['proclamatore_2'];
                }else{
                    $proc_2_id = NULL;
                }

                $page_sec_C[$key]['ruolo_1_nome'] = $this->getProclamatoreOption($page_sec_C[$key]['type_ruolo_1'],$proc_1_id);
                $page_sec_C[$key]['ruolo_2_nome'] = $this->getProclamatoreOption($page_sec_C[$key]['type_ruolo_2'] ,$proc_2_id);
            }
            unset($GLOBALS['nomi_in_uso']);

            $giovedi = new DateTime($adunanza['ad_data']);
            $day_of_week = $giovedi->format('w');
            $step_day_domenica = 7 - $day_of_week;
            $step_day_lunedi = $day_of_week - 1 ;
            $domenica = clone $giovedi;
            $lunedi = clone $giovedi;
            $lunedi = $lunedi->modify( '-'.$step_day_lunedi.' day' )->format('d/m/Y');
            $domenica = $domenica->modify( '+'.$step_day_domenica.' day' )->format('d/m/Y');
            $ad_settimana = 'da '. $lunedi.' a ' .$domenica;

            $ad_view = new adunanzaView();
            $ad_view->assign("adunanza_id", $id_adunanza);
            $ad_view->assign("data", $giovedi->format('d/m/Y'));
            $ad_view->assign("settimana", $ad_settimana);
            $ad_view->assign("data_link_wol", $data_link_wol);
            $ad_view->assign("visita", $ad_visita);
            $ad_view->assign("congresso", $ad_cong);
            $ad_view->assign("lettura", $lettura);
            $ad_view->assign("cantico_1", $cantico_1);
            $ad_view->assign("cantico_2", $cantico_2);
            $ad_view->assign("cantico_3", $cantico_3);

            $ad_view->assign("page_sec_A", $page_sec_A);
            $ad_view->assign("page_sec_B", $page_sec_B);
            $ad_view->assign("page_sec_C", $page_sec_C);

            $ad_view->assign("presidente_nome", $presidente_nome);
            $ad_view->assign("preghiera_1_nome", $preghiera_1_nome);
            $ad_view->assign("preghiera_2_nome", $preghiera_2_nome);

            $ad_view->assign("update_mode", false);
            $ad_view->assign("admin_mode", $admin_user);

            $app_update = new appView();
            $app_update->local_js = $ad_view::FORM_HEAD_JS;
            $app_update->titolo_pagina = $ad_view::TITOLO_FORM_MODIFY;

            $app_update->html_wrapper = $ad_view->renderAdunanzaFormModify();
            $app_update->defaultView();

        }else{
            echo 'error';
        }


    }

    public function adunanzaSave(){
        $ad_model = new adunanzaModel();

        $sec_A = array();
        $sec_B = array();
        $sec_C = array();

        $data = trim($_POST['data']);

        if(isset($_POST['visita'])){
            $visita = $_POST['visita'];
            $ad_model->setVisita($visita);
        }else{
            $visita = null;
        }

        if(isset($_POST['ad_cong'])){
            $ad_cong = $_POST['ad_cong'];
            $ad_model->setAdCong($ad_cong);
        }else{
            $ad_cong = null;
        }

        if(isset($_POST['adunanza_id'])){
            $data = dateTimeHelper::slash2dash($data);
            $ad_model->deleteAdunanzaById($_POST['adunanza_id']);
        }

        $date = new DateTime($data);
        $week = $date->format("W").'-'.$date->format("Y");

        $ad_model->setAdData($data);
        $ad_model->setWeek($week);
        $ad_model->setVisita($visita);


        if($ad_cong == null){
            $lett_sett = trim($_POST['lett_sett']);
            $cant_1 = trim($_POST['cant_1']);
            $cant_2 = trim($_POST['cant_2']);
            $cant_3 = trim($_POST['cant_3']);
            $preg_1 = trim($_POST['preg_1']);
            $preg_2 = trim($_POST['preg_2']);
            $pres = trim($_POST['pres']);

            $ad_model->setLettSett($lett_sett);
            $ad_model->setCant1($cant_1);
            $ad_model->setCant2($cant_2);
            $ad_model->setCant3($cant_3);
            $ad_model->setPreg1($preg_1);
            $ad_model->setPreg2($preg_2);
            $ad_model->setPres($pres);

            //sec A
            $key = 0;
            while (isset($_POST['sa_tema_' . $key])) {
                $tema = trim($_POST['sa_tema_' . $key]);
                $tempo = trim($_POST['sa_tempo_' . $key]);
                $type_ruolo_1 = trim($_POST['sa_type_ruolo_1_' . $key]);
                $ruolo_1 = trim($_POST['sa_ruolo_1_' . $key]);
                if (isset($_POST['sa_punto_' . $key]) && trim($_POST['sa_punto_' . $key]) != '') {
                    $punto = trim($_POST['sa_punto_' . $key]);
                    if (isset($_POST['sa_punto_ok_' . $key]) && $_POST['sa_punto_ok_' . $key]!= '') {
                        $punto_ok = 1;
                    } else {
                        $punto_ok = 0;
                    }
                } else {
                    $punto = NULL;
                    $punto_ok = NULL;
                }


                $one_parte = array(
                    'tema' => $tema,
                    'tempo' => $tempo,
                    'type_ruolo_1' => $type_ruolo_1,
                    'ruolo_1' => $ruolo_1,
                    'punto' => $punto,
                    'punto_ok' => $punto_ok
                );

                $sec_A[] = $one_parte;
                $key++;
            }
            $ad_model->setSecA($sec_A);

            //sec B
            $key = 0;
            while (isset($_POST['sb_tema_' . $key])) {
                $tema = trim($_POST['sb_tema_' . $key]);
                $tempo = trim($_POST['sb_tempo_' . $key]);
                $type_ruolo_1 = trim($_POST['sb_type_ruolo_1_' . $key]);

                if (isset($_POST['sb_ruolo_1_' . $key])) {
                    $ruolo_1 = trim($_POST['sb_ruolo_1_' . $key]);
                } else {
                    $ruolo_1 = null;
                }

                if (isset($_POST['sb_punto_' . $key]) && trim($_POST['sb_punto_' . $key]) != '') {
                    $punto = trim($_POST['sb_punto_' . $key]);
                    if (isset($_POST['sb_punto_ok_' . $key]) && $_POST['sb_punto_ok_' . $key]!= '') {
                        $punto_ok = 1;
                    } else {
                        $punto_ok = 0;
                    }
                } else {
                    $punto = NULL;
                    $punto_ok = NULL;
                }
                $one_parte = array(
                    'tema' => $tema,
                    'tempo' => $tempo,
                    'type_ruolo_1' => $type_ruolo_1,
                    'ruolo_1' => $ruolo_1,
                    'punto' => $punto,
                    'punto_ok' => $punto_ok
                );

                if (isset($_POST['sb_type_ruolo_2_' . $key])) {
                    $type_ruolo_2 = trim($_POST['sb_type_ruolo_2_' . $key]);
                    $one_parte['type_ruolo_2'] = $type_ruolo_2;
                }
                if (isset($_POST['sb_ruolo_2_' . $key])) {
                    $ruolo_2 = trim($_POST['sb_ruolo_2_' . $key]);
                    $one_parte['ruolo_2'] = $ruolo_2;
                }
                $sec_B[] = $one_parte;
                $key++;
            }
            $ad_model->setSecB($sec_B);

            //sec C
            $key = 0;
            while (isset($_POST['sc_tema_' . $key])) {
                $tema = trim($_POST['sc_tema_' . $key]);
                $tempo = trim($_POST['sc_tempo_' . $key]);
                $type_ruolo_1 = trim($_POST['sc_type_ruolo_1_' . $key]);
                //$ruolo_1 = trim($_POST['sc_ruolo_1_'.$key]);
                if (isset($_POST['sc_ruolo_1_' . $key])) {
                    $ruolo_1 = trim($_POST['sc_ruolo_1_' . $key]);
                    $one_parte['ruolo_1'] = $ruolo_1;
                }
                $one_parte = array(
                    'tema' => $tema,
                    'tempo' => $tempo,
                    'type_ruolo_1' => $type_ruolo_1,
                    'ruolo_1' => $ruolo_1
                );

                if (isset($_POST['sc_type_ruolo_2_' . $key])) {
                    $type_ruolo_2 = trim($_POST['sc_type_ruolo_2_' . $key]);
                    $one_parte['type_ruolo_2'] = $type_ruolo_2;
                }
                if (isset($_POST['sc_ruolo_2_' . $key])) {
                    $ruolo_2 = trim($_POST['sc_ruolo_2_' . $key]);
                    $one_parte['ruolo_2'] = $ruolo_2;
                }

                $sec_C[] = $one_parte;
                $key++;
            }
            $ad_model->setSecC($sec_C);
        }

        if ($ad_model->saveAdunanza()) {
            if(isset($_POST['adunanza_id'])){
                $response = array('esito' => "OK", 'mess' => "Adunanza modificata");
            }else{
                $response = array('esito' => "OK", 'mess' => "Adunanza salvata");
            }

        }else{
            $response = array('esito' => "OK", 'mess' => "Nessuna modifica salvata");
        }
        echo json_encode($response);

    }

    private function getProclamatore4Ruolo($id_ruolo, $posizione = null){
        $array_ruoli = array(
            "1" => "Presidente",
            "2" => "Preghiera",
            "3" => "Oratore",
            "4" => "Gemme spirituali",
            "5" => "Conduttore studio biblico",
            "6" => "Lettore studio biblico",
            "7" => "Scuola - lettura",
            "8" => "Scuola - dimostrazione",
            "9" => "Scuola - assistente"
        );
        if(isset($id_ruolo) && $id_ruolo != ''){
            $ad_model = new adunanzaModel();
            $proc_array = $ad_model->getProclamatore4Ruolo($id_ruolo,$posizione);
            if(in_array($proc_array[0],$GLOBALS['nomi_in_uso'])){
                $proc_array[] = $proc_array[0];
                $GLOBALS['nomi_in_uso'] = $proc_array[0];
                $proc_array[count($proc_array)] = $proc_array[0];
                unset($proc_array[0]);
            }else{
                $GLOBALS['nomi_in_uso'] = $proc_array[0];
            }
            foreach($proc_array as $key => $val){
                if(isset($proc_array[$key]['ruolo'])){
                    $proc_array[$key]['ruolo'] = $array_ruoli[$proc_array[$key]['ruolo']];
                }else{
                    $proc_array[$key]['ruolo'] ='';
                }
                if(isset($proc_array[$key]['adunanza_data']) && trim($proc_array[$key]['proclamatore_nome']) != '') {
                    $proc_array[$key]['adunanza_data'] = dateTimeHelper::dash2slash($proc_array[$key]['adunanza_data']);
                }else{
                    $proc_array[$key]['adunanza_data'] = '';
                }
            }
            return $proc_array;
        }else{
            return NULL;
        }
    }

    private function getProclamatoreOption($id_ruolo, $id_proc,$posizione = null){
        $array_ruoli = array(
            "1" => "Presidente",
            "2" => "Preghiera",
            "3" => "Oratore",
            "4" => "Gemme spirituali",
            "5" => "Conduttore studio biblico",
            "6" => "Lettore studio biblico",
            "7" => "Scuola - lettura",
            "8" => "Scuola - dimostrazione",
            "9" => "Scuola - assistente",
            "10" => "Sorvegliante"
        );
        if(isset($id_ruolo) && $id_ruolo != ''){
            $ad_model = new adunanzaModel();
            $proc_array = $ad_model->getProclamatore4Ruolo($id_ruolo,$posizione);
            foreach($proc_array as $key => $val){
                if($proc_array[$key]['proclamatore_id'] == $id_proc){
                    $proc_array[$key]['selected'] = true;
                }else{
                    $proc_array[$key]['selected'] = false;
                }
            }
            foreach($proc_array as $key => $val){
                if(isset($proc_array[$key]['ruolo'])){
                    $proc_array[$key]['ruolo'] = $array_ruoli[$proc_array[$key]['ruolo']];
                }else{
                    $proc_array[$key]['ruolo'] = '';
                }

                if(isset($proc_array[$key]['adunanza_data'])) {
                    $proc_array[$key]['adunanza_data'] = dateTimeHelper::dash2slash($proc_array[$key]['adunanza_data']);
                }else{
                    $proc_array[$key]['adunanza_data'] = '';
                }
            }

            return $proc_array;
        }else{
            return NULL;
        }
    }

    public function checkWeek(){
        $data = dateTimeHelper::slash2dash($_POST['ad_data']);
        $date = new DateTime($data);
        $week = $date->format("W").'-'.$date->format("Y");
        $ad_model = new adunanzaModel();
        $res = $ad_model->searchWeek($week);
        if(count($res) > 0){
            echo "Per questa settimana esiste gi&agrave; un'adunanza. Cambia settimana";
        }else{
            echo "true";
        }
    }

    public function deleteAdunanza(){
        $response = array('esito' => "KO", 'mess' => "");
        if(isset($_POST['id_adunanza'])){
            $id_adunanza = $_POST['id_adunanza'];
            $ad_model = new adunanzaModel();
            if($ad_model->deleteAdunanzaById($id_adunanza)){
                $response = array('esito' => "OK", 'mess' => "Adunanza cancellata");
            }else{
                $response = array('esito' => "KO", 'mess' => "Adunanza non presente");
            }
        }else{
            $response = array('esito' => "KO", 'mess' => "Errore nei parametri");
        }
        echo json_encode($response);
    }

    public function getPunti4Proc($id_proc){
        $ad_m = new adunanzaModel();
        $r = $ad_m->getPunti4Proc($id_proc);
        $array_punti = array();
        foreach($r as $key => $val){
            if(isset($r[$key]['punto']) && $r[$key]['punto'] != '')
            $array_punti[] = $r[$key]['punto'];
        }
        return $array_punti;

    }

    private function allPunti($punto,$array_punti){
        $mod = new adunanzaModel();
        $all_punti = $mod->allPunti($punto);
        return $all_punti;
    }

    private function reverseData($data){
        $array_data = explode("/",$data);
        $result = $array_data[2]."/".$array_data[1]."/".$array_data[0];
        return $result;
    }


}