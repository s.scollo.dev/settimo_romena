<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/05/2016
 * Time: 12:56
 */
class adunanzaModel
{
    protected $ad_data;
    protected $week;
    protected $visita;
    protected $ad_cong;
    protected $lett_sett;
    protected $pres;
    protected $preg_1;
    protected $preg_2;
    protected $cant_1;
    protected $cant_2;
    protected $cant_3;
    protected $sec_A = array();
    protected $sec_B = array();
    protected $sec_C = array();

    public function getAdData()
    {
        return $this->ad_data;
    }

    public function setAdData($ad_data)
    {
        $this->ad_data = $ad_data;
    }

    public function getWeek()
    {
        return $this->week;
    }

    public function setWeek($week)
    {
        $this->week = $week;
    }

    public function getVisita()
    {
        return $this->visita;
    }

    public function setVisita($visita)
    {
        $this->visita = $visita;
    }

    public function getAdCong()
    {
        return $this->ad_cong;
    }

    public function setAdCong($ad_cong)
    {
        $this->ad_cong = $ad_cong;
    }

    public function getLettSett()
    {
        return $this->lett_sett;
    }

    public function setLettSett($lett_sett)
    {
        $this->lett_sett = $lett_sett;
    }

    public function getPres()
    {
        return $this->pres;
    }

    public function setPres($pres)
    {
        $this->pres = $pres;
    }

    public function getPreg1()
    {
        return $this->preg_1;
    }

    public function setPreg1($preg_1)
    {
        $this->preg_1 = $preg_1;
    }

    public function getPreg2()
    {
        return $this->preg_2;
    }

    public function setPreg2($preg_2)
    {
        $this->preg_2 = $preg_2;
    }

    public function getCant1()
    {
        return $this->cant_1;
    }

    public function setCant1($cant_1)
    {
        $this->cant_1 = $cant_1;
    }

    public function getCant2()
    {
        return $this->cant_2;
    }

    public function setCant2($cant_2)
    {
        $this->cant_2 = $cant_2;
    }

    public function getCant3()
    {
        return $this->cant_3;
    }

    public function setCant3($cant_3)
    {
        $this->cant_3 = $cant_3;
    }

    public function getSecA()
    {
        return $this->sec_A;
    }

    public function setSecA($sec_A)
    {
        $this->sec_A = $sec_A;
    }

    public function getSecB()
    {
        return $this->sec_B;
    }

    public function setSecB($sec_B)
    {
        $this->sec_B = $sec_B;
    }

    public function getSecC()
    {
        return $this->sec_C;
    }

    public function setSecC($sec_C)
    {
        $this->sec_C = $sec_C;
    }

    public function getAllAdunanza(){
        try {
            $cn = dbAppHandler::getCon();
            $query = "SELECT * FROM adunanza order by adunanza_data desc";
            $obj= $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function parsPage($ad_data,$visita){
        header('Content-Type: text/html; charset=utf-8');
        error_reporting(1);
        $all_result = array();
        $ad_data = str_replace('-','/',$ad_data);
        $ad_data_show = dateTimeHelper::dash2slash($ad_data);
        $sc = 0;
        $all_result['ad_data'] = $ad_data_show;
        //estrazione tutta pagina
        $html = file_get_contents("http://wol.jw.org/ro/wol/dt/r34/lp-m/".$ad_data);

        if(isset($html) && $html != '') {


            //prima pulizia
            $html = trim(str_replace("<strong>: </strong>", "", $html));
            $html = trim(str_replace("<strong></strong>", "", $html));
            $html = trim(str_replace("�", "", $html));
            $html = trim(str_replace("�", "", $html));
            $html = trim(str_replace("<em>", "", $html));
            $html = trim(str_replace("</em>", "", $html));


            //ESTRAZIONE CANTICI
            $cantici = Array();
            preg_match_all("(<a(.*?)</a>)", $html, $res);
            $c = 1;
            for ($i = 0; $i < 1; $i++) {
                for ($x = 0; $x < count($res[$i]); $x++) {
                    $row = trim($res[$i][$x]);
                    $row = trim(str_replace(":", "", $row));
                    $row = trim(str_replace("<strong>", "", $row));
                    $row = trim(str_replace("</strong>", "", $row));
                    if (strstr($row, 'Cântarea')) {
                        $row = strip_tags($row);
                        $cantici[] = $row;
                        $all_result['cantico_' . $c] = $row;
                        $c++;
                    }
                }
            }

            //ESTRAZIONE MINUTI
            preg_match_all("(\((.*?)min.\))", $html, $res);
            $minuti = Array();
            for ($i = 0; $i < 1; $i++) {
                for ($x = 0; $x < count($res[$i]); $x++) {
                    $row = $res[$i][$x];
                    $row = trim(str_replace(":", "", $row));
                    if (strlen($row) > 3) {
                        $minuti[$x] = strip_tags($row);
                    }
                }
            }

            unset($minuti[count($minuti) - 1]); //cancello minuti parole finali
            unset($minuti[0]);// cancello minuti parole intro

            //ESTRAZIONE TITOLI di parti e sessioni
            $temi = array();
            preg_match_all("(<strong>(.*?)</strong>)", $html, $res);
            $array_row = $res[1];
            $i = 1;
            foreach ($array_row as $row) {
                $row = trim(str_replace(":", "", $row));
                $row = trim(str_replace("<strong>", "", $row));
                $row = trim(str_replace("</strong>", "", $row));
                if (strlen(trim($row)) > 3) {
                    $temi[] = $row;
                }
            }


            unset($temi[count($temi) - 1]); //cancello settimana
            unset($temi[count($temi) - 1]); //cancello torre di guardia
            unset($temi[0]); //cancello settimana

            //costruisco array in uscita -> all_result
            $all_result['lett_sett'] = $temi[1];
            $k = 1; //indice array minuti
            /*
              LEGENDA RUOLI
                    1 	PRES 	Presidente
                    2 	PREG 	Preghiera
                    3 	OR 	Oratore
                    4 	GS 	Gemme spirituali
                    5 	C_SB 	Conduttore studio biblico
                    6 	L_SB 	Lettore studio biblico
                    7 	S_LET 	Scuola - lettura
                    8 	S_DIM 	Scuola - dimostrazione
                    9 	S_ASS 	Scuola - assistente

             */
            $c_sec_A = 0;
            $c_sec_B = 0;
            $c_sec_C = 0;
            for ($w = 2; $w <= count($temi); $w++) {
                $ruolo_1 = 3; //oratore
                $ruolo_2 = NULL;
                $row = trim($temi[$w]);
                if ($row != 'COMORI DIN CUVÂNTUL LUI DUMNEZEU' && $row != 'SĂ FIM MAI EFICIENȚI ÎN PREDICARE' && $row != 'VIAȚA DE CREȘTIN') {
                    if (is_array($all_result['sec_C'])) {
                        if ($row == 'Studiul Bibliei în congregație') {
                            $ruolo_1 = 5;
                            $ruolo_2 = 6;
                        }
                        $all_result['sec_C'][] = array('tema' => $row, 'tempo' => $minuti[$k], 'ruolo_1' => $ruolo_1, 'ruolo_2' => $ruolo_2);
                        $k++;
                        $c_sec_C++;
                    } elseif (is_array($all_result['sec_B'])) {
                        $all_result['sec_B'][] = array('tema' => $row, 'tempo' => $minuti[$k], 'ruolo_1' => $ruolo_1, 'ruolo_2' => $ruolo_2);
                        $k++;
                        $c_sec_B++;
                    } elseif (is_array($all_result['sec_A'])) {
                        if ($c_sec_A == 1) {
                            $ruolo_1 = 3;
                        } elseif ($c_sec_A == 2) {
                            $ruolo_1 = 7;
                        }
                        $all_result['sec_A'][] = array('tema' => $row, 'tempo' => $minuti[$k], 'ruolo_1' => $ruolo_1, 'ruolo_2' => $ruolo_2);

                        $k++;
                        $c_sec_A++;
                    }
                } elseif ($row == 'COMORI DIN CUVÂNTUL LUI DUMNEZEU') {
                    $all_result['sec_A'] = Array();
                } elseif ($row == 'SĂ FIM MAI EFICIENȚI ÎN PREDICARE') {
                    $all_result['sec_B'] = Array();
                } elseif ($row == 'VIAȚA DE CREȘTIN') {
                    $all_result['sec_C'] = Array();
                }
            }


            //gestione dettagli

			/*
            if (count($all_result['sec_B']) == 3) {
                foreach ($all_result['sec_B'] as $key => $one) {
                    $all_result['sec_B'][$key]['ruolo_1'] = 8;
                    $all_result['sec_B'][$key]['ruolo_2'] = 9;
                }
            }
			*/
			//23/11/2018 allineamento ruoli x SĂ FIM MAI EFICIENȚI ÎN PREDICARE

            foreach ($all_result['sec_B'] as $key => $one) {
                if(
                    ($one['tema'] == 'Vizita inițială') ||
                    ($one['tema'] == 'Prima vizită ulterioară') ||
                    ($one['tema'] == 'A doua vizită ulterioară') ||
                    ($one['tema'] == 'A treia vizită ulterioară') ||
                    ($one['tema'] == 'Studiu biblic')
                )
                {
                    $all_result['sec_B'][$key]['ruolo_1'] = 8;
                    $all_result['sec_B'][$key]['ruolo_2'] = 9;
                }elseif($one['tema'] == 'Cuvântare'){
                    $all_result['sec_B'][$key]['ruolo_1'] = 11;
                    $all_result['sec_B'][$key]['ruolo_2'] = null;
                }elseif($one['tema'] == 'Dedică-te citirii și predării'){
                    $all_result['sec_B'][$key]['ruolo_1'] = 3;
                    $all_result['sec_B'][$key]['ruolo_2'] = null;
                }elseif(strpos($one['tema'],"Material video") != FALSE){
                    $all_result['sec_B'][$key]['ruolo_1'] = 3;
                    $all_result['sec_B'][$key]['ruolo_2'] = null;
                }
            }
			
            if ($visita == 1) {
                $all_result['sec_C'][(count($all_result['sec_C']) - 1)] = array('tema' => 'Discorso del sorvegliante', 'tempo' => '(30 min.)', 'ruolo_1' => 10, 'ruolo_2' => null);
            }

            return $all_result;
        }else{
            return false;
        }
    }

    public function loadFromId($adunanza_id){
        $result = array();
        $result['adunanza_id'] = $adunanza_id;
        $result['sec_A'] = array();
        $result['sec_B'] = array();
        $result['sec_C'] = array();

        $query_adun = "SELECT
                        adunanza_data AS ad_data,
                        adunanza_cantico_1 AS cantico_1,
                        adunanza_cantico_2 AS cantico_2,
                        adunanza_cantico_3 AS cantico_3,
                        adunanza_lettura_sett AS lett_sett,
                        adunanza_visita AS visita,
                        adunanza_congresso AS congresso,
                        adunanza_pres AS pres,
                        adunanza_preg_1 AS preg_1,
                        adunanza_preg_2 AS preg_2
                        FROM adunanza
                        WHERE adunanza.adunanza_id  = $adunanza_id ";
        try {
            $cn = dbAppHandler::getCon();
            $obj= $cn->prepare($query_adun);
            $obj->execute();
            $all_1 = $obj->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }

        foreach($all_1 as $key => $val){
            $result[$key] = $all_1[$key];
        }
        
        $query_parte = "SELECT
                            parte_id ,
                            parte_tema AS tema,
                            parte_tempo AS tempo,
                            parte_id_adunanza AS adunanza_id,
                            parte_id_sezione AS sezione_id,
                            parte_order FROM parte
                            WHERE parte.parte_id_adunanza = $adunanza_id
                            ORDER BY parte_id, parte_order";
        try {
            $cn = dbAppHandler::getCon();
            $obj= $cn->prepare($query_parte);
            $obj->execute();
            $all_2 = $obj->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }

        foreach($all_2 as $key => $val){
            $temp = array();
            $temp['tema'] = $all_2[$key]['tema'];
            $temp['tempo'] = $all_2[$key]['tempo'];
            $id_adunanza = $all_2[$key]['adunanza_id'];
            $id_sezione = $all_2[$key]['sezione_id'];
            $id_parte = $all_2[$key]['parte_id'];

            $PR = $this->getPR4parte($id_adunanza,$id_sezione,$id_parte);

            $c = 1;
            foreach($PR as $key => $val){
                if(isset($PR[$key]['ap_id_punto'])){
                    $temp['punto'] = $PR[$key]['ap_id_punto'];
                }
                $temp['punto_ok_'.$c] = $PR[$key]['ap_punto_ok'];
                $temp['ruolo_'.$c] = $PR[$key]['ap_id_ruolo'];
                $temp['proclamatore_'.$c] = $PR[$key]['ap_id_proclamatore'];
                $c++;
            }


            if($id_sezione == 1){
                $result['sec_A'][] = $temp;
            }
            if($id_sezione == 2){
                $result['sec_B'][] = $temp;
            }
            if($id_sezione == 3){
                $result['sec_C'][] = $temp;
            }


        }
    return $result;
    }

    public function getProclamatore4ruolo($ruolo,$posizione = null){
        if($ruolo == 1){
            try {
                $cn = dbAppHandler::getCon();
                $query = "SELECT
                            proclamatore_id,
                            proclamatore_nome,
                            proclamatore_cognome,
                            max(adunanza.adunanza_data) AS adunanza_data
                            FROM proclamatori
                            INNER JOIN ruoli_proclamatori ON ruoli_proclamatori.rp_proclamatore_id_ref=proclamatori.proclamatore_id
                            LEFT JOIN adunanza ON adunanza.adunanza_pres=ruoli_proclamatori.rp_proclamatore_id_ref
                            WHERE ruoli_proclamatori.rp_ruolo_id_ref=$ruolo
                            GROUP BY proclamatore_id, proclamatore_nome, proclamatore_cognome
                            ORDER BY max(adunanza.adunanza_data) ASC, proclamatore_nome, proclamatore_cognome";
                $obj= $cn->prepare($query);
                $obj->execute();
                $result = $obj->fetchAll(PDO::FETCH_ASSOC);

            } catch (PDOException $e) {
                trigger_error($e->getMessage(), E_USER_ERROR);
            }
        }elseif($ruolo == 2){
            $result_a = array();
            $result_b = array();
            try {
                $cn = dbAppHandler::getCon();
                $query = "SELECT
                            proclamatore_id,
                            proclamatore_nome,
                            proclamatore_cognome,
                            max(adunanza.adunanza_data) AS adunanza_data
                            FROM proclamatori
                            INNER JOIN ruoli_proclamatori ON ruoli_proclamatori.rp_proclamatore_id_ref=proclamatori.proclamatore_id
                            LEFT JOIN adunanza ON adunanza.adunanza_preg_$posizione=ruoli_proclamatori.rp_proclamatore_id_ref
                            WHERE ruoli_proclamatori.rp_ruolo_id_ref=$ruolo
                            GROUP BY proclamatore_id, proclamatore_nome, proclamatore_cognome
                            ORDER BY max(adunanza.adunanza_data) ASC, proclamatore_nome, proclamatore_cognome";
                $obj= $cn->prepare($query);
                $obj->execute();
                $result = $obj->fetchAll(PDO::FETCH_ASSOC);

            } catch (PDOException $e) {
                trigger_error($e->getMessage(), E_USER_ERROR);
            }

        }else{
            try {
                $cn = dbAppHandler::getCon();
                $query = "SELECT proclamatori.proclamatore_id as proclamatore_id,
                                proclamatori.proclamatore_nome as proclamatore_nome,
                                proclamatori.proclamatore_cognome as proclamatore_cognome,
                                MAX(adunanza.adunanza_data) as adunanza_data
                                FROM proclamatori
                                INNER JOIN ruoli_proclamatori ON proclamatori.proclamatore_id=ruoli_proclamatori.rp_proclamatore_id_ref
                                LEFT JOIN adunanza_proclamatore ON adunanza_proclamatore.ap_id_proclamatore=proclamatori.proclamatore_id
                                LEFT JOIN adunanza ON adunanza.adunanza_id = adunanza_proclamatore.ap_id_adunanza
                                WHERE ruoli_proclamatori.rp_ruolo_id_ref = $ruolo
                                GROUP BY proclamatori.proclamatore_id
                                ORDER BY max(adunanza.adunanza_data) asc";

                $obj= $cn->prepare($query);
                $obj->execute();
                $result = $obj->fetchAll(PDO::FETCH_ASSOC);
                foreach($result as $key => $val){
                    $result[$key]['ruolo'] = null;
                    $result[$key]['punto'] = null;
                    $proclamatore_id = $result[$key]['proclamatore_id'];
                    $adunanza_data = $result[$key]['adunanza_data'];
                    $info_proc =  $this->getRuoloPuntoByProcIdAdunData($proclamatore_id,$adunanza_data);
                    foreach($info_proc as $one_info){
                        $result[$key]['ruolo'] = $one_info['ruolo'];
                        $result[$key]['punto'] = $one_info['punto'];
                    }

                }
            } catch (PDOException $e) {
                trigger_error($e->getMessage(), E_USER_ERROR);
            }
        }
        return $result;

    }

    private function getRuoloPuntoByProcIdAdunData($proclamatore_id,$adunanza_data){
        try {
            $cn = dbAppHandler::getCon();
            $query = "SELECT ap_id_ruolo as ruolo, ap_id_punto as punto FROM adunanza_proclamatore
                        INNER JOIN adunanza ON adunanza.adunanza_id = adunanza_proclamatore.ap_id_adunanza
                        WHERE ap_id_proclamatore = $proclamatore_id AND adunanza.adunanza_data = '".$adunanza_data."'";
            $obj= $cn->prepare($query);
            $obj->execute();
            $res = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $res;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }
    public function getAllProclamatori(){
        try {
            $cn = dbAppHandler::getCon();
            $query = "SELECT * FROM proclamatori";
            $obj= $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function getLabelByRuolo($ruolo){
        try {
            $cn = dbAppHandler::getCon();
            $query = "SELECT ruolo_desc FROM ruoli WHERE ruolo_id = $ruolo";
            $obj= $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetch();
            return $result[0];
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function saveAdunanza(){

        $q_insert_adunanza = 'INSERT INTO adunanza
                                (adunanza_data, adunanza_settimana, adunanza_cantico_1, adunanza_cantico_2, adunanza_cantico_3, adunanza_lettura_sett, adunanza_visita, adunanza_pres, adunanza_preg_1, adunanza_preg_2, adunanza_congresso)
                              VALUES
                                ("'.$this->ad_data.'", "'.$this->week.'", "'.$this->cant_1.'", "'.$this->cant_2.'", "'.$this->cant_3.'", "'.$this->lett_sett.'", "'.$this->visita.'", "'.$this->pres.'", "'.$this->preg_1.'", "'.$this->preg_2.'", "'.$this->ad_cong.'")';

        $adunanza_id = 0;
        try {
            $cn = dbAppHandler::getCon();
            $cn->beginTransaction();

            //inserimento adunanza
            $insert_adunanza = $cn->prepare($q_insert_adunanza);
            $insert_adunanza->execute();
            $adunanza_id = $cn->lastInsertId();

            //SEC A
            $id_sec = 1;
            $cp = 0;
            foreach($this->sec_A AS $key => $info){
                $q_insert_parte = 'INSERT INTO parte(parte_tema, parte_tempo, parte_id_adunanza, parte_id_sezione, parte_order)
                                    VALUES ("'.$this->sec_A[$key]['tema'].'", "'.$this->sec_A[$key]['tempo'].'", "'.$adunanza_id.'", "'.$id_sec.'", "'.$cp.'")';
                $insert_parte = $cn->prepare($q_insert_parte);
                $insert_parte->execute();
                $parte_id = $cn->lastInsertId();

                $cr = 1;
                while(isset($this->sec_A[$key]['type_ruolo_'.$cr])){
					/*OLD
                    if($this->sec_A[$key]['type_ruolo_'.$cr] != 7){
                        $punto = 'NULL';
                        $punto_ok = 'NULL';
                    }else{
                        $punto = $this->sec_A[$key]['punto'];
                        $punto_ok = $this->sec_A[$key]['punto_ok'];
                    }
					*/
					//correzione per rimozione punto, altrimneti non salva il proc della letytura
					 $punto = 'NULL';
                     $punto_ok = 'NULL';
                    $q_insert_ruolo = 'INSERT INTO adunanza_proclamatore(ap_id_parte, ap_id_proclamatore, ap_id_ruolo, ap_id_punto, ap_punto_ok, ap_id_adunanza, ap_id_sezione)
                                        VALUES ('.$parte_id.', '.$this->sec_A[$key]['ruolo_'.$cr].', '.$this->sec_A[$key]['type_ruolo_'.$cr].', '.$punto.',  '.$punto_ok.','.$adunanza_id.', '.$id_sec.')';
                    $insert_ruolo = $cn->prepare($q_insert_ruolo);
                    $insert_ruolo->execute();
                    $cr++;
                }
                $cp++;
            }

            //SEC B
            $id_sec = 2;
            $cp = 0;
            foreach($this->sec_B AS $key => $info){
                $q_insert_parte = 'INSERT INTO parte(parte_tema, parte_tempo, parte_id_adunanza, parte_id_sezione, parte_order)
                                    VALUES ("'.$this->sec_B[$key]['tema'].'", "'.$this->sec_B[$key]['tempo'].'", '.$adunanza_id.', '.$id_sec.', '.$cp.')';
                $insert_parte = $cn->prepare($q_insert_parte);
                $insert_parte->execute();
                $parte_id = $cn->lastInsertId();

                $cr = 1;
                while(isset($this->sec_B[$key]['type_ruolo_'.$cr])){
					/*old con punti
                    if(isset($this->sec_B[$key]['punto']) && $this->sec_B[$key]['punto'] != ''){
                        $punto = $this->sec_B[$key]['punto'];
                        $punto_ok = $this->sec_B[$key]['punto_ok'];
                    }else{
                        $punto = 'NULL';
                        $punto_ok = 'NULL';
                    }
					*/
					$punto = 'NULL';
                    $punto_ok = 'NULL';
                    if($this->sec_B[$key]['type_ruolo_'.$cr] == 9){
                        $punto = 'NULL';
                        $punto_ok = 'NULL';
                    }

                    $q_insert_ruolo = 'INSERT INTO adunanza_proclamatore(ap_id_parte, ap_id_proclamatore, ap_id_ruolo, ap_id_punto, ap_punto_ok, ap_id_adunanza, ap_id_sezione)
                                        VALUES ('.$parte_id.', '.$this->sec_B[$key]['ruolo_'.$cr].','.$this->sec_B[$key]['type_ruolo_'.$cr].', '.$punto.',  '.$punto_ok.', '.$adunanza_id.', '.$id_sec.')';
                    $insert_ruolo = $cn->prepare($q_insert_ruolo);
                    $insert_ruolo->execute();
                    $cr++;
                }
                $cp++;
            }

            //SEC C
            $id_sec = 3;
            $cp = 0;
            foreach($this->sec_C AS $key => $info){
                if(isset($this->sec_C[$key]['tema']) && trim($this->sec_C[$key]['tema']) != ''){
                    $q_insert_parte = 'INSERT INTO parte(parte_tema, parte_tempo, parte_id_adunanza, parte_id_sezione, parte_order)
                                    VALUES ("'.$this->sec_C[$key]['tema'].'", "'.$this->sec_C[$key]['tempo'].'", "'.$adunanza_id.'", "'.$id_sec.'", "'.$cp.'")';
                    $insert_parte = $cn->prepare($q_insert_parte);
                    $insert_parte->execute();
                    $parte_id = $cn->lastInsertId();

                    $cr = 1;
                    while(isset($this->sec_C[$key]['type_ruolo_'.$cr])){
                        $q_insert_ruolo = 'INSERT INTO adunanza_proclamatore(ap_id_parte, ap_id_proclamatore, ap_id_ruolo, ap_id_punto, ap_punto_ok, ap_id_adunanza, ap_id_sezione)
                                        VALUES ("'.$parte_id.'", "'.$this->sec_C[$key]['ruolo_'.$cr].'", "'.$this->sec_C[$key]['type_ruolo_'.$cr].'", NULL, NULL,"'.$adunanza_id.'", "'.$id_sec.'")';
                        $insert_ruolo = $cn->prepare($q_insert_ruolo);
                        $insert_ruolo->execute();
                        $cr++;
                    }
                    $cp++;
                }
            }

            $cn->commit();
            return true;
        } catch (Exception $e) {
            $cn->rollBack();
            trigger_error("Error" . $e->getMessage());
            return false;
        }
    }

    public function deleteAdunanzaById($id_adunanza){
        try {
            $cn = dbAppHandler::getCon();
            $cn->beginTransaction();

            //delete  in adunanza
            $query = "DELETE FROM adunanza WHERE adunanza_id = $id_adunanza";
            $delete_ad = $cn->prepare($query);
            $delete_ad->execute();

            // delete in adunanza_proclamatore
            $query = "DELETE FROM adunanza_proclamatore WHERE ap_id_adunanza = $id_adunanza";
            $dalete_ad_pr = $cn->prepare($query);
            $dalete_ad_pr->execute();

            // delete in parte
            $query = "DELETE FROM parte WHERE parte_id_adunanza = $id_adunanza";
            $dalete_ad_pa = $cn->prepare($query);
            $dalete_ad_pa->execute();

            $cn->commit();
            return true;
        } catch (Exception $e) {
            $cn->rollBack();
            trigger_error("Error" . $e->getMessage());
            return false;
        }
    }

    public function deleteAdunanzaByWeek($week){
        try {
            $cn = dbAppHandler::getCon();
            $cn->beginTransaction();
            $query = "DELETE FROM adunanza WHERE adunanza_settimana = $week";
            $delete_ad = $cn->prepare($query);
            $delete_ad->execute();

            $cn->commit();
            return true;
        } catch (Exception $e) {
            $cn->rollBack();
            trigger_error("Error" . $e->getMessage());
            return false;
        }
    }

    public function searchWeek($week){
        try {
            $cn = dbAppHandler::getCon();
            $query = "SELECT * FROM adunanza WHERE adunanza_settimana = '$week'";
            $obj= $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function allPunti($punto = null){
        try {
            $cn = dbAppHandler::getCon();
            $query = "SELECT * FROM punti";
            $obj= $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            if(isset($punto)){
                foreach($result as $key => $val ){
                    if($result[$key]['punto_id'] == $punto){
                        $result[$key]['selected'] = true;
                    }else{
                        $result[$key]['selected'] = false;
                    }
                }
            }
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function getPR4parte($id_adunanza,$id_sezione,$id_parte){
        try {
            $cn = dbAppHandler::getCon();
            $query = "SELECT
                            ap_id_parte,
                            ap_id_proclamatore,
                            ap_id_ruolo,
                            ap_id_punto,
                            ap_punto_ok,
                            ap_id_adunanza,
                            ap_id_sezione
                        FROM adunanza_proclamatore
                        WHERE ap_id_adunanza = $id_adunanza
                        AND ap_id_sezione = $id_sezione
                        AND ap_id_parte = $id_parte
                        ORDER BY ap_id_ruolo";
            $obj= $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function getPunti4Proc($id_proc){
        $query = "SELECT ap_id_punto AS punto FROM adunanza_proclamatore WHERE ap_id_proclamatore = $id_proc  GROUP BY punto ORDER BY ap_id_punto ASC";
        try {
            $cn = dbAppHandler::getCon();
            $obj= $cn->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

}
