<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/05/2016
 * Time: 12:56
 */
class adunanzaView extends RainTPL
{
    const LISTA_JS = "../adunanza/adunanza_list.js";
    const FORM_HEAD_JS = "../adunanza/adunanza_form.js";
    const FORM_INSERT_JS = "../adunanza/adunanza_form_insert.js";
    const FORM_UPDATE_JS = "../adunanza/adunanza_form_update.js";
    const TITOLO_LISTA = "Gestione adunanza";
    const TITOLO_FORM_INSERT = "Inserimento adunanza";
    const TITOLO_FORM_MODIFY = "Modifica adunanza";
    const URL_CREATE_PROGRAMMA_ADUNANZA = "form-content-adunanza";
    const URL_SALVA_ADUNANZA = "salva-adunanza";
    const URL_LISTA_ADUNANZA = "adunanza";
    const URL_VERIFY_ADUNANZA = "";

    public function renderAdunanzaFormHead()
    {
        $this->assign("action","Inserimento nuova ");
        $this->assign("url_create_programma_adunanza", self::URL_CREATE_PROGRAMMA_ADUNANZA);
        return $this->draw("adunanza/adunanza_form",true);
    }

    public function renderAdunanzaFormContent()
    {
        $this->assign("url_salva_adunanza", self::URL_SALVA_ADUNANZA);
        return $this->draw("adunanza/adunanza_form_content",true);
    }


    public function renderAdunanzaFormModify()
    {
        $this->assign("url_salva_adunanza", self::URL_SALVA_ADUNANZA);
        return $this->draw("adunanza/adunanza_modify",true);
    }

    public function renderAdunanzaList($var){
        $this->assign("adunanza_list",$var);
        $this->assign("modify_msg","Vuoi cancellare adunanza?");
        $this->assign("cancella_adunanza","cancella-adunanza_");

        return $this->draw("adunanza/adunanza_list",true);
    }


}