<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 28/03/2016
 * Time: 08:29
 */
class appView extends RainTPL
{
    public $html_wrapper;
    public $local_js;
    public $titolo_pagina;

    public function defaultView()
    {
        if (!is_null($this->html_wrapper)) {
            $this->assign("wrapper", $this->html_wrapper);
        } else {
            $this->assign("wrapper", "");
        }
        $this->assign("local_js",$this->local_js);
        $this->assign("title_page",$this->titolo_pagina);
        $this->assign("version",$_SESSION['version']);
        $this->draw("main/mainApp");
    }

    public function renderHtml()
    {
        if (!empty($this->debug_shortcut)) {
            $this->assign("debug_shortcut",$this->debug_shortcut);
        }
        $this->assign("title_page",$this->title_page);
        $this->assignCSS();
        $this->assignJS();
        $this->draw("1_newtemplate/index");
    }

}