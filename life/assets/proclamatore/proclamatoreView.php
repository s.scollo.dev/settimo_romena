<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/03/2016
 * Time: 12:46
 */
class proclamatoreView extends RainTPL
{
    const LISTA_JS = "../proclamatore/proclamatore_list.js";
    const TITOLO_LISTA = "Gestione proclamatori";
    const TITOLO_FORM_INSERT = "Inserimento proclamatore";
    const TITOLO_FORM_MODIFY = "Modifica proclamatore";

    const FORM_INSERT_JS = "../proclamatore/proclamatore_form_insert.js";
    const FORM_UPDATE_JS = "../proclamatore/proclamatore_form_update.js";
    const URL_SALVA_PROCLAMATORE = "salva-proclamatore";
    const URL_LISTA_PROCLAMATORE = "proclamatori";
    const URL_VERIFY_PROCLAMATORE = "";

    public function renderProclamatoreList($var)
    {
        $this->assign("proclamatore_list",$var);
        $this->assign("modify_msg","Vuoi cancellare questo proclamatore");
        $this->assign("cancella_proclamatore","cancella-proclamatore_");

        return $this->draw("proclamatore/proclamatore_list",true);
    }

    public function renderProclamatoreFormInsert()
    {
        $this->assign("action","Inserimento nuovo ");
        $this->assign("url_insert_proclamatore", self::URL_SALVA_PROCLAMATORE);
        $this->assign("url_verify_proclamatore", self::URL_VERIFY_PROCLAMATORE);
        $this->assign("url_list_proclamatore", self::URL_LISTA_PROCLAMATORE);
        return $this->draw("proclamatore/proclamatore_form",true);
    }

    public function renderProclamatoreFormUpdate()
    {
        $this->assign("info_proclamatore","");
        $this->assign("action","Modifica ");
        $this->assign("url_insert_proclamatore", self::URL_SALVA_PROCLAMATORE);
        $this->assign("url_verify_proclamatore", self::URL_VERIFY_PROCLAMATORE);
        $this->assign("url_list_proclamatore", self::URL_LISTA_PROCLAMATORE);
        return $this->draw("proclamatore/proclamatore_form",true);
    }

}