<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/03/2016
 * Time: 12:46
 */
class proclamatoreModel
{
    protected $proclamatore_id;
    protected $proclamatore_nome;
    protected $proclamatore_cognome;
    protected $proclamatore_telefono;
    protected $ruoli = array();

    const TAB_PROCLAMATORI = "proclamatori";
    const TAB_RUOLI = "ruoli";
    const TAB_RUOLI_PROCLAMATORI = "ruoli_proclamatori";


    public function getProclamatoreId()
    {
        return $this->proclamatore_id;
    }

    public function setProclamatoreId($proclamatore_id)
    {
        $this->proclamatore_id = $proclamatore_id;
    }

    public function getProclamatoreNome()
    {
        return $this->proclamatore_nome;
    }

    public function setProclamatoreNome($proclamatore_nome)
    {
        $this->proclamatore_nome = $proclamatore_nome;
    }

    public function getProclamatoreCognome()
    {
        return $this->proclamatore_cognome;
    }

    public function setProclamatoreCognome($proclamatore_cognome)
    {
        $this->proclamatore_cognome = $proclamatore_cognome;
    }

   
    public function getProclamatoreTelefono()
    {
        return $this->proclamatore_telefono;
    }

    public function setProclamatoreTelefono($proclamatore_telefono)
    {
        $this->proclamatore_telefono = $proclamatore_telefono;
    }

    public function getProclamatoreMail()
    {
        return $this->proclamatore_mail;
    }

    public function setProclamatoreMail($proclamatore_mail)
    {
        $this->proclamatore_mail = $proclamatore_mail;
    }

    public function getRuoli()
    {
        return $this->ruoli;
    }

    public function setRuoli($ruoli)
    {
        $this->ruoli = $ruoli;
    }

    public function getAllProclamatore()
    {
        try {
            $p = dbAppHandler::getCon();
            if($p instanceof PDO) {}
            $query = "SELECT * FROM ".self::TAB_PROCLAMATORI;
            $obj= $p->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function saveProclamatore()
    {
        if(isset($this->proclamatore_id)){
            //UPDATE
            $info_update_proclamatore = array(
                ":proclamatore_id" => $this->proclamatore_id,
                ":proclamatore_nome" => $this->proclamatore_nome,
                ":proclamatore_cognome" => $this->proclamatore_cognome,
                ":proclamatore_telefono" => $this->proclamatore_telefono,
                ":proclamatore_mail" => $this->proclamatore_mail
            );


            $q_update_proclamatore = "UPDATE ". self::TAB_PROCLAMATORI ." SET
                                        proclamatore_nome = :proclamatore_nome,
                                        proclamatore_cognome = :proclamatore_cognome,
                                        proclamatore_telefono = :proclamatore_telefono,
                                        proclamatore_mail = :proclamatore_mail
                                  WHERE
                                        proclamatore_id = :proclamatore_id";

            try {
                $cn = dbAppHandler::getCon();
                $cn->beginTransaction();

                //update proclamatore
                $q_update_proclamatore = $cn->prepare($q_update_proclamatore);
                $q_update_proclamatore->execute($info_update_proclamatore);

                //clear old ruoli
                $q_clear_ruoli = "DELETE FROM ruoli_proclamatori WHERE rp_proclamatore_id_ref = $this->proclamatore_id ";
                $clear_ruoli = $cn->prepare($q_clear_ruoli);
                $clear_ruoli->execute();

                $ruoli_checked = $this->getRuoli();
                foreach($ruoli_checked AS $one_ruolo){
                    $q_insert_ruolo = "INSERT INTO ".self::TAB_RUOLI_PROCLAMATORI." (rp_ruolo_id_ref, rp_proclamatore_id_ref) VALUES ($one_ruolo,$this->proclamatore_id)";
                    $insert_ruolo = $cn->prepare($q_insert_ruolo);
                    $insert_ruolo->execute();
                }


                $cn->commit();
                return true;
            } catch (Exception $e) {
                $cn->rollBack();
                trigger_error("Error" . $e->getMessage());
                return false;
            }

        }else{
            //INSERT
            $q_insert_proclamatore = "INSERT INTO ".self::TAB_PROCLAMATORI."  (proclamatore_nome, proclamatore_cognome, proclamatore_telefono, proclamatore_mail) VALUES (:proclamatore_nome, :proclamatore_cognome, :proclamatore_telefono, :proclamatore_mail)";
            $info_insert_proclamatore = array(
                ":proclamatore_nome" => $this->proclamatore_nome,
                ":proclamatore_cognome" => $this->proclamatore_cognome,
                ":proclamatore_telefono" => $this->proclamatore_telefono,
                ":proclamatore_mail" => $this->proclamatore_mail
            );


            try {
                $cn = dbAppHandler::getCon();
                //inserimento proclamatore
                $insert_proclamatore = $cn->prepare($q_insert_proclamatore);
                $insert_proclamatore->execute($info_insert_proclamatore);

                $rp_proclamatore_id_ref = $cn->lastInsertId();
                $ruoli_checked = $this->getRuoli();
                foreach($ruoli_checked AS $one_ruolo){
                    $q_insert_ruolo = "INSERT INTO ".self::TAB_RUOLI_PROCLAMATORI." (rp_ruolo_id_ref, rp_proclamatore_id_ref) VALUES ($one_ruolo,$rp_proclamatore_id_ref  )";
                    $insert_ruolo = $cn->prepare($q_insert_ruolo);
                    $insert_ruolo->execute();
                }

                return true;
            } catch (Exception $e) {
                trigger_error("Error" . $e->getMessage());
                return false;
            }
        }
    }

    public static function loadFromId($id)
    {
        $info = array(":id" => $id);
        $query = "SELECT * FROM ".self::TAB_PROCLAMATORI." WHERE proclamatore_id = :id";
        try {
            $cn = dbAppHandler::getCon();
            $i_result = $cn->prepare($query);
            $i_result->execute($info);
            $i_result->setFetchMode(PDO::FETCH_CLASS, __CLASS__);
            $ob_return = $i_result->fetch();
            $i_result->closeCursor();
            unset($i_result, $cn);
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }

        if (!empty($ob_return) && !is_null($ob_return->getProclamatoreId())) {
            return $ob_return;
        } else {
            return null;
        }
    }

    public function deleteProclamatore($id)
    {
        $q_delete_proclamatore = "DELETE FROM ".self::TAB_PROCLAMATORI." WHERE proclamatore_id =".$id;
        $q_clear_ruoli = "DELETE FROM ruoli_proclamatori WHERE rp_proclamatore_id_ref = ".$id;


        try {
            $cn = dbAppHandler::getCon();
            $cn->beginTransaction();

            $obj= $cn->prepare($q_delete_proclamatore);
            $obj->execute();

            $clear_ruoli = $cn->prepare($q_clear_ruoli);
            $clear_ruoli->execute();

            $cn->commit();
            return true;
        } catch (Exception $e) {
            $cn->rollBack();
            trigger_error("Error" . $e->getMessage());
            return false;
        }
    }

    public function searchProclamatore($nome, $cognome)
    {
        $info = array(
            ":proclamatore_nome" => $nome,
            ":proclamatore_cognome" => $cognome
            );
        $query = "SELECT * FROM ".self::TAB_PROCLAMATORI ." WHERE proclamatore_nome = :proclamatore_nome AND proclamatore_cognome = :proclamatore_cognome";
        try {
            $p = dbAppHandler::getCon();
            if($p instanceof PDO) {}

            $obj= $p->prepare($query);
            $obj->execute($info);
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function getAllRuoli(){
        try {
            $p = dbAppHandler::getCon();
            if($p instanceof PDO) {}
            $query = "SELECT * FROM ".self::TAB_RUOLI." ORDER BY ruolo_desc";
            $obj= $p->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function getRuoliByProclamatore($proclamatore_id){
        try {
            $p = dbAppHandler::getCon();
            $query = "SELECT rp_ruolo_id_ref as ruolo_id FROM ".self::TAB_RUOLI_PROCLAMATORI." WHERE rp_proclamatore_id_ref=$proclamatore_id";
            $obj= $p->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function getPuntiByIdProcl($id_proc){
        try {
            $p = dbAppHandler::getCon();
            $query = "SELECT adunanza_proclamatore.ap_id_proclamatore, adunanza_proclamatore.ap_id_punto as punto, adunanza_proclamatore.ap_punto_ok AS superato, adunanza.adunanza_data as ad_data FROM adunanza_proclamatore INNER JOIN adunanza ON adunanza.adunanza_id=adunanza_proclamatore.ap_id_adunanza WHERE adunanza_proclamatore.ap_id_proclamatore = $id_proc AND adunanza_proclamatore.ap_id_punto IS NOT NULL ORDER BY adunanza_proclamatore.ap_id_punto";
            //$query = "SELECT adunanza.adunanza_data AS ad_data, ap_id_punto AS punto, punti.punto_desc AS nome_punto, ap_punto_ok AS superato FROM adunanza_proclamatore INNER JOIN adunanza on adunanza.adunanza_id=adunanza_proclamatore.ap_id_adunanza RIGHT JOIN punti ON punti.punto_id=adunanza_proclamatore.ap_id_punto WHERE ap_id_proclamatore = $id_proc ORDER BY adunanza.adunanza_data  ";
            $obj= $p->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }
    }

    public function getStoriaByIdProc($id_proc){
            $p = dbAppHandler::getCon();
            $query = "SELECT * FROM storia WHERE id_dim =  $id_proc";
            $obj= $p->prepare($query);
            $obj->execute();
            $result = $obj->fetchAll(PDO::FETCH_ASSOC);
            return $result;
    }

    public function getStoriaProclamatorebyIdProc($id_proc){
        $p = dbAppHandler::getCon();
        $query = "SELECT * FROM storico_proc WHERE id =  $id_proc";
        $obj= $p->prepare($query);
        $obj->execute();
        $result = $obj->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


}