<?php

/**
 * Created by PhpStorm.
 * User: SS
 * Date: 26/03/2016
 * Time: 12:06
 */

//include_once("stampaListaProclamatori.php");
class proclamatoreController
{

    public function proclamatoreList()
    {
        //definisco tipo di utente
        $admin_user = uacController::checkAdminUser();
        $proc_model = new proclamatoreModel();
        
        $proclamatore_list = $proc_model->getAllProclamatore();
        $proc_viewe = new proclamatoreView();
        $proc_viewe->assign("admin_mode", $admin_user);
        $page = new appView();
        $page->local_js = $proc_viewe::LISTA_JS;
        $page->titolo_pagina = $proc_viewe::TITOLO_LISTA;
        $page->html_wrapper = $proc_viewe->renderProclamatoreList($proclamatore_list);
        $page->defaultView();
    }


    public function proclamatoreForm()
    {
        //definisco tipo di utente
        $admin_user = uacController::checkAdminUser();
        
        if(isset($_GET['id'])){
            $id_proclamatore = $_GET['id'];
        }

        if(!isset($id_proclamatore)){
            //insert
            $proc_model = new proclamatoreModel();
            $ruoli_list = $proc_model->getAllRuoli();
            foreach($ruoli_list as $key => $info){
                $ruoli_list[$key]["checked"] = "";
            }
            $proc_viewe = new proclamatoreView();
            $proc_viewe->assign("update_mode", false);
            $proc_viewe->assign("proclamatore_id","");
            $proc_viewe->assign("proclamatore_nome","");
            $proc_viewe->assign("proclamatore_cognome","");
            $proc_viewe->assign("proclamatore_telefono","");
            $proc_viewe->assign("proclamatore_mail","");
            $proc_viewe->assign("ruoli_list",$ruoli_list);
            $proc_viewe->assign("admin_mode", $admin_user);

            $app_insert = new appView();
            $app_insert->local_js = $proc_viewe::FORM_INSERT_JS;
            $app_insert->titolo_pagina = $proc_viewe::TITOLO_FORM_INSERT;
            $app_insert->html_wrapper = $proc_viewe->renderProclamatoreFormInsert();
            $app_insert->defaultView();
        }else {
            //update
            $proc_model = new proclamatoreModel();
            $act_used = $proc_model->loadFromId($id_proclamatore);
            $ruoli_checked_list = $proc_model->getRuoliByProclamatore($id_proclamatore);
            $box_checked = array();
            foreach($ruoli_checked_list as $one_checked_ruolo){
                $box_checked[] = $one_checked_ruolo['ruolo_id']; //id ruolo checked

            }
            $ruoli_list = $proc_model->getAllRuoli();
            foreach($ruoli_list as $key => $val){
                $one_ruolo = $ruoli_list[$key]["ruolo_id"]; //id ruolo in elenco
                if (in_array($one_ruolo, $box_checked)) {
                    $ruoli_list[$key]["checked"] = 'checked="checked"';
                }else{
                    $ruoli_list[$key]["checked"] = "";
                }
            }
            $storia_proc = $proc_model->getStoriaProclamatorebyIdProc($id_proclamatore);
            foreach($storia_proc as $key => $val){
                $storia_proc[$key]['data'] = dateTimeHelper::dash2slash($storia_proc[$key]['data']);
                if(isset($storia_proc[$key]['superato']) &&  $storia_proc[$key]['superato'] == 1){
                    $storia_proc[$key]['superato'] = "ok";
                }else{
                    $storia_proc[$key]['superato'] = "";
                }
            }

            $proc_viewe = new proclamatoreView();
            $proc_viewe->assign("update_mode", true);
            $proc_viewe->assign("proclamatore_id", $act_used->getProclamatoreId());
            $proc_viewe->assign("proclamatore_nome", $act_used->getProclamatoreNome());
            $proc_viewe->assign("proclamatore_cognome", $act_used->getProclamatoreCognome());
            $proc_viewe->assign("proclamatore_telefono", $act_used->getProclamatoreTelefono());
            $proc_viewe->assign("proclamatore_mail", $act_used->getProclamatoreMail());
            $proc_viewe->assign("ruoli_list",$ruoli_list);
            $proc_viewe->assign("storia_proc",$storia_proc);
            $proc_viewe->assign("admin_mode", $admin_user);
            

            $app_update = new appView();
            $app_update->local_js = $proc_viewe::FORM_UPDATE_JS;
            $app_update->titolo_pagina = $proc_viewe::TITOLO_FORM_MODIFY;
            $app_update->html_wrapper = $proc_viewe->renderProclamatoreFormUpdate();
            $app_update->defaultView();

        }
    }

    public function proclamatoreSave(){
        $proc_model = new proclamatoreModel();
        if(isset($_POST['proclamatore_id'])){
            $proclamatore_id = trim($_POST['proclamatore_id']);
            $proc_model->setProclamatoreId($proclamatore_id);
        }
        $proc_model->setProclamatoreNome(trim($_POST['proclamatore_nome']));
        $proc_model->setProclamatoreCognome(trim($_POST['proclamatore_cognome']));
        if(isset($_POST['proclamatore_telefono'])){
            $proc_model->setProclamatoreTelefono(trim($_POST['proclamatore_telefono']));
        }
        if(isset($_POST['proclamatore_mail'])){
            $proc_model->setProclamatoreMail(trim($_POST['proclamatore_mail']));
        }
        if(isset($_POST['ruoli'])){
            $proc_model->setRuoli($_POST['ruoli']);
        }

        $result_search = $proc_model->searchProclamatore(trim($_POST['proclamatore_nome']),trim($_POST['proclamatore_cognome']));

        if(count($result_search) > 0){
            if(isset($proclamatore_id) && $result_search[0]['proclamatore_id'] == $proclamatore_id){
                if ($proc_model->saveProclamatore()) {
                    if(isset($proclamatore_id)){
                        $response = array('esito' => "OK", 'mess' => "Dati proclamatore modificati");
                    }else{
                        $response = array('esito' => "OK", 'mess' => "Nuovo proclamatore inserito");
                    }
                }
            }else{
                $response = array('esito' => "KO", 'mess' => "Proclamatore esistente");
            }
        }else{
            if ($proc_model->saveProclamatore()) {
                if(isset($proclamatore_id)){
                    $response = array('esito' => "OK", 'mess' => "Dati proclamatore modificati");
                }else{
                    $response = array('esito' => "OK", 'mess' => "Nuovo proclamatore inserito");
                }
            }
        }

        echo json_encode($response);
    }

    public function proclamatoreDelete(){
        $proclamatore_id = $_GET['id'];
        $a_model = new proclamatoreModel();
        if($a_model->deleteProclamatore($proclamatore_id) === true){
            header("location: proclamatori");
        }
    }

    public function stampaProclamatori()
    {
        $proc_model = new proclamatoreModel();
        $list_proclamatori = $proc_model->getAllProclamatore();
        $stampa = new stampaListaProclamatori();
        $stampa->stampaListaIsc($list_proclamatori);
    }

    public function getPunti(){
        $proc_id = $_GET['id'];
        $model_proc = new proclamatoreModel();
        $model_adun = new adunanzaModel();
        $all_punti = $model_adun->allPunti();
        $punti_by_proc =  $model_proc->getPuntiByIdProcl($proc_id);
        $array_punti_ok = array();
        foreach($punti_by_proc as $key => $val){
            $array_punti_ok[$punti_by_proc[$key]['punto']] = $punti_by_proc[$key]['superato'];
        }

        $result = array();;
        foreach($all_punti as $key => $val){
            $punto_id = $all_punti[$key]['punto_id'];
            $punto_desc = utf8_encode($all_punti[$key]['punto_desc']);
            $ruoli = "";
            if($all_punti[$key]['punto_lettura'] == 1){
                $ruoli .= " Lett";
            }
            if($all_punti[$key]['punto_dimostrazione'] == 1){
                $ruoli .= " Dim";
            }
            if($all_punti[$key]['punto_discorso'] == 1){
                $ruoli .= " Disc";
            }
            $superato = null;

            if(array_key_exists($all_punti[$key]['punto_id'], $array_punti_ok)){
                $superato = $array_punti_ok[$all_punti[$key]['punto_id']];

            }
            $result[] = array( "punto_id" => $punto_id, "punto_desc" => $punto_desc, "ruoli" => $ruoli, "superato" => $superato );
        }
       echo json_encode($result);
    }

    public function getStoria(){
        $proc_id = $_GET['id'];
        $model_proc = new proclamatoreModel();
        $result = $model_proc->getStoriaByIdProc($proc_id);
        echo json_encode($result);
    }

    public function getProclamatore(){
        $proc_id = $_GET['id'];
        $model_proc = new proclamatoreModel();
        $result = $model_proc->loadFromId($proc_id);
        if($result instanceof proclamatoreModel){
            $info = $result->getProclamatoreNome()." ".$result->getProclamatoreCognome();
            echo json_encode($info);
        }else{
            return false;
        }


    }


}