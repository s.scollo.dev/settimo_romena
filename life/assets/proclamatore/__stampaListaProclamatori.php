<?php
include_once("assets/plugin/fpdf181/fpdf.php");
/**
 * Created by PhpStorm.
 * User: salvatore
 * Date: 11/04/2016
 * Time: 16:10
 */
class stampaListaProclamatori
{
    public function stampaListaIsc($lista_proclamatori){


        $anno_accademico = '2015 - 2016';

        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 14);

        $pdf->Cell(190, 10, 'UNIVERSITA\' DELLA TERZA ETA - CARMAGNOLA', 0, 0, 'C');

        $pdf->SetY(17);
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(190, 10, 'ANNO ACCADEMICO ' . $anno_accademico, 0, 0, 'C');

        $pdf->SetY(25);
        $pdf->SetFont('Arial', 'B', 13);
        $pdf->Cell(190, 10, 'ELENCO PROCLAMATORI', 0, 0, 'C');


        $pdf->setY(45);
        $pdf->SetFont('Arial', 'B', 11);
        $pdf->Cell(5, 10, '', 0, 0, 'L');
        $pdf->Cell(30, 10, 'COGNOME', 0, 0, 'L');
        $pdf->Cell(30, 10, 'NOME', 0, 0, 'L');
        $pdf->Cell(80, 10, 'INDIRIZZO', 0, 0, 'C');
        $pdf->Cell(25, 10, 'TELEFONO', 0, 0, 'L');
        $pdf->Cell(15, 10, 'TESSERA', 0, 0, 'L');

        $pdf->setY(55);
        $pdf->SetFont('Arial', '', 9);
        $y = 0;
        $count = 1;
        foreach($lista_proclamatori as $one_info){
            $pdf->setY(55+$y);
            $tessera = strtoupper($one_info['proclamatore_id'] );
            $nome = utf8_decode(strtoupper($one_info['proclamatore_nome']));
            $cognome = utf8_decode(strtoupper($one_info['proclamatore_cognome']));
            $telefono = strtoupper($one_info['proclamatore_telefono']);
            $comune = utf8_decode(strtoupper($one_info['proclamatore_comune'] ));
            $indirizzo = utf8_decode(strtoupper($one_info['proclamatore_via'].', '.$one_info['proclamatore_civico']));

            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(5, 10, $count, 0, 0, 'L');
            $pdf->Cell(30, 10, $cognome, 0, 0, 'L');
            $pdf->Cell(30, 10, $nome, 0, 0, 'L');
            $pdf->Cell(50, 10, $indirizzo, 0, 0, 'L');
            $pdf->Cell(30, 10, $comune, 0, 0, 'L');
            $pdf->Cell(25, 10, $telefono, 0, 0, 'L');
            $pdf->Cell(15, 10, $tessera, 0, 0, 'R');


            $y = $y+10;

        }

        $pdf->Output();
    }
}