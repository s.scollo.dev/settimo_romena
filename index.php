<?php
ini_set('display_errors','On');
ini_set('max_input_vars','1500');
sec_session_start();

//DEFINISCO PARAMETRO CHE RIPRENDO IN CONFIG
//NON POSSO INSERIRE $_SERVER['DOCUMENT_ROOT'] IN CONST DI CONFIG
define("DOC_ROOT",$_SERVER['DOCUMENT_ROOT']);

//caricamento autoloader
require "assets/driver/driverEngine.php";


//avvio applicazione
appStart();


function sec_session_start() {
    $id_session = date('YMD');
    $session_name = $id_session; // Imposta un nome di sessione
    $secure = false; // Imposta il parametro a true se vuoi usare il protocollo 'https'.
    $httponly = true; // Questo impedir� ad un javascript di essere in grado di accedere all'id di sessione.
    ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
    $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    session_name($session_name); // Imposta il nome di sessione con quello prescelto all'inizio della funzione.
    session_start(); // Avvia la sessione php.
    session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
}


//FUNZIONE DI SERVIZIO
function pre($x){
    echo '<pre>';
    print_r($x);
    echo '<pre>';
}